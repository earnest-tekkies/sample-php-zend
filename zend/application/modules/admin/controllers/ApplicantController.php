<?php

class Admin_ApplicantController extends Zend_Controller_Action
{
	
    public function init()
    {
        /* Initialize action controller here */
    }

   public function indexAction()
    {
	
     
        $this->view->headLink()->appendStylesheet("/css/searchCriteria.css"); 
 
         $this->view->headScript()
                ->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.widget.min.js")
                ->appendFile("/js/jquery.ui.position.js")
                ->appendFile("/js/jquery.ui.autocomplete.js")
                ->appendFile("/js/applicantList.js")
                ->appendFile("/js/jquery.mousewheel.js")
                 ->appendFile("/js/jquery.tablesorter.js")
                ->appendFile("/js/jquery.jscrollpane.min.js");
            $this->view->headLink()
                    ->appendStylesheet("/css/greentheme.css")
                    ->appendStylesheet("/css/jquery.ui.autocomplete.css");
        $this->view->headScript()->appendScript(
        "$(document).ready(function(){
            try{
            
            $('#applicantsTbl').tablesorter( { headers: {  1: {sorter: false} },widgets: ['zebra']} ); 
              }catch(e){alert(e);}
            $('#limitCount').change(function(){
              $('#page').val(1);
            $('#paginatorFilter').submit();

            });
           $('.paginationControl a' ).click(function(){
           
            $('#page').val($(this).attr('page'))
            $('#paginatorFilter').submit();
            });
            
          /*
           $('.searchCriteria').jScrollPane
           (
                {
                    hijackInternalLinks: true
                }
	   );
           */
            });
          
          ");
        $auth = Zend_Auth::getInstance();
       if($auth->hasIdentity())
        {
			 $storage = $auth->getStorage();
			$storedata=$storage->read();
			
			// Annsys 13-09-2012
			$identity = $auth->getIdentity();
			if(isset($identity->role))
			{
				$role = strtolower($identity->role);
				$groupRole=strtolower($identity->group_role);
				$user_role=strtolower($identity->user_role);
				$aclRoleGroup= new Model_aclRoleGroup();
				$aclUsersRoleTbl =new Model_aclRoleUsers();
				$groupPrevileges[]='';
				$userPrevilegeAllow[]='';
				if($user_role!='')
				{
					$userPrevilegeRow=$aclUsersRoleTbl->find($user_role)->current();
					
					 if($userPrevilegeRow !="")
					{
						$userPrevilegeAllow=unserialize($userPrevilegeRow->allow_previleges);
						//$userPrevilegeDeny=unserialize($userPrevilegeRow->deny_previleges);
						
					}
				}
				if($groupRole!="")
				{
		  
					$groupPrevilegeRow=$aclRoleGroup->find($groupRole)->current();
					if($groupPrevilegeRow !="")
					{
						$groupPrevileges=unserialize($groupPrevilegeRow->previleges);          
						
					}
				}			
	   
			}
		}
		if(in_array('21',$groupPrevileges) || in_array('21',$userPrevilegeAllow) || $groupRole=='admin')
		{ 
			$this->view->profileviewaccess='yes';
		}
		else
		{
			$this->view->profileviewaccess='no';
		}

        $searchreportfieldsTab = new admin_Model_searchreportfields();
      
       
          $fieldsquery = $searchreportfieldsTab->select();
         $fieldsquery->from('search_report_fields as srf',array('field_description_id'));
          $fieldsquery->join('search_fields as sf','srf.field_description_id=sf.field_name',array('field_name','field_description'));
         $fieldsquery->where("usertype='$storedata->group_role' and active=1");
         $fieldsquery->setIntegrityCheck(false);
       
         
         
          
         $result = $searchreportfieldsTab->fetchAll($fieldsquery);
         $this->view->applicantfielddescriptionList =$result->toArray();
        $form = new admin_Form_applicantfielddescriptionform($result);
        $this->view->form=$form;
         $fieldsDisplay=array();
       
         foreach($result as $field)
         {
            $fieldsDisplay[$field->field_description_id]=$field->field_description;
            $fieldsToFetch[$field->field_description_id]=$field->field_description_id;
         }
         $form->populate(array("field_description_id"=>$fieldsToFetch));
        
         
         $styles=array();
         
          // Default display name
         {
              $fieldsToFetch['name']="CONCAT(app_first_name,  ' ',app_last_name)";
              $fieldsToFetch['applicant_id']="applicant_id";
              $fieldsToFetch['app_gender']="app_gender";
               $fieldsToFetch['marital_status']="app_marital_status";
              unset($fieldsDisplay['name']);
         }
         if(isset($fieldsDisplay['age']))
         {
            
             $fieldsToFetch['age']="DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')";
             
         }
          if(isset($fieldsDisplay['app_address']))
         {
            $styles['app_address']="white-space:nowrap; COLOR: #3f3f3f;";
           
         }
        
          if(isset($fieldsDisplay['app_marital_status']))                    
         {
            $fieldsToFetch['app_marital_status']= new zend_db_expr("IF( app_marital_status =  'S',  'Single', IF( app_marital_status =  'D',  'Divorced', IF( app_marital_status =  'W'
            AND app_gender =  'Male',  'Widower',  'Widow' ) ) )");
         }
         if(isset($fieldsDisplay['app_email']))                    
         {
            $fieldsToFetch['app_email']= "concat(app_email1,'<br>',app_email2,'<br>',app_email3)";
         }
          if(isset($fieldsDisplay['app_phone']))                    
         {
            $fieldsToFetch['app_phone']= "concat(app_phone1,'<br>',app_phone2,'<br>',app_phone3)";
         }
          if(isset($fieldsDisplay['registration_date']))                    
         {
            $fieldsToFetch['registration_date']= "DATE_FORMAT(registration_date, '%m/%Y')";
         }
          if(isset($fieldsDisplay['app_dob']))                    
         {
            $fieldsToFetch['app_dob']= "DATE_FORMAT(app_dob, '%m/%d/%Y')";
         }
          if(isset($fieldsDisplay['last_activity_date']))                    
         {
            $fieldsToFetch['last_activity_date']= "DATE_FORMAT(last_activity_date, '%m/%d/%Y')";
         }
         $this->view->styles=$styles;
         $this->view->fieldsDisplay=$fieldsDisplay;
        $paginatorFilter= new admin_Form_paginatorFilter();
       $applicantDetailsTbl = new applicant_Model_applicantDetails();
       if($this->getRequest()->isPost())
        {   
        
           if(isset($_POST["search"]))
           {
               $_POST["filterPaameters"]=serialize($_POST);
           }
           $paginatorFilter->populate($_POST);
           $form->populate($_POST);
         
        }  
           $this->view->paginatorFilter=$paginatorFilter;
           $matchMakerTbl = new Model_staffLogin();
           $staffList= array();
           foreach($matchMakerTbl->fetchAll("usertype='MatchMaker'") as $staff)
           {
               $staffList[$staff->staff_id]= $staff->name."(".$staff->staff_id.")";
           }
            $this->view->staffList=$staffList;
       $query=$applicantDetailsTbl->select();
    
       $query->from("applicant_information",$fieldsToFetch);
      
       $filterParameters = unserialize($this->_request->getParam('filterPaameters'));
      
       $searchCriteriaArr=$this->queryBuilder($query,$filterParameters);
       foreach ($searchCriteriaArr as $searchCriteria)
       {
           if(count($searchCriteria["items"])>0)
           {
              
           $searchCriterias.="<div class=\"headerbg\"><div class=\"headerbar\">".$searchCriteria["name"]."</div></div>";
           $searchCriterias.="<div><ul><li><a href=''>".implode("</a></li><li><a href=''>",$searchCriteria["items"])."</a></li></ul></div>";
            }
       }
      $this->view->searchCriterias=$searchCriterias;
      
      if($storedata->group_role=="MatchMaker")
      {
        $userApplicantAccessList=array("");
      $userApplicantAccessTab = new admin_Model_userapplicantaccess();
      
        $userApplicantAccess=$userApplicantAccessTab->fetchRow("staff_id = '$storedata->staff_id'");
    
      if($userApplicantAccess->applicant_lists !="")
      {	
          $userApplicantAccessList=unserialize($userApplicantAccess->applicant_lists);
      }
      
      
     //**********
       $query->where("applicant_id in ('".implode("','",$userApplicantAccessList)."')");
      }
          $query->setIntegrityCheck(false);
           
     
     $registry = Zend_Registry::getInstance();
      
      $acl=  $registry->get('acl'); 
      $role= $registry->get('role'); 
     
      
  
   
    $this->view->matchmakersettings=$acl->isAllowed($role, 'admin'.":"."applicant", 'updatematchmakeraccess');
     $this->view->addToEventsAccess=$acl->isAllowed($role, 'admin'.":"."applicant", 'getevents');
        $this->view->exportToXlsAccess=$acl->isAllowed($role, 'admin'.":"."applicant", 'exportlist');
        $this->view->sendBulkMailAcccess=$acl->isAllowed($role, 'admin'.":"."applicant", 'sendbulkemail');
      
      // echo "<br>".$query;
           $adapter =  new Zend_Paginator_Adapter_DbTableSelect($query);
           
            $paginator = new Zend_Paginator($adapter);
            $limitCount=$this->_request->getParam('limitCount', "");
            $paginator->setItemCountPerPage($limitCount);
            $page = $this->_request->getParam('page', 1);
            $paginator->setCurrentPageNumber($page);
             if($paginator->getTotalItemCount()=== 1)
            {
                $applicant_id=$paginator->getItem(1)->applicant_id;
               $this->_redirect("/admin/applicant/profile/applicantId/$applicant_id");
            }
            // pass the paginator to the view to render
            $this->view->paginator = $paginator;
            
       
    }
     public function sendbulkemailAction()
     {
         
 
     $this->view->headScript() 
                          ->appendFile("/js/jquery.ui.core.min.js")
                        ->appendFile("/js/jquery.ui.widget.min.js")
                        ->appendFile("/js/jquery.ui.position.js")
                       ->appendFile("/js/jquery.ui.autocomplete.js")
                       ->appendFile("/js/ckeditor/ckeditor.js")
                       ->appendFile("/js/jquery.timeago.js")
                       ->appendFile("/js/ckeditor/adapters/jquery.js")
            ->appendFile("/js/bulkemails.js");
     $this->view->headLink()
             ->appendStylesheet("/js/ckeditor/sample.css")
             ->appendStylesheet("/css/jquery.ui.autocomplete.css"); 
 

       $this->view->headScript()->appendScript("  
           $(document).ready(function() {
           
           $( '#message' ).ckeditor();

           });  ");
   
  
          $auth = Zend_Auth::getInstance();
       if($auth->hasIdentity())
        {
         $storage = $auth->getStorage();
        $storedata=$storage->read();
    
        }
        
           $profileids= $this->_getParam('profileids');
            $applicantDetailsTbl = new applicant_Model_applicantDetails();
         $applicantQuery=$applicantDetailsTbl->select();
         $applicantId = $this->_getParam("applicantId");
         $applicantQuery->from("applicant_information as ai",array("app_first_name",
             "app_last_name","app_email1","app_email2","app_email3"));
         $applicantQuery->where("applicant_id in (".implode(",",$profileids).")"); 
         $applicantQuery->setIntegrityCheck(false);
         $result=$applicantDetailsTbl->fetchAll($applicantQuery);
         $receiverNameList=array();
         foreach($result as $data)
         {
             $receiverNameList[]=$data->app_first_name." ".$data->app_last_name;
             if($data->app_email1 !="")
             $receiverEmails[] = $data->app_email1;
              if($data->app_email2 !="")
             $receiverEmails[] = $data->app_email2;
               if($data->app_email3 !="")
             $receiverEmails[] = $data->app_email3;
         }

         $this->view->profileids         =   $profileids;
         $this->view->receiverEmailsIds  =   trim(implode(",",$receiverEmails));
         $this->view->receiverNames  =   trim(implode(">,<",$receiverNameList));
	

     }
     public function findmatchAction()
     {	
         
         $this->view->headLink()->appendStylesheet("/css/searchCriteria.css"); 
 
         $this->view->headScript()
                ->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.widget.min.js")
                ->appendFile("/js/jquery.ui.position.js")
                ->appendFile("/js/jquery.ui.autocomplete.js")
                  ->appendFile("/js/jquery.mousewheel.js")
                             ->appendFile("/js/jquery.jscrollpane.min.js");
            $this->view->headLink()->appendStylesheet("/css/jquery.ui.autocomplete.css");
                  $this->view->headScript()->appendFile("/js/matchProfile.js")
                             ->appendFile("/js/commentsAddBulk.js")
                                ->appendFile("/js/searchfields.js");
        //paginationControl
        $this->view->headScript()->appendScript(
        "$(document).ready(function(){
            $('#limitCount').change(function(){
              $('#page').val(1);
            $('#paginatorFilter').submit();

            });
           $('.paginationControl a' ).click(function(){
           
            $('#page').val($(this).attr('page'))
            $('#paginatorFilter').submit();
            });
            
             $('#searchDisplay').click(function(){
               $('#searchDiv').toggle();
             });
          });
          ");
      
         

         //18-9-2012 start
		$auth = Zend_Auth::getInstance();    //get authentication
        if($auth->hasIdentity())
		{ 
			$identity = $auth->getIdentity();
			if(isset($identity->role))
			{
				$groupRole=strtolower($identity->group_role);
				$user_role=strtolower($identity->user_role);
			}
		}
		$this->view->maker_access=array();
		$this->view->current_user=$groupRole;
		if($groupRole=='matchmaker')
		{
			$userApplicantAccessList=array();
            $userApplicantAccessTab = new admin_Model_userapplicantaccess();
			if($user_role!='')
			{
				$query = $userApplicantAccessTab->select();
				$query->from("user_applicant_access",array('applicant_lists'));
				$query->where( "staff_id='".$user_role."'");
				$query->setIntegrityCheck(false);
	  
				$userApplicantAccess=$userApplicantAccessTab->fetchRow($query);
				if($userApplicantAccess->applicant_lists !="")
				{
				   $userApplicantAccessList=  unserialize($userApplicantAccess->	applicant_lists);
				   $this->view->maker_access=$userApplicantAccessList;
				}
				
			}		
		}
		
		//18-9-2012 end
         
  
        
           $form = new admin_Form_advanceSearch();
         $applicantDetailsTbl = new applicant_Model_applicantDetails();
         $applicantId = $this->_getParam("applicantId");
         //eliminate pevious Matches
          $query=$applicantDetailsTbl->select();
         $query->from("match_information as mi",
                 array("applicant_id"=>"if(entered_applicant_id='$applicantId',reference_applicant_id,entered_applicant_id)"));
         $query->where("(mi.entered_applicant_id='$applicantId' ) or (mi.reference_applicant_id='$applicantId' )");
          $query->setIntegrityCheck(FALSE);
          
          foreach($applicantDetailsTbl->fetchAll($query) as $row)
          {
             $enteredApplicantIds[$row->applicant_id]= $row->applicant_id;
          }
       $this->view->enteredApplicantIds=($enteredApplicantIds =="")? array() :$enteredApplicantIds;
         $applicantQuery=$applicantDetailsTbl->select();
         $applicantQuery->from("applicant_information as ai",array(
             "applicant_id","app_gender","app_first_name", "app_last_name",
                     "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
                     "app_nationality","app_us_status","registration_date",
             "app_marital_status","last_activity_date","app_zip","match_age_low","match_age_high",
             "match_nationality","match_ethnicity","match_language_must","match_language_desirable",
             "match_state","match_islamic_practice_level","match_height_low","match_height_high",
             "match_skin_color","match_hijab_status","match_annual_income_low","match_annual_income_high",
             "match_occupation","match_us_status","match_education"));
         $applicantQuery->where("applicant_id='$applicantId'"); 
         $applicantQuery->setIntegrityCheck(false);
         
         
         $paginatorFilter= new admin_Form_paginatorFilter();
         $applicantRow=$applicantDetailsTbl->fetchRow($applicantQuery);
         $this->view->applDetails=$applicantRow;
         $searchDefaults=array();
         if(!$this->getRequest()->isPost())
         {
         if($applicantRow != NULL)
         {
             if($applicantRow->app_gender =="Male" )  $form->getElement("app_gender")->setValue("Female"); else $form->getElement("app_gender")->setValue("Male");
            $form->getElement("age_from")->setValue($applicantRow->match_age_low); 
            $form->getElement("age_to")->setValue($applicantRow->match_age_high); 
            $form->getElement("app_nationality")->setValue(explode(":",$applicantRow->match_nationality)); 
            $form->getElement("app_ethnicity")->setValue(explode(":",$applicantRow->match_ethnicity)); 
            $form->getElement("app_language")->setValue(explode(":",$applicantRow->match_language_must)); 
            $form->getElement("app_language_des")->setValue(explode(":",$applicantRow->match_language_desirable)); 
            $form->getElement("app_state")->setValue(explode(":",$applicantRow->match_state)); 
            
            $form->getElement("app_islamic_practice_level")->setValue(explode(":",$applicantRow->match_islamic_practice_level)); 
            $form->getElement("app_height_from_feet")->setValue(floor($applicantRow->match_height_low/12));
            $form->getElement("app_height_from_inch")->setValue($applicantRow->match_height_low%12);
            $form->getElement("app_height_to_feet")->setValue(floor($applicantRow->match_height_high/12));
            $form->getElement("app_height_to_inch")->setValue($applicantRow->match_height_high%12);
            $form->getElement("app_skin_color")->setValue(explode(":",$applicantRow->match_skin_color)); 
            
            $form->getElement("app_hijab_status")->setValue(explode(":",$applicantRow->match_hijab_status));
            $currency= new Zend_Currency();
            $currency = new Zend_Currency('en_US');
            $currency->setFormat(array('display' => Zend_Currency::NO_SYMBOL));
            if($applicantRow->match_annual_income_low >0)
            $form->getElement("app_annual_income_low")->setValue($currency->toCurrency($applicantRow->match_annual_income_low)); 
            if($applicantRow->match_annual_income_high > 0)
            $form->getElement("app_annual_income_high")->setValue($currency->toCurrency($applicantRow->match_annual_income_high)); 
            if($applicantRow->match_occupation != "Not Specified")
            $form->getElement("app_occupation")->setValue($applicantRow->match_occupation); 
            $form->getElement("app_us_status")->setValue(explode(":",$applicantRow->match_us_status)); 
            $form->getElement("app_education")->setValue(explode(":",$applicantRow->match_education)); 
            
           
            
         }
         
         }
        
         
           else
            
        { 
          if(isset($_POST["search"]))
           {
              $filterParameters=$_POST;
               $_POST["filterPaameters"]=serialize($_POST);
           }
           else
           {
                $filterParameters = unserialize($this->_request->getParam('filterPaameters'));
           }
           $paginatorFilter->populate($_POST);
           $form->populate($_POST);
      
       $paginatorFilter->getElement('filterPaameters')->setValue($filterPaameters);
          $this->view->paginatorFilter=$paginatorFilter;
       $query=$applicantDetailsTbl->select();
        $fieldsToDisplay=array("applicant_id","app_gender","app_first_name","app_last_name","app_nationality",
           "app_us_status","app_marital_status","last_activity_date",
            "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
           "registration_date","approval_status","app_dob");
       $query->from("applicant_information",$fieldsToDisplay);
           
       
      $searchCriteriaArr=$this->queryBuilder($query,$filterParameters);
      
     
       foreach ($searchCriteriaArr as $searchCriteria)
       {
           if(count($searchCriteria["items"])>0)
           {
              
           $searchCriterias.="<div class=\"headerbar\">".$searchCriteria["name"]."</div>";
           $searchCriterias.="<div><ul><li><a href=''>".implode("</a></li><li><a href=''>",$searchCriteria["items"])."</a></li></ul></div>";
            }
       }
         
       $this->view->searchCriterias=$searchCriterias;
      
       
          $query->setIntegrityCheck(false);
             $adapter =  new Zend_Paginator_Adapter_DbTableSelect($query);
            $paginator = new Zend_Paginator($adapter);
           
            $limitCount= $this->_request->getParam('limitCount',10);
             $paginator->setItemCountPerPage($limitCount);
            $page = $this->_request->getParam('page', 1);
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator; 
           
             }
             $this->view->form=$form;
        }
     
    public function queryBuilder(&$query,$filterParameters=array())
    {	
        $searchCriterias=array();
        $count=0;
         $item= addslashes(trim($filterParameters["name"]));
       if($item!="")
       {
           $searchCriterias[++$count]['name']="Name";
           $searchCriterias[$count]['items'][]=$item;
          
          $query->where("lower(app_first_name)   LIKE lower('%$item%') or lower(app_last_name)    LIKE lower('%$item%') or

			 lower(app_nick_name)     LIKE lower('%$item%') or lower(intro_first_name) LIKE lower('%$item%') or 
                         lower(intro_last_name)   LIKE lower('%$item%') or lower(intro_nick_name)  LIKE lower('%$item%') or 
                         lower(nf_ref_name1)      LIKE lower('%$item%') or lower(nf_ref_name2)     LIKE lower('%$item%') or 
                         lower(nf_ref_name3)      LIKE lower('%$item%') or lower(permitted_to) LIKE lower('%$item%')");
       }
        $email= trim($filterParameters["email"]);
        if($email!="")
       {
               
           $searchCriterias[++$count]['name']="email";
           $searchCriterias[$count]['items'][]=$email;
           $query->where("lower(app_email1)    LIKE lower('%$email%') or lower(app_email2)   LIKE lower('%$email%')   or
       lower(app_email3)    LIKE lower('%$email%') or lower(intro_email1) LIKE lower('%$email%')   or 
       lower(intro_email2)  LIKE lower('%$email%') or lower(intro_email3) LIKE lower('%$email%')   or 
       lower(nf_ref_email1) LIKE lower('%$email%') or lower(nf_ref_email2) LIKE lower('%$email%')  or 
       lower(nf_ref_email3) LIKE lower('%$email%')");
       }
       
              $phone= trim($filterParameters["phone"]);
       
       if($phone!="")
       {
            $searchCriterias[++$count]['name']="Phone";
           $searchCriterias[$count]['items'][]=$phone;
        $phone=   str_replace(array( "(",")","-",".","_"), "",$phone);
          
          
        $query->where("lower(app_phone1)    LIKE lower('%$phone%') or lower(app_phone2)    LIKE lower('%$phone%')  or 
                       lower(app_phone3)    LIKE lower('%$phone%') or lower(intro_phone1)  LIKE lower('%$phone%')  or 
                       lower(intro_phone2)  LIKE lower('%$phone%') or lower(intro_phone3)  LIKE lower('%$phone%')  or 
                       lower(nf_ref_phone1) LIKE lower('%$phone%') or lower(nf_ref_phone2) LIKE lower('%$phone%')  or 
                       lower(nf_ref_phone3) LIKE lower('%$phone%') ");
       }
       $app_state= $filterParameters["app_state"];
        if(is_array($app_state))
        if(!in_array("Any", $app_state))
       if(count($app_state)>0)
       {
            $searchCriterias[++$count]['name']="State";
           $searchCriterias[$count]['items']=$app_state;
          
           $stateList= implode("' or app_state='", $app_state);
           $query->where("app_state='".$stateList."'");
       }
           
       
        $app_gender= $filterParameters["app_gender"];
           if(is_array($app_gender))
       if(count($app_gender)>0)
       {
            $searchCriterias[++$count]['name']="Gender";
           $searchCriterias[$count]['items']=$app_gender;
           
           $genderList= implode("' or app_gender='", $app_gender);
           $query->where("app_gender='".$genderList."'");
       }
           
        $app_marital_status= $filterParameters["app_marital_status"];
         if(is_array($app_marital_status))
       if(count($app_marital_status)>0)
       {
            $searchCriterias[++$count]['name']="Marital Status";
           $searchCriterias[$count]['items']=$app_marital_status;
        
           $maritalStatusList= implode("' or app_marital_status='", $app_marital_status);
           $query->where("app_marital_status='".$maritalStatusList."'");
       }  
       
     $curr_date = date('Y-m-d');
     $lowerSelected=false;
     $age_from= trim($filterParameters["age_from"]);
     $age_to= trim($filterParameters["age_to"]);
     if($age_from!=""and $age_from!="Any")
     {
         $lowerSelected=true;
          $searchCriterias[++$count]['name']= "Age";
          $searchCriterias[$count]['items'][0] = " > ".$age_from;
     $date_age_from = date('Y-m-d' , strtotime( ' -'.($age_from-1).' year -6 month' ,  strtotime($curr_date)));
     $search_date_age_from = date('Y-m-d' , strtotime($date_age_from));
      $query->where("app_dob <= '$date_age_from' ");
     }
     if($age_to!=""and $age_to!="Any")
     {
       if($lowerSelected)
       {
           $lowerSelected=false;
          $searchCriterias[$count]['items'][0] .= " And  <".$age_to;
       }
       else
       {
             $searchCriterias[++$count]['name']="Age";
             $searchCriterias[$count]['items'][0]= " < ".$age_to;
       }
          
       $date_age_to = date('Y-m-d' , strtotime( ' -'.$age_to.' year -6 month' ,  strtotime($curr_date)));
       $search_date_age_to = date('Y-m-d' , strtotime($date_age_to));
       $query->where("app_dob >= '$search_date_age_to' ");
     }
     
      $app_nationality= $filterParameters["app_nationality"];
      
      if(is_array($app_nationality))
      if(!in_array("Any", $app_nationality))
      {
	 if(!in_array("Other", $app_nationality))
         {
           if(count($app_nationality)>0)
           {
                $searchCriterias[++$count]['name']="Nationality";
                $searchCriterias[$count]['items']=$app_nationality;
                
               $nationalityList= implode("' or app_nationality='", $app_nationality);
               $query->where("app_nationality='".$nationalityList."'");
           } 
         }
         else
         {
              $searchCriterias[++$count]['name']= "Nationality";
              $searchCriterias[$count]['items'][] = $filterParameters["app_nationality_othertext"];
              $query->where("lower(app_nationality) like  lower('".trim($filterParameters["app_nationality_othertext"])."')");
         }
      }
      
        $approval_status= $filterParameters["approval_status"];
       if(is_array($approval_status))
       if(count($approval_status)>0)
       {
           $searchCriterias[++$count]['name']= "Approval Status";
           $searchCriterias[$count]['items']=$approval_status;
           $approvalStatusList= implode("' or approval_status='", $approval_status);
           $query->where("approval_status='".$approvalStatusList."'");
       }
        $applicant_ids= trim($filterParameters["applicant_id"]);
        if($applicant_ids !="")
        {
            $applicantIdList =explode(",",$applicant_ids);
            if(is_array($applicantIdList))
             if(count($applicantIdList)>0)
            {
             $searchCriterias[++$count]['name']= "Applicant Id";
             $searchCriterias[$count]['items']=$applicantIdList;
             $query->where("applicant_id in ('".implode("','",$applicantIdList)."')");
             }
       
        }
       // admin search filters 
        
        
         $app_first_name= addslashes(trim($filterParameters["app_first_name"]));
       if($app_first_name!="")
       {
            $searchCriterias[++$count]['name']= "First Name";
            $searchCriterias[$count]['items'][]=$app_first_name;
          $query->where("lower(app_first_name)   LIKE lower('%$app_first_name%')");
       }
        
       $app_last_name= addslashes(trim($filterParameters["app_last_name"]));
       if($app_last_name!="")
       {
           $searchCriterias[++$count]['name']= "Last Name";
           $searchCriterias[$count]['items'][]=$app_last_name;
          $query->where("lower(app_last_name)   LIKE lower('%$app_last_name%')");
       } 
      
       $app_city= trim($filterParameters["app_city"]);
        if($app_city !="")
        {
            $cityList =explode(",",$app_city);
             if(is_array($cityList))
             if(count($cityList)>0)
            {
              $searchCriterias[++$count]['name']= "City";
              $searchCriterias[$count]['items']=$cityList;
              $query->where("lower(app_city) LIKE lower('%".implode("%') or lower(app_city) LIKE lower('%", $cityList)."%')");
       
             }
       
        }
       
       
     $app_language= $filterParameters["app_language"];
      if(is_array($app_language))
      if(!in_array("Any", $app_language))
      {
	 if(!in_array("Other", $app_language))
         {
           if(count($app_language)>0)
           {
               
           $searchCriterias[++$count]['name']= "Language";
           $searchCriterias[$count]['items']=$app_language; 
           $query->where("lower(app_language) like lower('%".implode("%') or lower(app_language) LIKE lower('%", $app_language)."%')");
                      
           } 
         }
         else
         {
           $searchCriterias[++$count]['name']= "Language";
           $searchCriterias[$count]['items'][]=$filterParameters["app_language_othertext"]; 
            $query->where("lower(app_language) like  lower('".trim($filterParameters["app_language_othertext"])."')");
         }
      }
      
     $app_language_des= $filterParameters["app_language_des"];
      if(is_array($app_language_des))
      if(!in_array("Any", $app_language_des))
      {
	 if(!in_array("Other", $app_language_des))
         {
           if(count($app_language_des)>0)
           {
              $searchCriterias[++$count]['name']="Languages Desired";
              $searchCriterias[$count]['items']=$app_language_des;
             $query->where("lower(app_language) like lower('%".implode("%') or lower(app_language) LIKE lower('%", $app_language_des)."%')");
                      
           } 
         }
         else
         {
               $searchCriterias[++$count]['name']="Languages Desired";
              $searchCriterias[$count]['items'][]=$filterParameters["app_language_des_othertext"];
            $query->where("lower(app_language) like  lower('%".addslashes(trim($filterParameters["app_language_des_othertext"])."%')"));
            
         }
      }
   
           
      $app_ethnicity= $filterParameters["app_ethnicity"];
      if(is_array($app_ethnicity))
      if(!in_array("Any", $app_ethnicity))
      {
	 if(!in_array("Other", $app_ethnicity))
         {
           if(count($app_ethnicity)>0)
           {
             $query->where("app_ethnicity='".implode("' or app_ethnicity='", $app_ethnicity)."'");
              $searchCriterias[++$count]['name']="Ethnicity";
              $searchCriterias[$count]['items']=$app_ethnicity;
                      
           } 
         }
         else
         {
            $searchCriterias[++$count]['name']="Languages Desired";
            $searchCriterias[$count]['items'][]=$filterParameters["app_ethnicity_othertext"]; 
            $query->where("lower(app_ethnicity) like  lower('%".addslashes(trim($filterParameters["app_ethnicity"])."%')"));
            
         }
        }
      
      $app_education= $filterParameters["app_education"];
      if(is_array($app_education))
      if(!in_array("Any", $app_education))
      {
	 if(!in_array("Other", $app_education))
         {
           if(count($app_education)>0)
           {
               $searchCriterias[++$count]['name']="Education";
               $searchCriterias[$count]['items']=$app_education;
               $query->where("app_education='".implode("' or app_education='", $app_education)."'");
                      
           } 
         }
         else
         {
              $searchCriterias[++$count]['name']="Education";
            $searchCriterias[$count]['items'][]=$filterParameters["app_education_othertext"]; 
            $query->where("lower(app_education) like  lower('%".addslashes(trim($filterParameters["app_education_othertext"]))."%')");
            
         }
        }
      
          
            
         $app_occupation= strtolower(trim($filterParameters["app_occupation"]));
         if(strtolower($app_occupation)!='any')
        if($app_occupation !="")
        {
            $occupationList =explode(",",$app_occupation);
             if(is_array($occupationList)){
                 
             if(!in_array("any", $occupationList))
             if(count($occupationList)>0)
            {
              $searchCriterias[++$count]['name']="Education";
              $searchCriterias[$count]['items']=$occupationList; 
              $query->where("lower(app_occupation) LIKE lower('%".implode("%') or lower(app_occupation) LIKE lower('%", $occupationList)."%')");
       
             }
              }
       
        }
 
        
      
        $filter = new Zend_Filter_LocalizedToNormalized();
         if($filterParameters["app_annual_income_low"]!="")
         {
             $app_annual_income_low= $filter->filter($filterParameters["app_annual_income_low"]);

             if($app_annual_income_low >=0)
                {
                       
                   $lowerSelected=true;
                   $searchCriterias[++$count]['name']="Income";
                  $searchCriterias[$count]['items'][0] .= " >= ".$app_annual_income_low;
             
               $query->where("app_annual_income >= $app_annual_income_low");
             }
         }
         if($filterParameters["app_annual_income_high"]!="")
         {
             $app_annual_income_high= $filter->filter($filterParameters["app_annual_income_high"]);
             if($app_annual_income_high >=0)
             {
                 
               if($lowerSelected)
               {
                   $lowerSelected=false;
                  $searchCriterias[$count]['items'][0] .= " And  <=".$app_annual_income_high;
               }
               else
               {
                     
                  $searchCriterias[++$count]['name']="Income";
                  $searchCriterias[$count]['items'][0] = " <= ".$app_annual_income_high;
             
               }
               $query->where("app_annual_income <= $app_annual_income_high");
             }
         }
 
      
         $app_no_of_children_from= $filterParameters["app_no_of_children_from"];
         if($app_no_of_children_from !="")
         {
            $lowerSelected=true;
            $searchCriterias[++$count]['name']="Children";
            $searchCriterias[$count]['items'][0] .= " >= ".$app_no_of_children_from;
           $query->where("app_no_of_children >= $app_no_of_children_from");
         }
         
         $app_no_of_children_to= $filterParameters["app_no_of_children_to"];
         if($app_no_of_children_to !="")
         {
            if($lowerSelected)
               {
                   $lowerSelected=false;
                  $searchCriterias[$count]['items'][0] .= " And  <=".$app_no_of_children_to;
               }
               else
               {
                     
                  $searchCriterias[++$count]['name']="Children";
                  $searchCriterias[$count]['items'][0] = " <= ".$app_no_of_children_to;
             
               }
            $query->where("app_no_of_children <= '$app_no_of_children_to'");
         }
       
         
      $app_future_kids_status= $filterParameters["app_future_kids_status"];
      if(is_array($app_future_kids_status))
      if(!in_array("Any", $app_future_kids_status))
      {
	 if(!in_array("Other", $app_future_kids_status))
         {
           if(count($app_future_kids_status)>0)
           {
               $searchCriterias[++$count]['name']="Future Kids";
               $searchCriterias[$count]['items'] = $app_future_kids_status;
               
               $query->where("app_future_kids_status='".implode("' or app_future_kids_status='", $app_future_kids_status)."'");
                      
           } 
         }
      
        }
        
      $app_us_status= $filterParameters["app_us_status"];
      if(is_array($app_us_status))
      if(!in_array("Any", $app_us_status))
      {
	 if(!in_array("Other", $app_us_status))
         {
           if(count($app_us_status)>0)
           {
               $searchCriterias[++$count]['name']="US Status";
               $searchCriterias[$count]['items'] = $app_us_status;
                 $query->where("app_us_status='".implode("' or app_us_status='", $app_us_status)."'");
                 
           } 
         }
         else
         {
             $searchCriterias[++$count]['name']="usStatus";
             $searchCriterias[$count]['items'][0] = trim($filterParameters["app_us_status_othertext"]);
            $query->where("lower(app_us_status) like  lower('%".addslashes(trim($filterParameters["app_us_status_othertext"]))."%')");
            
         }
        } 
        
 
      $app_islamic_practice_level= $filterParameters["app_islamic_practice_level"];
      if(is_array($app_islamic_practice_level))
      if(!in_array("Doesnot Matter", $app_islamic_practice_level))
      {
	 if(!in_array("Other", $app_islamic_practice_level))
         {
           if(count($app_islamic_practice_level)>0)
           {
             $searchCriterias[++$count]['name']="Islamic Practice Level";
             $searchCriterias[$count]['items'] = $app_islamic_practice_level;
            
                 $query->where("app_islamic_practice_level='".implode("' or app_islamic_practice_level='", $app_islamic_practice_level)."'");
                 
           } 
         }
    
        }
   
        
     $app_hijab_status= $filterParameters["app_hijab_status"];
         if($app_hijab_status !="")
         {
            $searchCriterias[++$count]['name']="Hijab Status";
            $searchCriterias[$count]['items'][0] = $app_hijab_status;
            $query->where("app_hijab_status = '$app_hijab_status'");
         }
         
        $speak_permission_status= $filterParameters["speak_permission_status"];
       if(is_array($speak_permission_status))
       if(count($speak_permission_status)>0)
       {
          $searchCriterias[++$count]['name']="Speak Permission";
            $searchCriterias[$count]['items'] = $speak_permission_status;
          $query->where("speak_permission_status='".implode("' or speak_permission_status='", $speak_permission_status)."'");
       } 
       
       $picture_status= $filterParameters["picture_status"];
       if(is_array($picture_status))
       if(count($picture_status)>0)
       {
          $Y="(photo_file1 IS NOT NULL or photo_file1 !='')";
          $N="(photo_file1 IS NULL or photo_file1='')";
           if(count($picture_status)<2)
           {
             $searchCriterias[++$count]['name']="Picture Status";
            $searchCriterias[$count]['items'] = $picture_status;
              $query->where($picture_status[0]);
           }
          
        
          
       }  
       
       
       $referred_by= $filterParameters["referred_by"];
      if(is_array($referred_by))
      if(!in_array("Any", $referred_by))
      {
	 if(!in_array("Other", $referred_by))
         {
           if(count($referred_by)>0)
           {
             $searchCriterias[++$count]['name']="Referred By";
            $searchCriterias[$count]['items'] = $referred_by;
                 $query->where("referred_by='".implode("' or referred_by='", $referred_by)."'");
                 
           } 
         }
         else
         {
             $searchCriterias[++$count]['name']="Referred By";
            $searchCriterias[$count]['items'][0] = $filterParameters["referred_by_other"];
            $query->where("lower(referred_by) like  lower('%".addslashes(trim($filterParameters["referred_by_other"]))."%')");
            
         }
        } 
        
        
             
       $app_body_type= $filterParameters["app_body_type"];
      if(is_array($app_body_type))
      if(!in_array("Any", $app_body_type))
      {
	 if(!in_array("Other", $app_body_type))
         {
           if(count($app_body_type)>0)
           {
               $searchCriterias[++$count]['name']="Body type";
                $searchCriterias[$count]['items'] = $app_body_type;
                 $query->where("app_body_type='".implode("' or app_body_type='", $app_body_type)."'");
                 
           } 
         }
        
        }   
       
       
          $app_height_from_feet= $filterParameters["app_height_from_feet"];
          $app_height_from_inch= $filterParameters["app_height_from_inch"];
          $app_from_height=$app_height_from_inch*12+$app_height_from_feet;
         if($app_from_height >=36)
         {
             $lowerSelected=true;
           $searchCriterias[++$count]['name']="Height";
           $searchCriterias[$count]['items'][0] = " >= ".floor($app_from_height/12) ." Feet ".($app_from_height%12)." Inch";
           $query->where("app_height >= $app_from_height");
         }
         
        
         $app_height_to_feet= $filterParameters["app_height_to_feet"];
         $app_height_to_inch= $filterParameters["app_height_to_inch"];
         $app_to_height=$app_height_to_feet*12+$app_height_to_inch;
         if($app_to_height >=36)
         {
             if($lowerSelected)
               {
                   $lowerSelected=false;
                  $searchCriterias[$count]['items'][0] .= " And  <=".$app_height_to_feet ." Feet ".$app_height_to_inch." Inch";
               }
               else
               {
                     
                  $searchCriterias[++$count]['name']="Height";
                  $searchCriterias[$count]['items'][0] = " <= ".$app_height_to_feet ." Feet ".$app_height_to_inch." Inch";
             
               }
           $query->where("app_height <= $app_to_height");
         }
      
       
        $app_skin_color= $filterParameters["app_skin_color"];
      if(is_array($app_skin_color))
      if(!in_array("Any", $app_skin_color))
      {
	 if(!in_array("Other", $app_skin_color))
         {
           if(count($app_skin_color)>0)
           {
                $searchCriterias[++$count]['name']="Skin Color";
                $searchCriterias[$count]['items'] = $app_skin_color;
                $query->where("app_skin_color='".implode("' or app_skin_color='", $app_skin_color)."'");
                 
           } 
         }
        
        }   
       
         $sent_agreement_by= $filterParameters["sent_agreement_by"];
         if(is_array($sent_agreement_by))
       if(count($sent_agreement_by)>0)
       {
           $searchCriterias[++$count]['name']="Agreement Send by";
           $searchCriterias[$count]['items'] = $sent_agreement_by;
           $query->where("sent_agreement_by='".implode("' or sent_agreement_by='", $sent_agreement_by)."'");
       }  
       
        $agreement_sent_status= $filterParameters["agreement_sent_status"];
         if(is_array($agreement_sent_status))
       if(count($agreement_sent_status)>0)
       {
           $searchCriterias[++$count]['name']="Agreement Status";
           $searchCriterias[$count]['items'] = $agreement_sent_status;
           $query->where("agreement_sent_status='".implode("' or agreement_sent_status='", $agreement_sent_status)."'");
       }  
       
       
     
       
        $zip_range= $filterParameters["zip_range"];
        $zip_code= $filterParameters["zip_code"];
       if(is_numeric($zip_range) and is_numeric($zip_code))
       {
           if(($zip_range > 0 ) and ($zip_code >100))
           {
            $zipcodeTbl = new masters_Model_zipcode();
            $data=$zipcodeTbl->get_zip_Details($zip_code, $zip_range);
          
            if(is_array($data['zip']) and count($data['zip'])>0)
            {
          
            $searchCriterias[++$count]['name']="Zip Ramge";
           $searchCriterias[$count]['items'][] = $zip_range ." Miles from ".$zip_code;
            $query->where("app_zip in ('".implode("','",$data['zip'])."')");
         
            }
            
           }
           
       }
      
       //$zipcodeTbl->
           
     $search_all= trim($filterParameters["search_all"]);
     if($search_all !="")
     {
          $searchCriterias[++$count]['name']="Search ALL";
           $searchCriterias[$count]['items'][] = $search_all;
         $applicantIdArray=array();
         $commentsTbl = new admin_Model_comments();
         $emailLogsTbl = new admin_Model_emailLogs();
         $commentsQuery= $commentsTbl->select();
         $emailQuery=$emailLogsTbl->select();
         $commentsQuery->from("comment_information", array("entered_applicant_id","reference_applicant_id"));
         $commentsQuery->where("lower(comment) LIKE lower('%$search_all%')");
         $commentsQuery->group("entered_applicant_id");
         
         $emailQuery->from("email_log_information",array("applicant_id"))
                     ->where("lower(sender_mail_id) LIKE lower('%$search_all%') or
                             lower(receiver_mail_id) LIKE lower('%$search_all%')
                             or lower(message) LIKE lower('%$search_all%') ")
                             ->group("applicant_id");
         foreach($commentsTbl->fetchAll($commentsQuery) as $comments)
         {
             $applicantIdArray[$comments->entered_applicant_id]=$comments->entered_applicant_id;
             $applicantIdArray[$comments->reference_applicant_id]=$comments->reference_applicant_id;
         }  
         
          foreach($emailLogsTbl->fetchAll($emailQuery) as $emailLog)
         {
             $applicantIdArray[$emailLog->applicant_id]=$emailLog->applicant_id;
         } 
          if(is_array($applicantIdArray))
             if(count($applicantIdArray)>0)
            {
             $query->where("applicant_id in ('".implode("','",$applicantIdArray)."')");
             }
             
           
     }
      $eventsearch= trim($filterParameters["eventsearch"]);
      $eventid=trim($filterParameters["eventid"]); 
      if($eventsearch !="" && $eventid!="" )
      {
           $searchCriterias[++$count]['name']="Event Search";
           $searchCriterias[$count]['items'][] = $eventsearch;
          $applicantIdArray=array();
          //events_attended
         $eventsTbl = new masters_Model_eventsattended();
         //	event_id 	applicant_id
         ;
         
          foreach($eventsTbl->fetchAll("event_id = '$eventid'") as $events)
         {
             $applicantIdArray[$events->applicant_id]=$events->applicant_id;
         } 
          if(is_array($applicantIdArray))
             if(count($applicantIdArray)>0)
            {
             $query->where("applicant_id in ('".implode("','",$applicantIdArray)."')");
             }
        
       
      }
      
      
       return  $searchCriterias;
        
    }
     public function advancesearchAction()
     {
         
              $this->view->headScript()
                       ->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.widget.min.js")
                ->appendFile("/js/jquery.ui.position.js")
                ->appendFile("/js/jquery.ui.autocomplete.js")
                 ->appendFile("/js/jquery.formatCurrency.min.js")
                      ->appendFile("/js/searchfields.js")
                       ->appendFile("/js/eventSearch.js");
               
            $this->view->headLink()->appendStylesheet("/css/jquery.ui.autocomplete.css");
               $this->view->headScript()
                       ->appendScript("
               $(document).ready(function(){
                     $('#app_annual_income_high').blur(function()
                        {   
                             $('#app_annual_income_high').formatCurrency({ colorize:true,symbol:'' });
                        });
                         $('#app_annual_income_low').blur(function()
                        {   
                             $('#app_annual_income_low').formatCurrency({ colorize:true,symbol:'' });
                        });
                        });");
        $form = new admin_Form_advanceSearch();
        $this->view->form=$form;
         if($this->getRequest()->isPost())
            
        { 
       
          $this->_forward("index");
        
        }  
     }
     public function profilesearchAction()
     {	
        $form = new admin_Form_pofileSearch();
        $this->view->form=$form;
         if($this->getRequest()->isPost())
            
        { 
             if($this->_getParam('advance') != "")
          {
         
           $this->_redirect("/admin/applicant/advancesearch");
          }
        
          $this->_forward("index");
          
          
        }  
     }
     
     public function logsAction()
     {	
         $applicantId=base64_decode($this->_getParam("applicantId"));
         $profileLogs = new applicant_Model_profileUpdateLogs();
         $query=$profileLogs->select();
         $query->from("applicant_profile_update_information as apui",array("applicant_id","field_name",
             "old_value","new_value","profile_edited_by","profile_edit_date"));
          $query->join("applicant_field_description as afd","apui.field_name=afd.field_name",array("afd.field_name","afd.field_description"));
         $query->where("applicant_id = '".$applicantId."'");
         $query->order("profile_edit_date desc");
         $query->setIntegrityCheck(false);
         $this->view->logs=$profileLogs->fetchAll($query);
         
         
         $applicantDetailsTbl = new applicant_Model_applicantDetails();
        $query = $applicantDetailsTbl->select();
            
         $query->from('applicant_information as ai',array("applicant_id","app_first_name","app_last_name"));
         $query->where("ai.applicant_id ='".$applicantId."'");
        $query->setIntegrityCheck(false);
        $this->view->applicantDetails =$applicantDetailsTbl->fetchRow($query);
        
     }
          public function emaildetailAction()
     {
           $this->view->headScript() 
                          ->appendFile("/js/jquery.ui.core.min.js")
                        ->appendFile("/js/jquery.ui.widget.min.js")
                        ->appendFile("/js/jquery.ui.position.js")
                       ->appendFile("/js/jquery.ui.autocomplete.js")
                       ->appendFile("/js/jquery.base64.min.js")
                       ->appendFile("/js/waypoints.min.js")
                       ->appendFile("/js/emailDetailStaff.js")
                       ->appendFile("/js/ckeditor/ckeditor.js")
                       ->appendFile("/js/jquery.timeago.js")
                       ->appendFile("/js/ckeditor/adapters/jquery.js");
     $this->view->headLink()
             ->appendStylesheet("/js/ckeditor/sample.css")
             ->appendStylesheet("/css/jquery.ui.autocomplete.css"); 
 


       $this->view->headScript()->appendScript("  
           $(document).ready(function() {
          
           $( '#message' ).ckeditor();

           });  ");
       
           
       $s_no=$this->_request->getParam('id');
       $this->view->s_no=$s_no;
       if($s_no!="")
           $s_no=  base64_decode ($s_no);
      $applicantId=$this->_request->getParam('applicantId');
      $this->view->applicantId=$applicantId;
        
       if($applicantId!="")
           $applicantId=  base64_decode ($applicantId);
       
           $auth = Zend_Auth::getInstance();
       if($auth->hasIdentity())
        {
         $storage = $auth->getStorage();
        $storedata=$storage->read();
    
        }
       //  $applicantId=$storedata->applicant_id;
           $applicantDetailTbl = new applicant_Model_applicantDetails();
        $appDetailQuery=$applicantDetailTbl->select();
        $appDetailQuery->from("applicant_information as ai",array("app_first_name","app_last_name","app_email1",
            "app_email2","app_email3","intro_email1","intro_email2","intro_email3","speak_permission_status"));
        $appDetailQuery->where("ai.applicant_id = '".$applicantId."'");
         $appDetailQuery->setIntegrityCheck(false);
         
          $appDetail=$applicantDetailTbl->fetchRow($appDetailQuery);
        $this->view->applicantName=$applicantName=$appDetail->app_first_name." " .$appDetail->app_last_name;
         
        if($appDetail->intro_email1 !="")  $applicantIntroEmails[]=  $appDetail->intro_email1;
        if($appDetail->intro_email2 !="")  $applicantIntroEmails[]=  $appDetail->intro_email2;
        if($appDetail->intro_email3 !="")  $applicantIntroEmails[]=  $appDetail->intro_email3;
        
       
        if($appDetail->app_email1 !="")  $applicantEmails[]=  $appDetail->app_email1;
        if($appDetail->app_email2 !="")  $applicantEmails[]=  $appDetail->app_email2;
        if($appDetail->app_email3 !="")  $applicantEmails[]=  $appDetail->app_email3;
        
       
          $emailLogs= new applicant_Model_emailLogs();
          $query=$emailLogs->select();
        $query->from("email_log_information as el",array("applicant_id","staff_id","sender_mail_id","receiver_mail_id",
            "send_by","send_by_name"=>"if(send_by='A',concat('	Staff(',staff_id,')'),concat(app_first_name,' ',app_last_name))",
            "recv_by_name"=>"if(send_by='S',concat('	Staff(',staff_id,')'),concat(app_first_name,' ',app_last_name))",
            "mailing_date","subject","message","applicant_status"));
        $query->join("applicant_information as ai","el.applicant_id=ai.applicant_id",array("app_first_name","app_last_name"));
        $query->where("el.applicant_id = '".$applicantId."' and s_no='$s_no' ");
        $query->order("mailing_date desc");
        $query->setIntegrityCheck(false);
       
         $this->view->emailLogs=$emailLogs->fetchRow($query);
           if($appDetail->speak_permission_status=='Y')
        {
            if($this->view->emailLogs->send_by="A")
            {
             if(in_array($this->view->emailLogs->sender_mail_id, $applicantIntroEmails))
             {
               $this->view->onbehalf=true;
               $applicantEmails=$applicantIntroEmails;
             }
            }
            else
            {
                if(in_array($this->view->emailLogs->receiver_mail_id, $applicantIntroEmails))
             {
               $this->view->onbehalf=true;
               $applicantEmails=$applicantIntroEmails;
             }
            }
        }
       
             $toEmailId=implode(",",$applicantEmails);
                    if($this->getRequest()->isPost())
        { 
               try{
      
        $staffLoginTbl = new masters_Model_stafflogininformation();
         $staffRow= $staffLoginTbl->find($storedata->staff_id)->current();
         $mail= new Zend_Mail();
       $mail->setFrom($staffRow->email,$staffRow->name);
      
       $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/settings.ini','settings');
       $template=$config->emailtemplate->staffemail;
       $html = new Zend_View();
       $html->setScriptPath(APPLICATION_PATH ."/emailTemplates");
       $message=$this->_getParam("message");
       $html->assign('name', $staffRow->name." (".$staffRow->staff_id.")");
       $html->assign('message', stripcslashes($message));
        $html->assign('applicantName',$applicantName);
       $bodyText = $html->render($template);
        
       $subject=$this->_getParam("subject");
       
       
        $mail->setSubject($subject);
       
       $mail->addTo($toEmailId);
      
       $mail->setBodyHtml($bodyText);
      
        $mail->send();
        $row=$emailLogs->createRow();
        
       	$row->applicant_id=$applicantId;
        $row->staff_id=$storedata->staff_id;
        $row->sender_mail_id=$staffRow->email;
        $row->receiver_mail_id=$toEmailId;
        
        $row->send_by="S";
        $row->mailing_date=date("Y-m-d G:i:s");
        $row->subject=$subject;
        $row->message=$message;
        $row->save();
        
        $applicantRow=$applicantDetailTbl->find($applicantId)->current();
        $applicantRow->last_activity_date=date("Y-m-d G:i:s");
        $applicantRow->save();
        $this->view->mailSentFlag="Sucess";
       
               }catch(Exception $e)
               {
                   $this->view->mailSentFlag="Failure";
                    
               }
        }
         
         if($this->view->emailLogs!="")
         {
             $emailLogs->update(array("admin_status"=>1), "s_no='$s_no'");
         }
     }
       public function getemailsAction()
     {
        $this->_helper->layout()->disableLayout();
        Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);  
        
            $auth = Zend_Auth::getInstance();
       if($auth->hasIdentity())
        {
         $storage = $auth->getStorage();
        $storedata=$storage->read();
    
        }
           
            $applicantId=$this->_getParam("applicantId");
    
          $emailLogs= new applicant_Model_emailLogs();
          $query=$emailLogs->select();
        $query->from("email_log_information as el",array("applicant_id","staff_id","sender_mail_id","receiver_mail_id",
            "send_by","recv_by_name"=>"if(el.send_by='A',concat('	Staff(',el.staff_id,')'),concat(app_first_name,' ',app_last_name))",
            "send_by_name"=>"if(el.send_by='S',concat('	Staff(',el.staff_id,')'),concat(app_first_name,' ',app_last_name))",
            "mailing_date","subject","s_no","applicant_status"));
        $query->join("applicant_information as ai","el.applicant_id=ai.applicant_id",array("app_first_name","app_last_name"));
          $query->joinLeft("email_log_information as el2", "el.applicant_id=el2.applicant_id and el.s_no <el2.s_no ",array());
        $query->where("el2.s_no IS NULL"); 
        $query->order("mailing_date desc");
        
        if($applicantId !="")
        {
        $applicantId=  base64_decode ($applicantId);
          $query->where("el.applicant_id = '".$applicantId."'");
        
        }
        else
        {
               $query->group("el.applicant_id");
        }
       
        
        $query->setIntegrityCheck(false);
      
            $adapter =  new Zend_Paginator_Adapter_DbTableSelect($query);
            $paginator = new Zend_Paginator($adapter);
            $limitCount=$this->_request->getParam('limitCount', 5);
            $paginator->setItemCountPerPage($limitCount);
            $page = $this->_request->getParam('page', 1);
            
            $paginator->setCurrentPageNumber($page);
            $TotalCount=$paginator->getTotalItemCount();
            
           if($page<=$paginator->count())
           {
           $page++;
           
            echo "{\"result\" : true,\"TotalCount\": $TotalCount,\"pageCount\":$page,\"details\":".$paginator->toJson()."}";
           }
           else
           {
                echo "{\"result\" : false,\"TotalCount\": $TotalCount,\"pageCount\":$page,\"details\":".$paginator->toJson()."}";
           
           }
            
     
       
     }
      public function emaillistAction()
    {
          
        
        $this->view->headScript() 
                        ->appendFile("/js/jquery.ui.core.min.js")
                        ->appendFile("/js/jquery.ui.widget.min.js")
                        ->appendFile("/js/jquery.ui.position.js")
                       ->appendFile("/js/jquery.ui.autocomplete.js")
                       ->appendFile("/js/jquery.base64.min.js")
                       ->appendFile("/js/waypoints.min.js")
                       ->appendFile("/js/emailsStaff.js")
                       ->appendFile("/js/ckeditor/ckeditor.js")
                       ->appendFile("/js/jquery.timeago.js")
                       ->appendFile("/js/ckeditor/adapters/jquery.js");
     $this->view->headLink()
             ->appendStylesheet("/js/ckeditor/sample.css")
             ->appendStylesheet("/css/jquery.ui.autocomplete.css"); 
 


       $this->view->headScript()->appendScript("  
           $(document).ready(function() {
          
           $( '#message' ).ckeditor();

           });  ");
   
  
          $auth = Zend_Auth::getInstance();
           $emailLogs= new applicant_Model_emailLogs();
          $applicantDetailTbl = new applicant_Model_applicantDetails();  
       if($auth->hasIdentity())
        {
         $storage = $auth->getStorage();
        $storedata=$storage->read();
    
        }
          $applicantId=$this->_getParam("applicantId");
           $this->view->applicantId=$applicantId;
          $applicantId= base64_decode($applicantId);
          
          if(is_numeric($applicantId))
          {
              
                $appDetailQuery=$applicantDetailTbl->select();
                $appDetailQuery->from("applicant_information as ai",array("app_first_name","app_last_name"));
                $appDetailQuery->where("ai.applicant_id = '".$applicantId."'");
                $appDetailQuery->setIntegrityCheck(false);
                $appDetail=$applicantDetailTbl->fetchRow($appDetailQuery);
                $this->view->applicantName=$appDetail->app_first_name." " .$appDetail->app_last_name." ($applicantId)";
          }
        $emailLogs= new applicant_Model_emailLogs();
           if($this->getRequest()->isPost())
        { 
               
          try{
          $staffLoginTbl = new masters_Model_stafflogininformation();
         $staffRow= $staffLoginTbl->find($storedata->staff_id)->current();
         $applcantIds=array_filter($this->_getParam("applcantIds"));
         //****************************
          $applicantQuery=$applicantDetailTbl->select();
          $applicantQuery->from("applicant_information as ai",array("app_first_name","app_last_name","applicant_id","intro_email1","speak_permission_status",
             "intro_email2","intro_email3","app_email1","app_email2","app_email3"));
         $applicantQuery->where("applicant_id in (".implode(",",$applcantIds).")"); 
       
         $applicantQuery->setIntegrityCheck(false);
        
         $result=$applicantDetailTbl->fetchAll($applicantQuery);
         $applicantLists=array();
         foreach($result as $appDetail)
         {
             $applicantLists[$appDetail->applicant_id]['name'] =$appDetail->app_first_name." " .$appDetail->app_last_name;
             $applicantEmails=array();
        if($appDetail->speak_permission_status=="Y")
        {
        if($appDetail->intro_email1 !="")  $applicantEmails[]=  $appDetail->intro_email1;
        if($appDetail->intro_email2 !="")  $applicantEmails[]=  $appDetail->intro_email2;
        if($appDetail->intro_email3 !="")  $applicantEmails[]=  $appDetail->intro_email3;
        }
         else
         {
        if($appDetail->app_email1 !="")  $applicantEmails[]=  $appDetail->app_email1;
        if($appDetail->app_email2 !="")  $applicantEmails[]=  $appDetail->app_email2;
        if($appDetail->app_email3 !="")  $applicantEmails[]=  $appDetail->app_email3;
         }
          $applicantLists[$appDetail->applicant_id]["emails"]=$applicantEmails;
         }

         //******************************
         $message=$this->_getParam("message");
         $subject=$this->_getParam("subject");
         $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/settings.ini','settings');
       $template=$config->emailtemplate->bulkemail;
       $html = new Zend_View();
       $html->setScriptPath(APPLICATION_PATH ."/emailTemplates");
       $message=$this->_getParam("message");
          foreach($applicantLists as $applicantId=>$applicant)
         {
           
        if(count($applicant['emails'])>0)
        {
       $mail= new Zend_Mail();
       $mail->setFrom($staffRow->email,$staffRow->name);
       $mail->setSubject($subject);
         $html->assign('from', $staffRow->name );
       $html->assign('name', $applicant['name'] );
       $html->assign('message', stripcslashes($message));
       $bodyText = $html->render($template);
       
     
       $mail->addTo(implode(",",$applicant['emails']));
     
       $mail->setBodyHtml($bodyText);
        $mail->send();
        
        $row=$emailLogs->createRow();
        
       	$row->applicant_id=$applicantId;
        $row->staff_id=$storedata->staff_id;
        $row->sender_mail_id=$staffRow->email;
        $row->receiver_mail_id=$applicant['emails'][0];
        
        $row->send_by="S";
        $row->mailing_date=date("Y-m-d G:i:s");
        $row->subject=$subject;
        $row->message=$message;
        $row->save();
        
       /* $applicantRow=$applicantDetailTbl->find($applicantId)->current();
        $applicantRow->last_activity_date=date("Y-m-d G:i:s");
        $applicantRow->save();*/
        }
        $this->view->mailSentFlag="Sucess";
         }
               }catch(Exception $e)
               {
                   echo $e->getMessage();
                   $this->view->mailSentFlag="Failure";
               }
        }
   
        
    }
    
          
  
        public function emailsAction()
    {
        
        $this->view->headScript()
                       ->appendFile("/js/emailsStaff.js")
                       ->appendFile("/js/ckeditor/ckeditor.js")
                       ->appendFile("/js/ckeditor/adapters/jquery.js");
     $this->view->headLink()->appendStylesheet("/js/ckeditor/sample.css"); 
 


       $this->view->headScript()->appendScript("  
           $(document).ready(function() {
           
           $( '#message' ).ckeditor();

           });  ");
   
  
          $auth = Zend_Auth::getInstance();
           $emailLogs= new applicant_Model_emailLogs();
          $applicantDetailTbl = new applicant_Model_applicantDetails();  
       if($auth->hasIdentity())
        {
         $storage = $auth->getStorage();
        $storedata=$storage->read();
    
        }
           $applicantId=base64_decode($this->_getParam("applicantId"));
        $emailLogs= new applicant_Model_emailLogs();
           if($this->getRequest()->isPost())
        { 
               try{
          $staffLoginTbl = new masters_Model_stafflogininformation();
         $staffRow= $staffLoginTbl->find($storedata->staff_id)->current();
       $mail= new Zend_Mail();
       $mail->setFrom($staffRow->email,$staffRow->name);
       $message=$this->_getParam("message");
       $subject=$this->_getParam("subject");
       $mail->setSubject($subject);
      
       $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/settings.ini','settings');
       $template=$config->emailtemplate->staffemail;
       $html = new Zend_View();
       $html->setScriptPath(APPLICATION_PATH ."/emailTemplates");
       $message=$this->_getParam("message");
       $html->assign('name', $staffRow->name." (".$staffRow->staff_id.")");
       $html->assign('message', stripcslashes($message));
       $bodyText = $html->render($template);
       
       $toEmailId=$this->_getParam("toEmailId");
       $mail->addTo($toEmailId);
        $mail->setBodyHtml($bodyText);
        $mail->send();
        $row=$emailLogs->createRow();
        
       	$row->applicant_id=$applicantId;
        $row->staff_id=$storedata->staff_id;
        $row->sender_mail_id=$staffRow->email;
        $row->receiver_mail_id=$toEmailId;
        
        $row->send_by="S";
        $row->mailing_date=date("Y-m-d G:i:s");
        $row->subject=$subject;
        $row->message=$message;
        $row->save();
        
        $applicantRow=$applicantDetailTbl->find($applicantId)->current();
        $applicantRow->last_activity_date=date("Y-m-d G:i:s");
        $applicantRow->save();
        $this->view->mailSentFlag=true;
               }catch(Exception $e)
               {
                   $this->view->mailSentFlag=false;
               }
        }
        // 
         
        
        $query=$emailLogs->select();
        $query->from("email_log_information as el",array("applicant_id","staff_id","sender_mail_id","receiver_mail_id",
            "send_by","send_by_name"=>"if(send_by='A',concat('	Staff(',staff_id,')'),concat(app_first_name,' ',app_last_name))",
            "recv_by_name"=>"if(send_by='S',concat('	Staff(',staff_id,')'),concat(app_first_name,' ',app_last_name))",
            "mailing_date","subject","message"));
        $query->join("applicant_information as ai","el.applicant_id=ai.applicant_id",array());
        
        $query->where("el.applicant_id = '".$applicantId."'");
        $query->order("mailing_date desc");
        $query->setIntegrityCheck(false);
        
        $applicantDetailTbl = new applicant_Model_applicantDetails();
        $appDetailQuery=$applicantDetailTbl->select();
        $appDetailQuery->from("applicant_information as ai",array("app_first_name","app_last_name","app_email1",
            "app_email2","app_email3","intro_email1","intro_email2","intro_email3","speak_permission_status"));
        $appDetailQuery->where("ai.applicant_id = '".$applicantId."'");
         $appDetailQuery->setIntegrityCheck(false);
       // $this->view->showEmailId=true;
         
      $registry = Zend_Registry::getInstance();
      $acl=  $registry->get('acl'); 
      $role= $registry->get('role'); 
       $this->view->showEmailId=$acl->isAllowed($role, 'admin'.":"."applicant", 'checkEmailAcc');
     
       
      
        $this->view->emailLogs=$emailLogs->fetchAll($query);
        $appDetail=$applicantDetailTbl->fetchRow($appDetailQuery);
        $this->view->applicantName=$appDetail->app_first_name." " .$appDetail->app_last_name;
        
        $this->view->applicantEmails=array();
        if($appDetail->app_email1 !="")  $this->view->applicantEmails[]=  $appDetail->app_email1;
        if($appDetail->app_email2 !="")  $this->view->applicantEmails[]=  $appDetail->app_email2;
        if($appDetail->app_email3 !="")  $this->view->applicantEmails[]=  $appDetail->app_email3;
        $this->view->IntroEmails=array();
        if($appDetail->speak_permission_status=='Y')
        {
        if($appDetail->intro_email1 !="")  $this->view->IntroEmails[]=  $appDetail->intro_email1;
        if($appDetail->intro_email2 !="")  $this->view->IntroEmails[]=  $appDetail->intro_email2;
        if($appDetail->intro_email3 !="")  $this->view->IntroEmails[]=  $appDetail->intro_email3;
        }
       
        
        
     
    }
     public function matchesAction()
     {	
         $appMatchTbl = new applicant_Model_matchInformation();
         $query=$appMatchTbl->select();
         $auth = Zend_Auth::getInstance();
		 if($auth->hasIdentity())
		 {
			$storage = $auth->getStorage();
			$storedata=$storage->read();
			//annsys start 3-10-2012
			$identity = $auth->getIdentity();
			if(isset($identity->role))
			{
				$groupRole=strtolower($identity->group_role);
				$user_role=strtolower($identity->user_role);
			}
			//annsys end
		 }
		 //annsys start 3-10-2012
		 $this->view->maker_access=array();
		 $this->view->current_user=$groupRole;
		 if($groupRole=='matchmaker')
		 {
			$userApplicantAccessList=array();
            $userApplicantAccessTab = new admin_Model_userapplicantaccess();
			if($user_role!='')
			{
				$query = $userApplicantAccessTab->select();
				$query->from("user_applicant_access",array('applicant_lists'));
				$query->where( "staff_id='".$user_role."'");
				$query->setIntegrityCheck(false);
	  
				$userApplicantAccess=$userApplicantAccessTab->fetchRow($query);
				if($userApplicantAccess->applicant_lists !="")
				{
				   $userApplicantAccessList=  unserialize($userApplicantAccess->	applicant_lists);
				   $this->view->maker_access=$userApplicantAccessList;
				}
				
			}		
		 }
		
         $applicantId=base64_decode($this->_getParam("applicantId")); 
         $this->view->applicantId=$applicantId;
         $query->from("match_information as mi",array("entered_applicant_id","reference_applicant_id"
             ,"match_maked_by","match_maked_date"));
         $query->join("applicant_information as ai",
                 "((ai.applicant_id = mi.reference_applicant_id )and (mi.entered_applicant_id='$applicantId' )) or
                  ((ai.applicant_id = mi.entered_applicant_id)and (mi.reference_applicant_id='$applicantId' ))",
                 array("applicant_id","app_gender","app_first_name", "app_last_name",
                     "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
                     "app_nationality","app_us_status","registration_date",
             "app_marital_status","last_activity_date"));
		
           $query->order(array("app_first_name","app_last_name")); 
           $query->setIntegrityCheck(FALSE);
           $this->view->matchData= $appMatchTbl->fetchAll($query);
           
         
     }
     
      public function profileeditAction()
    {	
         
           $this->view->headScript()
                       ->appendFile("/js/profile.js"); 
        $applicantDetailsTbl = new applicant_Model_applicantDetails();
        $applicantId= $this->_getParam('applicantId');
        $this->view->applicantId= $applicantId;
       $data =array();
        
        $row=$applicantDetailsTbl->fetchRow("applicant_id = '$applicantId'");
        if($row !=""){ 
            $this->view->pofileFound=true;
            $data=$row->toArray();
        
        $profileDetails= new stdClass();
        
        if(count($data>0))
        {
        foreach($data as $field => $value)
        {
           $profileDetails->$field =$value; 
        }
         
        $eventsTbl = new masters_Model_events();
        $eventsQuery=$eventsTbl->select();
        $eventsQuery->from("events as ev",array("event_id","host","location","date"=>"DATE_FORMAT(date,'%D&nbsp;%b&nbsp;%Y')","from_time","to_time"));
        $eventsQuery->join("events_attended as eva","ev.event_id=eva.event_id",array());
        $eventsQuery->where("eva.applicant_id= '$applicantId'");
        $eventsQuery->setIntegrityCheck(false);
        
        $this->view->eventslist=$eventsTbl->fetchAll($eventsQuery);
         $currency=new Zend_Currency();
         $currency->setLocale("en_US");
         $currency->toCurrency();
	 $currency->setValue($profileDetails->app_annual_income);
         $profileDetails->app_annual_income =  $currency->toCurrency();
         
       
        
         //Applicant Profile Heading
	
	//$this->view->app_phone1_type =  $data['app_phone1_type'];
        $profileDetails->app_phone1=Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone1);
       	$profileDetails->app_phone2 = ($profileDetails->app_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone2));
	$profileDetails->app_phone3 = ($profileDetails->app_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone3));
	
	 if(trim($profileDetails->match_religion) == 'Not Specified') $profileDetails->match_religion='';
         if(trim($profileDetails->match_age_low) == 'Not Specified') $profileDetails->match_age_low='';
	 if(trim($profileDetails->match_age_high) == 'Not Specified') $profileDetails->match_age_high='';
	 if(trim($profileDetails->match_nationality) == 'Not Specified') $profileDetails->match_nationality='';
	 if(trim($profileDetails->match_ethnicity) == 'Not Specified') $profileDetails->match_ethnicity='';
	 if(trim($profileDetails->match_state) == 'Not Specified') $profileDetails->match_state='';
	 if(trim($profileDetails->match_us_status) == 'Not Specified') $profileDetails->match_us_status='';
	 if(trim($profileDetails->match_us_living_years) == 'Not Specified') $profileDetails->match_us_living_years='';
	 if(trim($profileDetails->match_language_must) == 'Not Specified') $profileDetails->match_language_must='';
	 if(trim($profileDetails->match_language_desirable) == 'Not Specified') $profileDetails->match_language_desirable='';
	 if(trim($profileDetails->match_height_low) == 'Not Specified') $profileDetails->match_height_low='';
	 if(trim($profileDetails->match_height_high) == 'Not Specified') $profileDetails->match_height_high='';
	 if(trim($profileDetails->match_skin_color) == 'Not Specified') $profileDetails->match_skin_color='';
	 if(trim($profileDetails->match_islamic_practice_level) == 'Not Specified') $profileDetails->match_islamic_practice_level='';
	 if(trim($profileDetails->match_hijab_status) == 'Not Specified') $profileDetails->match_hijab_status='';
	 if(trim($profileDetails->match_kid_status) == 'Not Specified') $profileDetails->match_kid_status='';
	 if(trim($profileDetails->match_other_requirement) == 'Not Specified') $profileDetails->match_other_requirement='';

	//Match Education & Carer
	if(trim($profileDetails->match_education) == 'Not Specified') $profileDetails->match_education='';
	if(trim($profileDetails->match_occupation) == 'Not Specified') $profileDetails->match_occupation='';
         $currency->setValue($profileDetails->match_annual_income_low);
         $currency->toCurrency();
	$profileDetails->match_annual_income_low = ($profileDetails->match_annual_income_low ==""? "": ("From: ". $currency->toCurrency()));
	
        $currency->setValue($profileDetails->match_annual_income_high);
        $profileDetails->match_annual_income_high = ($profileDetails->match_annual_income_high==""? "": ("To: ".$currency->toCurrency()));
	
        

	$profileDetails->intro_phone1 = ($profileDetails->intro_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone1));
	$profileDetails->intro_phone2 = ($profileDetails->intro_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone2));
	$profileDetails->intro_phone3 = ($profileDetails->intro_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone3));
	
	$profileDetails->nf_ref_phone1 = ($profileDetails->nf_ref_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone1));
	
	$profileDetails->nf_ref_phone2 = ($profileDetails->nf_ref_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone2));
	$profileDetails->nf_ref_phone3 = ($profileDetails->nf_ref_phone3  == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone3));
	$this->view->profileDetails=$profileDetails;
        
        }
        else
        {
             $this->view->pofileFound=false;
        }
    }
    }
    
         public function updatephotosAction()
    {
            
         $this->view->headScript()->appendFile("/js/updatephotos.js")
                 ->appendFile("/js/applpassword.js")
               ->appendFile("/js/capchaGenerate.js");
          $applicantDetailsTbl = new applicant_Model_applicantDetails();
        
      
         $applicantId=$this->_getParam("applicantId");
        $action=$this->_getParam("act");
         $data =$applicantDetailsTbl->fetchRow("applicant_id = '$applicantId'");
        if($action =="delete")
        {
            $photoId=$this->_getParam("photo");
            if($photoId!="")
            {
                 for($i=1;$i<=5;$i++)
                    {

                    if($data->{"photo_file$i"} ==$photoId )
                    {
                       
                        for($j=$i;$j<5;$j++)
                        {
                           $data->{"photo_file$j"}=$data->{"photo_file".($j+1)};
                        }
                       unlink(APPLICATION_PATH."/upload/".$photoId);
                       $data->{"photo_file$j"}="";
                       
                        $data->save();
                     }
                            
                    }
                    }
            }
        
           
        
       
      
        $fileUploadCount=5;
        $photofiles=array();
        $fileCount=1;
        for($i=1;$i<=5;$i++)
        {
                        
        if($data->{"photo_file$i"} != "" )
        {
            $photofiles[]= $data->{"photo_file$i"};
            $fileCount++;
            $fileUploadCount--;    
        }
        }
       
        
        $uploadForm= new  applicant_Form_imageupload($fileUploadCount);
        
         if($this->getRequest()->isPost())
        {   
            try{
             if($uploadForm->isValid($_POST))
                     {
                       
                        $upload = new Zend_File_Transfer_Adapter_Http();
                        $files = $upload->getFileInfo();
                        foreach ($files as $file => $info) {
                            if(isset($uploadForm->$file))
                             if (!$uploadForm->$file->receive()) { print "Upload error";}
                             else {
                                  $uploadForm->getValue($file);
                                 
                                   $data->{"photo_file$fileCount"}=$uploadForm->getValue($file);
                               
                                     $photofiles[]= $data->{"photo_file$fileCount"};
                                     $fileCount++;
                                     $fileUploadCount--; 
                                 }
                                  $data->save();
                               }
                    }
            }catch(Exception $e) { $this->view->uploadErr="Unable To Upload please try again";}
        }
         $this->view->photofiles=$photofiles;
         $this->view->fileUploadCount=$fileUploadCount; 
         $this->view->data =$data;
        $uploadForm= new  applicant_Form_imageupload($fileUploadCount);
        $this->view->form=$uploadForm;
      
            
    }  
      
   public function editprofileAction()
    {	
        $this->view->headScript() 
                   ->appendFile("/js/jquery.maskedinput-1.0.js")
                   ->appendFile("/js/ui.core.js")
                   ->appendFile("/js/jquery.ui.core.min.js")
                   ->appendFile("/js/jquery.ui.core.min.js")
                   ->appendFile("/js/jquery.ui.widget.min.js")
                   ->appendFile("/js/jquery.datepick.min.js")
                   ->appendFile("/js/jquery.ui.timepicker.js?v=0.2.9")
                   ->appendFile("/js/jquery.numeric.js")
                   ->appendFile("/js/capchaGenerate.js")
                   ->appendFile("/js/profile.js")
                   ->appendFile("/js/applicantedit.js");
         $this->view->headLink()
                  ->appendStylesheet("/css/jquery-ui-1.8.14.custom.css")
                  ->appendStylesheet("/css/jquery.ui.timepicker.css")
                  ->appendStylesheet('/css/jquery.datepick.css')
                  ->appendStylesheet('/css/editprofile.css');
    
      $applicantDetailsTbl = new applicant_Model_applicantDetails();
      $form = new Form_joinform();
      $applicantId=$this->_getParam("applicantId");
      $dbData=$applicantDetailsTbl->fetchRow("applicant_id = '$applicantId'")->toArray();
     
      $profileDetails= new stdClass();
       if(count($dbData>0))  {   
        foreach($dbData as $field => $value)
         { 
           $profileDetails->$field =$value; 
         }
        $this->view->applicantId=$applicantId;
        $this->view->photo_file1= $profileDetails->photo_file1;
        $this->view->photo_file2= $profileDetails->photo_file2;
        $this->view->photo_file3= $profileDetails->photo_file3;
        $this->view->photo_file4= $profileDetails->photo_file4;
        $this->view->photo_file5= $profileDetails->photo_file5;
        
         $currency=new Zend_Currency();
         $currency->setLocale("en_US");
          $currency->setFormat(array('display' => Zend_Currency::NO_SYMBOL));
         $currency->toCurrency();
	 $currency->setValue($profileDetails->app_annual_income);
         $profileDetails->app_annual_income =  $currency->toCurrency();
         
          $profileDetails->ph1part1=  substr($profileDetails->app_phone1,0,-7);
          $profileDetails->ph1part2= substr($profileDetails->app_phone1,3,-4);
          $profileDetails->ph1part3=substr($profileDetails->app_phone1,-4);
          $profileDetails->ph2part1=  substr($profileDetails->app_phone2,0,-7);
          $profileDetails->ph2part2= substr($profileDetails->app_phone2,3,-4);
          $profileDetails->ph2part3=substr($profileDetails->app_phone2,-4);
          $profileDetails->ph3part1=  substr($profileDetails->app_phone3,0,-7);
          $profileDetails->ph3part2= substr($profileDetails->app_phone3,3,-4);
          $profileDetails->ph3part3=substr($profileDetails->app_phone3,-4);
          
          if($profileDetails->speak_permission_status=="Y"){
          $profileDetails->introph1part1=  substr($profileDetails->intro_phone1,0,-7);
          $profileDetails->introph1part2= substr($profileDetails->intro_phone1,3,-4);
          $profileDetails->introph1part3=substr($profileDetails->intro_phone1,-4);
          $profileDetails->introph2part1=  substr($profileDetails->intro_phone2,0,-7);
          $profileDetails->introph2part2= substr($profileDetails->intro_phone2,3,-4);
          $profileDetails->introph2part3=substr($profileDetails->intro_phone2,-4);
          $profileDetails->introph3part1=  substr($profileDetails->intro_phone3,0,-7);
          $profileDetails->introph3part2= substr($profileDetails->intro_phone3,3,-4);
          $profileDetails->introph3part3=substr($profileDetails->intro_phone3,-4);
          }
          $profileDetails->app_gender=trim($profileDetails->app_gender);
          
          if($profileDetails->app_dob!=""){
          $dob = explode("-", $profileDetails->app_dob);
             
          $profileDetails->dobdt = $dob[2];
          $profileDetails->dobmt = $dob[1];
          $profileDetails->dobyr = $dob[0];
           $dob= $dob[0]."-". $dob[1]."-". $dob[2];
           $profileDetails->dob=$dob;
          }
          $app_us_status = $profileDetails->app_us_status;
          if(count($app_us_status)>0)
          {
             // check for other option
             if(count($app_us_status)==1)
             {
                $app_us_statuselemt= $form->getElement('app_us_status');
                $app_us_statusoptions =$app_us_statuselemt->getMultiOptions();
                
               
                if(!in_array($app_us_status,$app_us_statusoptions ))
                {
                   
                 $profileDetails->app_us_status="Other";
                 $profileDetails->app_us_status_other=$app_us_status;                        
                 
                }
                else
                {
                    $profileDetails->app_us_status=$app_us_status;
                }
              
              }
         }

        $app_nationality = $profileDetails->app_nationality;
        if(count($app_nationality)>0)
         {

             // check for other option
             if(count($app_nationality)==1)
             {
                $app_nationalityelemt= $form->getElement('app_nationality');
                $app_nationalityoptions =$app_nationalityelemt->getMultiOptions();
               
                if(!in_array($app_nationality,$app_nationalityoptions ))
                {
                     $profileDetails->app_nationality="Other";
               $profileDetails->app_nationality_other=$app_nationality;
              
                 
                }else{
                    $profileDetails->app_nationality=$app_nationality;
                }
              
              }
    }
       $app_ethnicity = $profileDetails->app_ethnicity;
          if(count($app_ethnicity)>0)
         {

             // check for other option
             if(count($app_ethnicity)==1)
             {
                $app_ethnicityelemt= $form->getElement('app_ethnicity');
                $app_ethnicityoptions =$app_ethnicityelemt->getMultiOptions();
               
                if(!in_array($app_ethnicity,$app_ethnicityoptions ))
                {
                     $profileDetails->app_ethnicity="Other";
               $profileDetails->app_ethnicity_other=$app_ethnicity;                        
                 
                }else{
                    $profileDetails->app_ethnicity=$app_ethnicity;
                }
              
              }
      }
         $app_education = trim($profileDetails->app_education);
          if(count($app_education)>0)
         {

             // check for other option
             if(count($app_education)==1)
             {
                $app_educationelemt= $form->getElement('app_education');
                $app_educationoptions =$app_educationelemt->getMultiOptions();
                if(!in_array($app_education,$app_educationoptions ))
                {
                   $profileDetails->app_education="Other";
                   $profileDetails->app_education_other=$app_education;                        
                 
                }
                else{
                    $profileDetails->app_education=$app_education;
                }
              
              }
        }

$referred_by = $profileDetails->referred_by;
          if(count($referred_by)>0)
         {

             // check for other option
             if(count($referred_by)==1)
             {
                $referred_byelemt= $form->getElement('referred_by');
                $referred_byoptions =$referred_byelemt->getMultiOptions();
               
                if(!in_array($referred_by,$referred_byoptions ))
                {
                     $profileDetails->referred_by="Other";
               $profileDetails->referred_by_other=$referred_by;                        
                 
                }else{
                    $profileDetails->referred_by=$referred_by;
                }
              
              }
        }
          $app_language = explode(':',trim($profileDetails->app_language,":"));
          if(count($app_language)>0)
         {

             // check for other option
             if(count($app_language)==1)
             {
                $app_languageelemt= $form->getElement('app_language');
                $app_languageoptions =$app_languageelemt->getMultiOptions();
               
                if(!in_array($app_language[0],$app_languageoptions ))
                {
                  
               $profileDetails->app_language_other=$app_language[0];   
                $profileDetails->app_language=array("Other");
                 
                }else{
                    $profileDetails->app_language=$app_language;
                }
              
              }else{
                  $profileDetails->app_language=$app_language;
                  
              }
              
        }

        $match_nationality = explode(':',trim($profileDetails->match_nationality,":"));
          if(count($match_nationality)>0)
         {

             // check for other option
             if(count($match_nationality)==1)
             {
                $match_nationalityelemt= $form->getElement('match_nationality');
                $match_nationalityoptions =$match_nationalityelemt->getMultiOptions();
               
                if(!in_array($match_nationality[0],$match_nationalityoptions ))
                {
                   
                  
                    if($match_nationality[0]=="Not Specified")
                    {
                         $profileDetails->match_nationality=array("Any");
                    }else{
                 
               $profileDetails->match_nationality_other=$match_nationality[0];   
                $profileDetails->match_nationality=array("Other");
                    }
                 
                }else{
                    $profileDetails->match_nationality=$match_nationality;
                }
              
              }else{
                  $profileDetails->match_nationality=$match_nationality;
                  
              }
              
   }
 
$match_ethnicity = explode(':',trim($profileDetails->match_ethnicity,":"));
          if(count($match_ethnicity)>0)
         {

             // check for other option
             if(count($match_ethnicity)==1)
             {
                $match_ethnicityelemt= $form->getElement('match_ethnicity');
                $match_ethnicityoptions =$match_ethnicityelemt->getMultiOptions();
               
                if(!in_array($match_ethnicity[0],$match_ethnicityoptions ))
                {
                   
                  
                    if($match_ethnicity[0]=="Not Specified")
                    {
                         $profileDetails->match_ethnicity=array("Any");
                    }else{
                 
               $profileDetails->match_ethnicity_other=$match_ethnicity[0];   
                $profileDetails->match_ethnicity=array("Other");
                    }
                 
                }else{
                    $profileDetails->match_ethnicity=$match_ethnicity;
                }
              
              }else{
                  $profileDetails->match_ethnicity=$match_ethnicity;
                  
              }
              
}
 

$match_state = explode(':',trim($profileDetails->match_state,":"));
          if(count($match_state)>0)
         {

             // check for other option
             if(count($match_state)==1)
             {
                $match_stateelemt= $form->getElement('match_state');
                $match_stateoptions =$match_stateelemt->getMultiOptions();
               
                if(!in_array($match_state[0],$match_stateoptions ))
                {
                   
                  
                    if($match_state[0]=="Not Specified")
                    {
                         $profileDetails->match_state=array("Any State");
                    }else{
                 
               $profileDetails->match_state_other=$match_state[0];   
                $profileDetails->match_state=array("Other");
                    }
                 
                }else{
                    $profileDetails->match_state=$match_state;
                }
              
              }else{
                  $profileDetails->match_state=$match_state;
                  
              }
              
}
 
$match_language_must = explode(':',trim($profileDetails->match_language_must,":"));
          if(count($match_language_must)>0)
         {

             // check for other option
             if(count($match_language_must)==1)
             {
                $match_language_mustelemt= $form->getElement('match_language_must');
                $match_language_mustoptions =$match_language_mustelemt->getMultiOptions();
               
                if(!in_array($match_language_must[0],$match_language_mustoptions ))
                {
                   
                  
                    if($match_language_must[0]=="Not Specified")
                    {
                         $profileDetails->match_language_must=array("None");
                    }else{
                 
               $profileDetails->match_language_must_other=$match_language_must[0];   
                $profileDetails->match_language_must=array("Other");
                    }
                 
                }else{
                    $profileDetails->match_language_must=$match_language_must;
                }
              
              }else{
                  $profileDetails->match_language_must=$match_language_must;
                  
              }
              
}
 $profileDetails->match_low_feet=intval($profileDetails->match_height_low/12);

              $profileDetails->match_low_inch=$profileDetails->match_height_low%12;
               $profileDetails->match_high_feet=intval($profileDetails->match_height_high/12);

                $profileDetails->match_high_inch=$profileDetails->match_height_high%12;
                
$match_language_desirable = explode(':',trim($profileDetails->match_language_desirable,":"));
          if(count($match_language_desirable)>0)
         {

             // check for other option
             if(count($match_language_desirable)==1)
             {
                $match_language_desirableelemt= $form->getElement('match_language_desirable');
                $match_language_desirableoptions =$match_language_desirableelemt->getMultiOptions();
               
                if(!in_array($match_language_desirable[0],$match_language_desirableoptions ))
                {
                   
                  
                    if($match_language_desirable[0]=="Not Specified")
                    {
                         $profileDetails->match_language_desirable=array("None");
                    }else{
                 
               $profileDetails->match_language_desirable_other=$match_language_desirable[0];   
                $profileDetails->match_language_desirable=array("Other");
                    }
                 
                }else{
                    $profileDetails->match_language_desirable=$match_language_desirable;
                }
              
              }else{
                  $profileDetails->match_language_desirable=$match_language_desirable;
                  
              }
              
}
 if($profileDetails->app_gender=='Male'){
     $match_job_nature_name = explode(':',trim($profileDetails->match_job_nature_name,":"));
          if(count($match_job_nature_name)>0)
         {

             // check for other option
             if(count($match_job_nature_name)==1)
             {
                $match_job_nature_nameelemt= $form->getElement('match_job_nature_name_female');
                $match_job_nature_nameoptions =$match_job_nature_nameelemt->getMultiOptions();
               
                if(!in_array($match_job_nature_name[0],$match_job_nature_nameoptions ))
                {
                   
                  
                    if($match_job_nature_name[0]=="Not Specified")
                    {
                         $profileDetails->match_job_nature_name=array("Any");
                    }else{
                 
               $profileDetails->match_job_nature_name_other=$match_job_nature_name[0];   
                $profileDetails->match_job_nature_name=array("Other");
                    }
                 
                }else{
                    $profileDetails->match_job_nature_name=$match_job_nature_name;
                }
              
              }else{
                  $profileDetails->match_job_nature_name=$match_job_nature_name;
                  
              }
              
}
 
 }else{
     $match_job_nature_name = explode(':',trim($profileDetails->match_job_nature_name,":"));
          if(count($match_job_nature_name)>0)
         {

             // check for other option
             if(count($match_job_nature_name)==1)
             {
                $match_job_nature_nameelemt= $form->getElement('match_job_nature_name');
                $match_job_nature_nameoptions =$match_job_nature_nameelemt->getMultiOptions();
               
                if(!in_array($match_job_nature_name[0],$match_job_nature_nameoptions ))
                {
                   
                  
                    if($match_job_nature_name[0]=="Not Specified")
                    {
                         $profileDetails->match_job_nature_name=array("Any");
                    }else{
                 
               $profileDetails->match_job_nature_name_other=$match_job_nature_name[0];   
                $profileDetails->match_job_nature_name=array("Other");
                    }
                 
                }else{
                    $profileDetails->match_job_nature_name=$match_job_nature_name;
                }
              
              }else{
                  $profileDetails->match_job_nature_name=$match_job_nature_name;
                  
              }
              
}
 
 }
 $match_education = explode(",",trim($profileDetails->match_education));
 
          if(count($match_education)>0)
         {
         
             // check for other option
            // if(count($match_education)==1)
             {
                $match_educationelemt= $form->getElement('match_education');
                $match_educationoptions =$match_educationelemt->getMultiOptions();
               $otherOptions = array_diff($match_education, $match_educationoptions);
              
                if(count($otherOptions)>0){
                    if($match_education[0]!='Not Specified'){
                  $match_education[]="Other";
                  foreach($otherOptions as $key=>$val)
               $profileDetails->match_education_other=$otherOptions[$key];                        
                 
                }}
                    $profileDetails->match_education=$match_education;
                
              
              }
        }   
        $match_us_status = explode(",",trim($profileDetails->match_us_status));
 
          if(count($match_us_status)>0)
         {
         
             // check for other option
            // if(count($match_us_status)==1)
             {
                $match_us_statuselemt= $form->getElement('match_us_status');
                $match_us_statusoptions =$match_us_statuselemt->getMultiOptions();
               $otherOptions = array_diff($match_us_status, $match_us_statusoptions);
       //   print_r($otherOptions);
                if(count($otherOptions)>0){
                    if($otherOptions[0]!='Not Specified'){
                  $match_us_status[]="Other";
                  foreach($otherOptions as $key=>$val)
            {
                      $profileDetails->match_us_status_other=$otherOptions[$key]; 
                  
                    }
                    }
                }
                    $profileDetails->match_us_status=$match_us_status;
                
              
              }
        }   
  
    $profileDetails->app_feet=intval($profileDetails->app_height/12);

               $profileDetails->app_inch=$profileDetails->app_height%12;
               
            
                if($profileDetails->match_skin_color!="Not Specified"){
$match_skin_color = explode(",",trim($profileDetails->match_skin_color));
 
          if(count($match_skin_color)>0)
         {
         
             // check for other option
            // if(count($match_skin_color)==1)
             {
                $match_skin_colorelemt= $form->getElement('match_skin_color');
                $match_skin_coloroptions =$match_skin_colorelemt->getMultiOptions();
               $otherOptions = array_diff($match_skin_color, $match_skin_coloroptions);
       //   print_r($otherOptions);
                if(count($otherOptions)>0){
                  
                 
                  foreach($otherOptions as $key=>$val)
            {
                      $profileDetails->match_skin_color_other=$otherOptions[$key]; 
                  
                    }
                  
                }
                    $profileDetails->match_skin_color=$match_skin_color;
                
              
              }
        } 
                }

      if($profileDetails->match_islamic_practice_level!="Not Specified"){
$match_islamic_practice_level = explode(",",trim($profileDetails->match_islamic_practice_level));
 
          if(count($match_islamic_practice_level)>0)
         {
         
             // check for other option
            // if(count($match_islamic_practice_level)==1)
             {
                $match_islamic_practice_levelelemt= $form->getElement('match_islamic_practice_level');
                $match_islamic_practice_leveloptions =$match_islamic_practice_levelelemt->getMultiOptions();
               $otherOptions = array_diff($match_islamic_practice_level, $match_islamic_practice_leveloptions);
       //   print_r($otherOptions);
                if(count($otherOptions)>0){
                  
                
                  foreach($otherOptions as $key=>$val)
            {
                      $profileDetails->match_islamic_practice_level_other=$otherOptions[$key]; 
                  
                    }
                  
                }
                    $profileDetails->match_islamic_practice_level=$match_islamic_practice_level;
                
              
              }
        } 
                }
//*********************************888
if(isset($profileDetails->app_children_desc)){
 $child_Array=explode(":", $profileDetails->app_children_desc);
   if(isset($child_Array[0])){
                $sub_array1=explode(",",$child_Array[0]);
if(isset($sub_array1[0]) && isset($sub_array1[1]) && isset($sub_array1[2]))
{
    $profileDetails->app_children_age1 = $sub_array1[0];
    $profileDetails->app_children_sex1 = $sub_array1[1];
    $profileDetails->app_children_live_status1 = $sub_array1[2];
}
   }  if(isset($child_Array[1])){
                $sub_array2=explode(",",$child_Array[1]);
if(isset($sub_array1))
{
    $profileDetails->app_children_age2= $sub_array2[0];
    $profileDetails->app_children_sex2 = $sub_array2[1];
    $profileDetails->app_children_live_status2 = $sub_array2[2];
}
   }
     if(isset($child_Array[2])){
                $sub_array3=explode(",",$child_Array[2]);
if(isset($sub_array3))
{
    $profileDetails->app_children_age3 = $sub_array3[0];
    $profileDetails->app_children_sex3 = $sub_array3[1];
    $profileDetails->app_children_live_status3 = $sub_array3[2];
}
     }  if(isset($child_Array[3])){           $sub_array4=explode(",",$child_Array[3]);
if(isset($sub_array4))
{
    $profileDetails->app_children_age4 = $sub_array4[0];
    $profileDetails->app_children_sex4 = $sub_array4[1];
    $profileDetails->app_children_live_status4 = $sub_array4[2];
}
     }
       if(isset($child_Array[4])){
                $sub_array5=explode(",",$child_Array[4]);
if(isset($sub_array5))
{
    $profileDetails->app_children_age5 = $sub_array5[0];
    $profileDetails->app_children_sex5 = $sub_array5[1];
    $profileDetails->app_children_live_status5 = $sub_array5[2];
}
       }
         if(isset($child_Array[5])){
                $sub_array6=explode(",",$child_Array[5]);
  if(isset($sub_array6))
{
    $profileDetails->app_children_age6= $sub_array6[0];
    $profileDetails->app_children_sex6 = $sub_array6[1];
    $profileDetails->app_children_live_status6 = $sub_array6[2];
}          }    
                }



//***************************************




          $app_islamic_practice_level= $profileDetails->app_islamic_practice_level;
          if(count($app_islamic_practice_level)>0)
          {
              $profileDetails->app_islamic_practice_level=$app_islamic_practice_level;
          }
          
          $currency->setValue($profileDetails->match_annual_income_low);
         $currency->toCurrency();
	$profileDetails->match_annual_income_low = ($profileDetails->match_annual_income_low ==""? "0.00" :  $currency->toCurrency());
	
        $currency->setValue($profileDetails->match_annual_income_high);
        $profileDetails->match_annual_income_high = ($profileDetails->match_annual_income_high==""? "" : $currency->toCurrency());
	
        

	$profileDetails->intro_phone1 = ($profileDetails->intro_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone1));
	$profileDetails->intro_phone2 = ($profileDetails->intro_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone2));
	$profileDetails->intro_phone3 = ($profileDetails->intro_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone3));
	
	$profileDetails->nf_ref_phone1 = ($profileDetails->nf_ref_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone1));
	
	$profileDetails->nf_ref_phone2 = ($profileDetails->nf_ref_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone2));
	$profileDetails->nf_ref_phone3 = ($profileDetails->nf_ref_phone3  == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone3));
	$this->view->profileDetails=$profileDetails;
        
        
        $form->populate(get_object_vars($profileDetails));
            $this->view->form=$form;
    
        }
      if($this->getRequest()->isPost())
         {
             $i=1;
             $form->isValid($_POST);
   
           {
              
             try{
      $data= array();
                $data['app_first_name']=$form->getValue('app_first_name');
                $data['app_last_name']=$form->getValue('app_last_name');
                $data['app_nick_name']=$form->getValue('app_nick_name');
                $data['app_address']=$form->getValue('app_address');
                $data['app_state']=$form->getValue('app_state');
                $data['app_city']=$form->getValue('app_city');
                //$data['app_other_city']=$form->getValue('app_other_city');
                $data['app_zip']=$form->getValue('app_zip');
                $data['app_phone1']=$form->getValue('ph1part1').$form->getValue('ph1part2').$form->getValue('ph1part3');
                $data['app_phone1_type']=$form->getValue('app_phone1_type');
                $data['app_phone2']=$form->getValue('ph2part1').$form->getValue('ph2part2').$form->getValue('ph2part3');
                $data['app_phone2_type']="";
                if($data['app_phone2'] != "" && $data['app_phone2'] != null){
                        $data['app_phone2_type']=$form->getValue('app_phone2_type');
                }
                $data['app_phone3']=$form->getValue('ph3part1').$form->getValue('ph3part2').$form->getValue('ph3part3');
                $data['app_phone3_type']="";
                if($data['app_phone3'] != "" && $data['app_phone3'] != null){
                        $data['app_phone3_type']=$form->getValue('app_phone3_type');
                }
                $data['app_email1']=$form->getValue('app_email1');
                $data['app_email2']=$form->getValue('app_email2');
                $data['app_email3']=$form->getValue('app_email3');
                $data['app_check_mails']=$form->getValue('app_check_mails');
                $data['app_contact_time']=$form->getValue('app_contact_time');
                /////////for 2nd page
                $data['app_religion']=$form->getValue('app_religion');
                $data['app_gender']=$form->getValue('app_gender');
                $dobdt=$form->getValue('dobdt');
                $dobmt=$form->getValue('dobmt');
                $dobyr=$form->getValue('dobyr');
                $dob=$dobyr."-".$dobmt."-".$dobdt;
               
                $data['app_dob']=$dob;
     
                $data['app_nationality']=$form->getValue('app_nationality');
                $app_nationality_other=$form->getValue('app_nationality_other');
                if($data['app_nationality']=='Other'){
                        $data['app_nationality']=$app_nationality_other;
                }
                $data['app_ethnicity']=$form->getValue('app_ethnicity');
                $app_ethnicity_other=$form->getValue('app_ethnicity_other');
                if($data['app_ethnicity']=='Other'){
                        $data['app_ethnicity']=$app_ethnicity_other;
                }
                $data['app_place_of_birth']=$form->getValue('app_place_of_birth');
                $data['app_us_status']=$form->getValue('app_us_status');
                $app_us_status_other=$form->getValue('app_us_status_other');
                if($data['app_us_status']=='Other'){
                        $data['app_us_status']=$app_us_status_other;
                }
                $data['app_us_living_years']=$form->getValue('app_us_living_years');
                $data['app_language']=":".implode(":",$form->getValue('app_language')).":";
                $app_language_other=$form->getValue('app_language_other');
                if(strpos($data['app_language'],"Other")){
                        $data['app_language']=str_replace("Other",$app_language_other,$data['app_language']);
                }
                $data['app_education']=$form->getValue('app_education');
                $app_education_other=$form->getValue('app_education_other');
                if($data['app_education']=='Other'){
                        $data['app_education']=$app_education_other;
                }
                $data['app_occupation']=$form->getValue('app_occupation');
               
                //$data['app_annual_income']=str_replace('$','',$data['app_annual_income']);
                //$data['app_annual_income']=str_replace(',','',$data['app_annual_income']);
                $data['app_annual_income']=round($form->getValue('app_annual_income'));
                if(strval($data['app_annual_income'])==""){
                        $data['app_annual_income']=null;
                }


                if($data['app_gender']=='Female'){
                $data['app_job_nature_name']=$form->getValue('app_job_nature_name_female');
                } else if($data['app_gender']=='Male'){
                    $data['app_job_nature_name']=$form->getValue('app_job_nature_name');
                }
                $app_job_nature_name_other=$form->getValue('app_job_nature_name_other');
                if($data['app_job_nature_name']=='Other'){
                        $data['app_job_nature_name']=$app_job_nature_name_other;
                }
                $data['app_marital_status']=$form->getValue('app_marital_status');
                $app_children_status=$form->getValue('app_children_status');
                $data['app_no_of_children'] =$form->getValue('app_no_of_children');
                $app_children_age1=$form->getValue('app_children_age1');
                $app_children_sex1=$form->getValue('app_children_sex1');
                $app_children_live_status1=$form->getValue('app_children_live_status1');
                $app_children_age2=$form->getValue('app_children_age2');
                $app_children_sex2=$form->getValue('app_children_sex2');
                $app_children_live_status2=$form->getValue('app_children_live_status2');
                $app_children_age3=$form->getValue('app_children_age3');
                $app_children_sex3=$form->getValue('app_children_sex3');
                $app_children_live_status3=$form->getValue('app_children_live_status3');
                $app_children_age4=$form->getValue('app_children_age4');
                $app_children_sex4=$form->getValue('app_children_sex4');
                $app_children_live_status4=$form->getValue('app_children_live_status4');
                $app_children_age5=$form->getValue('app_children_age5');
                $app_children_sex5=$form->getValue('app_children_sex5');
                $app_children_live_status5=$form->getValue('app_children_live_status5');
                $app_children_age6=$form->getValue('app_children_age6');
                $app_children_sex6=$form->getValue('app_children_sex6');
                $app_children_live_status6=$form->getValue('app_children_live_status6');
                $app_children1=$app_children_age1 . "," . $app_children_sex1 . "," . $app_children_live_status1;
                $app_children2=$app_children_age2 . "," . $app_children_sex2 . "," . $app_children_live_status2;
                $app_children3=$app_children_age3 . "," . $app_children_sex3 . "," . $app_children_live_status3;
                $app_children4=$app_children_age4 . "," . $app_children_sex4 . "," . $app_children_live_status4;
                $app_children5=$app_children_age5 . "," . $app_children_sex5 . "," . $app_children_live_status5;
                $app_children6=$app_children_age6 . "," . $app_children_sex6 . "," . $app_children_live_status6;
                if($data['app_no_of_children']=='1'){
                        $data['app_children_desc']=$app_children1;
                }
                if($data['app_no_of_children']=='2'){
                        $data['app_children_desc']=$app_children1 . ":" . $app_children2;
                }
                if($data['app_no_of_children']=='3'){
                        $data['app_children_desc']=$app_children1 . ":" . $app_children2 . ":" . $app_children3;
                }
                if($data['app_no_of_children']=='4'){
                        $data['app_children_desc']=$app_children1 . ":" . $app_children2 . ":" . $app_children3 . ":" . $app_children4;
                }
                if($data['app_no_of_children']=='5'){
                        $data['app_children_desc']=$app_children1 . ":" . $app_children2 . ":" . $app_children3 . ":" . $app_children4 . ":" . $app_children5;
                }
                if($data['app_no_of_children']=='6'){
                        $data['app_children_desc']=$app_children1 . ":" . $app_children2 . ":" . $app_children3 . ":" . $app_children4 . ":" . $app_children5 . ":" . $app_children6;
                }
                $data['app_other_children_desc']=$form->getValue('app_other_children_desc');
                //$data['app_future_kids_status']=$form->getValue('app_future_kids_status');
                $data['app_islamic_practice_level']=$form->getValue('app_islamic_practice_level');
                $data['app_hijab_status']=$form->getValue('app_hijab_status');
                $data['app_body_type']=$form->getValue('app_body_type');
                $app_feet=$form->getValue('app_feet');
                $app_inch=$form->getValue('app_inch');
                $data['app_height']= ($app_feet * 12) + $app_inch;
                $data['app_skin_color']=$form->getValue('app_skin_color');
                $data['app_description']=$form->getValue('app_description');
                ////// from 3rd page

                $data['match_age_high']=$form->getValue('match_age_high');
                if($data['match_age_high']==""){
                        $data['match_age_high']=null;
                }
                $data['match_age_low']=$form->getValue('match_age_low');
                if($data['match_age_low']==""){
                        $data['match_age_low']=null;
                }
                if($form->getValue('match_religion')!="" || $form->getValue('match_religion')!=null)
                $data['match_religion']= ":".implode(":",$form->getValue('match_religion')).":";
                else
                $data['match_religion']=null;


                $data['match_nationality']=":".implode(":",$form->getValue('match_nationality')).":";
                if($data['match_nationality']==":Any:" || $data['match_nationality']=="" || $data['match_nationality']==null){
                        $data['match_nationality']='Not Specified';
                }
                $match_nationality_other=$form->getValue('match_nationality_other');
                if(strpos($data['match_nationality'],"Other")){
                        $data['match_nationality']=str_replace("Other",$match_nationality_other,$data['match_nationality']);
                }

                $data['match_ethnicity']=":".implode(":",$form->getValue('match_ethnicity')).":";
                if($data['match_ethnicity']==":Any:" || $data['match_ethnicity']=="" || $data['match_ethnicity']==null){
                        $data['match_ethnicity']='Not Specified';
                }
                $match_ethnicity_other=$form->getValue('match_ethnicity_other');
                if(strpos($data['match_ethnicity'],"Other")){
                        $data['match_ethnicity']=str_replace("Other",$match_ethnicity_other,$data['match_ethnicity']);
                }
                $data['match_state']=":".implode(":",$form->getValue('match_state')).":";
                if($data['match_state']=="Any State" || $data['match_state']=="" || $data['match_state']==null){
                        $data['match_state']='Not Specified';
                }
               
                if($form->getValue('match_us_status')!="" || $form->getValue('match_us_status')!=null)
                {
                $data['match_us_status']=implode(",",$form->getValue('match_us_status'));
                $match_us_statusOther=$form->getValue('match_us_statusOther');
                 if(strpos($data['match_us_status'],",Other")){
                      $data['match_us_status']=substr($data['match_us_status'],0,strpos($data['match_us_status'],",Other"));
                      $data['match_us_status']= $data['match_us_status'] . "," . $match_us_statusOther;
                  }
                   else if($data['match_us_status'] == "Other"){
                                $data['match_us_status']=$match_us_statusOther;
                        }
                }
                else{
                        $data['match_us_status']='Not Specified';
                }
                  
                $data['match_us_living_years']=$form->getValue('match_us_living_years');
                if($data['match_us_living_years']=="" || $data['match_us_living_years']==null){
                        $data['match_us_living_years']='Not Specified';
                }
                if($form->getValue('match_education')!="" || $form->getValue('match_education')!=null)
                        {
                $data['match_education']=implode(",",$form->getValue('match_education'));
                $match_education_other=$form->getValue('match_education_other');

                //if($data['match_education']!='Not Specified')
                    {
                        if(strpos($data['match_education'],",Other")){
                                $data['match_education']=substr($data['match_education'],0,strpos($data['match_education'],",Other"));
                                $data['match_education']= $data['match_education'] . "," . $match_education_other;
                        }else if($data['match_education']== "Other"){
                                $data['match_education']=$match_education_other;
                        }
                }
                }else{
                // if($data['match_education']=="" || $data['match_education']==null){
                        $data['match_education']='Not Specified';

                }

                $data['match_occupation']=$form->getValue('match_occupation');
                if($data['match_occupation']=="" || $data['match_occupation']==null){
                        $data['match_occupation']='Not Specified';
                }

                
                $data['match_annual_income_low']=str_replace('$','',$data['match_annual_income_low']);
                $data['match_annual_income_low']=str_replace(',','',$data['match_annual_income_low']);
                $data['match_annual_income_low']=round($form->getValue('match_annual_income_low'));
                if(strval($data['match_annual_income_low'])==""){
                        $data['match_annual_income_low']= null;
                }

               
                $data['match_annual_income_high']=str_replace('$','',$data['match_annual_income_high']);
                $data['match_annual_income_high']=str_replace(',','',$data['match_annual_income_high']);
                 $data['match_annual_income_high']=round($form->getValue('match_annual_income_high'));
                if(strval($data['match_annual_income_high'])==""){
                        $data['match_annual_income_high']= null;
                }
                $match_job_nature_name_other=$form->getValue('match_job_nature_name_other');

                if($data['app_gender']=='Female'){
                $match_job_nature_name_female=":".implode(":",$form->getValue('match_job_nature_name_female')).":";
                        if($match_job_nature_name_female==":Any:" || $match_job_nature_name_female=="" || $match_job_nature_name_female==null){
                                $data['match_job_nature_name']='Not Specified';
                        }
                        //$match_job_nature_name_other=$form->getValue('match_job_nature_name_other');
                        if(strpos($match_job_nature_name_female,"Other")){
                                $data['match_job_nature_name']=str_replace("Other",$match_job_nature_name_other,$match_job_nature_name_female);
                        }
                        $data['match_job_nature_name']=$match_job_nature_name_female;
                } else if($data['app_gender']=='Male'){
                $match_job_nature_name= ":".implode(":",$form->getValue('match_job_nature_name')).":";
                        if($match_job_nature_name==":Any:" || $match_job_nature_name=="" || $match_job_nature_name==null){
                                $data['match_job_nature_name']='Not Specified';
                        }
                        //$match_job_nature_name_other=$form->getValue('match_job_nature_name_other');
                        if(strpos($match_job_nature_name,"Other")){
                                $data['match_job_nature_name']=str_replace("Other",$match_job_nature_name_other,$data['match_job_nature_name']);
                        }
                }
                //print_r($form->getValue('match_language_must'));
                //print_r($form->getValue('match_language_desirable'));
                $data['match_language_must']=":".implode(":",$form->getValue('match_language_must')).":";
                if($data['match_language_must']==":None:" || $data['match_language_must']=="" || $data['match_language_must']==null){
                        $data['match_language_must']='Not Specified';
                }
                $match_language_must_other=$form->getValue('match_language_must_other');
                if(strpos($data['match_language_must'],"Other")){
                        $data['match_language_must']=str_replace("Other",$match_language_must_other,$data['match_language_must']);
                }
                $data['match_language_desirable']=":".implode(":",$form->getValue('match_language_desirable')).":";
                if($data['match_language_desirable']==":None:" || $data['match_language_desirable']=="" || $data['match_language_desirable']==null){
                        $data['match_language_desirable']='Not Specified';
                }
                $match_language_desirable_other=$form->getValue('match_language_desirable_other');
                if(strpos($data['match_language_desirable'],"Other")){
                        $data['match_language_desirable']=str_replace("Other",$match_language_desirable_other,$data['match_language_desirable']);
                }
                $match_low_feet=$form->getValue('match_low_feet');
                $match_low_inch=$form->getValue('match_low_inch');
                $data['match_height_low']=($match_low_feet * 12) + $match_low_inch;
                $match_high_feet=$form->getValue('match_high_feet');
                $match_high_inch=$form->getValue('match_high_inch');
                $data['match_height_high']=($match_high_feet * 12) + $match_high_inch;
                if($form->getValue('match_islamic_practice_level')!="" || $form->getValue('match_islamic_practice_level')!=null)
                $data['match_islamic_practice_level']=implode(",",$form->getValue('match_islamic_practice_level'));
                else
                $data['match_islamic_practice_level']='Not Specified';


                if($form->getValue('match_skin_color')!="" || $form->getValue('match_skin_color')!=null)
                $data['match_skin_color']=implode(",",$form->getValue('match_skin_color'));
                else
                $data['match_skin_color']='Not Specified';


                $data['match_hijab_status']=$form->getValue('match_hijab_status');
                if($data['match_hijab_status']=="" || $data['match_hijab_status']==null){
                        $data['match_hijab_status']='Not Specified';
                }
                $data['match_kid_status']=$form->getValue('match_kid_status');
                if($data['match_kid_status']=="" || $data['match_kid_status']==null){
                        $data['match_kid_status']='Not Specified';
                }
                $data['match_other_requirement']=$form->getValue('match_other_requirement');
                if($data['match_other_requirement']=="" || $data['match_other_requirement']==null){
                        $data['match_other_requirement']='Not Specified';
                }
                /////////////from page 4
                $data['speak_permission_status']=$form->getValue('speak_permission_status');
                $data['permitted_to']=$form->getValue('permitted_to');
                $data['intro_first_name']=$form->getValue('intro_first_name');
                $data['intro_last_name']=$form->getValue('intro_last_name');
                $data['intro_nick_name']=$form->getValue('intro_nick_name');
                $data['intro_address']=$form->getValue('intro_address');
                $data['intro_state']=$form->getValue('intro_state');
                $data['intro_city']=$form->getValue('intro_city');
                $data['intro_zip']=$form->getValue('intro_zip');
                $data['intro_phone1']=$form->getValue('introph1part1').$form->getValue('introph1part2').$form->getValue('introph1part3');
                if($data['intro_phone1'] != "" && $data['intro_phone1'] != null){
                        $data['intro_phone1_type']=$form->getValue('intro_phone1_type');
                }
                $data['intro_phone1_type']=$form->getValue('intro_phone1_type');
                $data['intro_phone2']=$form->getValue('introph2part1').$form->getValue('introph2part2').$form->getValue('introph2part3');
                $data['intro_phone2_type']="";
                if($data['intro_phone2'] != "" && $data['intro_phone2'] != null){
                        $data['intro_phone2_type']=$form->getValue('intro_phone2_type');
                }
                $data['intro_phone3']=$form->getValue('introph3part1').$form->getValue('introph3part2').$form->getValue('introph3part3');
                $data['intro_phone3_type']="";
                if($data['intro_phone3'] != "" && $data['intro_phone3'] != null){
                        $data['intro_phone3_type']=$form->getValue('intro_phone3_type');
                }

                $data['intro_email1']=$form->getValue('intro_email1');
                $data['intro_email2']=$form->getValue('intro_email2');
                $data['intro_email3']=$form->getValue('intro_email3');
                $data['intro_contact_time']=$form->getValue('intro_contact_time');
                $data['nf_ref_name1']=$form->getValue('nf_ref_name1');
                $data['nf_ref_phone1']=$form->getValue('nfrefph1part1').$form->getValue('nfrefph1part2').$form->getValue('nfrefph1part3');
                $data['nf_ref_email1']=$form->getValue('nf_ref_email1');
                $data['nf_ref_name2']=$form->getValue('nf_ref_name2');
                $data['nf_ref_phone2']=$form->getValue('nfrefph2part1').$form->getValue('nfrefph2part2').$form->getValue('nfrefph2part3');
                $data['nf_ref_email2']=$form->getValue('nf_ref_email2');
                $data['nf_ref_name3']=$form->getValue('nf_ref_name3');
                $data['nf_ref_phone3']=$form->getValue('nfrefph3part1').$form->getValue('nfrefph3part2').$form->getValue('nfrefph3part3');
                $data['nf_ref_email3']=$form->getValue('nf_ref_email3');
                $data['referred_by']=$form->getValue('referred_by');
                $referred_by_other=$form->getValue('referred_by_other');
                if($data['referred_by']=='Other'){
                        $data['referred_by']=$referred_by_other;

                }
                $data['match_other_requirement']=$form->getValue('match_other_requirement');
                $data['staff_comment']=$form->getValue('staff_comment');
                $data['staff_additional_notes']=$form->getValue('staff_additional_notes');
                $data['private_notes_to_staff']=$form->getValue('private_notes_to_staff');
                $data['sent_agreement_by']=$form->getValue('sent_agreement_by');
                $data['agreement_sent_status']=$form->getValue('agreement_sent_status');
                $data['approval_status']=$form->getValue('approval_status');
                $modifications = array_diff_assoc($data, $dbData);
                if(count($modifications)>0)
                $applicantDetailsTbl->updatedata($modifications, $applicantId);
                $profileLogs = new applicant_Model_profileUpdateLogs();
                  $profile_edit_date = date("Y-m-d H:i:s"); 
                 $auth = Zend_Auth::getInstance();
                 if($auth->hasIdentity())
                 {
                  $storage = $auth->getStorage();
                  $storedata=$storage->read();
                 }
            
                foreach($modifications as $key=>$mod)
                {
                  $logRow= $profileLogs->createRow();
                  $logRow->applicant_id=$applicantId;
                  $logRow->field_name=$key;
                  $logRow->old_value=$dbData[$key];
                  $logRow->new_value=$mod;
                  $logRow->profile_edited_by=$storedata->staff_id;	
                  $logRow->profile_edit_date=$profile_edit_date;
                  $logRow->save();
                }
             
                $this->_redirect('/admin/applicant/profile/applicantId/'.$applicantId);
                }catch(Exception $e)
                 {
                             echo  $e->getMessage();
                 }
                }
           
             }
    }
    
      public function viewprofileAction()
    {	
          
         $this->view->headScript()
                       ->appendFile("/js/profile.js"); 
 
        $applicantDetailsTbl = new applicant_Model_applicantDetails();
        
        $auth = Zend_Auth::getInstance();
       if($auth->hasIdentity())
        {
         $storage = $auth->getStorage();
        $storedata=$storage->read();
    
        }
         $applicantId=$this->_getParam("applicantId",$storedata->applicant_id);
         
         
        $data =$applicantDetailsTbl->fetchRow("applicant_id = '$applicantId'")->toArray();
        
        $eventsTbl = new masters_Model_events();
        $eventsQuery=$eventsTbl->select();
        $eventsQuery->from("events as ev",array("event_id","host","location","date"=>"DATE_FORMAT(date,'%D&nbsp;%b&nbsp;%Y')","from_time","to_time"));
        $eventsQuery->join("events_attended as eva","ev.event_id=eva.event_id",array());
        $eventsQuery->where("eva.applicant_id= '$applicantId'");
        $eventsQuery->setIntegrityCheck(false);
        
        $this->view->eventslist=$eventsTbl->fetchAll($eventsQuery);
        $profileDetails= new stdClass();
        
        if(count($data>0))
        {
        foreach($data as $field => $value)
        {
           $profileDetails->$field =$value; 
        }
        
         $currency=new Zend_Currency();
         $currency->setLocale("en_US");
         $currency->toCurrency();
	 $currency->setValue($profileDetails->app_annual_income);
         $profileDetails->app_annual_income =  $currency->toCurrency();
         
       
        
         //Applicant Profile Heading
	
	//$this->view->app_phone1_type =  $data['app_phone1_type'];
        $profileDetails->app_phone1=Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone1);
       	$profileDetails->app_phone2 = ($profileDetails->app_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone2));
	$profileDetails->app_phone3 = ($profileDetails->app_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone3));
	
	 if(trim($profileDetails->match_religion) == 'Not Specified') $profileDetails->match_religion='';
         if(trim($profileDetails->match_age_low) == 'Not Specified') $profileDetails->match_age_low='';
	 if(trim($profileDetails->match_age_high) == 'Not Specified') $profileDetails->match_age_high='';
	 if(trim($profileDetails->match_nationality) == 'Not Specified') $profileDetails->match_nationality='';
	 if(trim($profileDetails->match_ethnicity) == 'Not Specified') $profileDetails->match_ethnicity='';
	 if(trim($profileDetails->match_state) == 'Not Specified') $profileDetails->match_state='';
	 if(trim($profileDetails->match_us_status) == 'Not Specified') $profileDetails->match_us_status='';
	 if(trim($profileDetails->match_us_living_years) == 'Not Specified') $profileDetails->match_us_living_years='';
	 if(trim($profileDetails->match_language_must) == 'Not Specified') $profileDetails->match_language_must='';
	 if(trim($profileDetails->match_language_desirable) == 'Not Specified') $profileDetails->match_language_desirable='';
	 if(trim($profileDetails->match_height_low) == 'Not Specified') $profileDetails->match_height_low='';
	 if(trim($profileDetails->match_height_high) == 'Not Specified') $profileDetails->match_height_high='';
	 if(trim($profileDetails->match_skin_color) == 'Not Specified') $profileDetails->match_skin_color='';
	 if(trim($profileDetails->match_islamic_practice_level) == 'Not Specified') $profileDetails->match_islamic_practice_level='';
	 if(trim($profileDetails->match_hijab_status) == 'Not Specified') $profileDetails->match_hijab_status='';
	 if(trim($profileDetails->match_kid_status) == 'Not Specified') $profileDetails->match_kid_status='';
	 if(trim($profileDetails->match_other_requirement) == 'Not Specified') $profileDetails->match_other_requirement='';

	//Match Education & Carer
	if(trim($profileDetails->match_education) == 'Not Specified') $profileDetails->match_education='';
	if(trim($profileDetails->match_occupation) == 'Not Specified') $profileDetails->match_occupation='';
         $currency->setValue($profileDetails->match_annual_income_low);
         $currency->toCurrency();
	$profileDetails->match_annual_income_low = ($profileDetails->match_annual_income_low ==""? "" : ("From: " . $currency->toCurrency()));
	
        $currency->setValue($profileDetails->match_annual_income_high);
        $profileDetails->match_annual_income_high = ($profileDetails->match_annual_income_high==""? "" : (" To: " .$currency->toCurrency()));
	
        

	$profileDetails->intro_phone1 = ($profileDetails->intro_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone1));
	$profileDetails->intro_phone2 = ($profileDetails->intro_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone2));
	$profileDetails->intro_phone3 = ($profileDetails->intro_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone3));
	
	$profileDetails->nf_ref_phone1 = ($profileDetails->nf_ref_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone1));
	
	$profileDetails->nf_ref_phone2 = ($profileDetails->nf_ref_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone2));
	$profileDetails->nf_ref_phone3 = ($profileDetails->nf_ref_phone3  == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone3));
	$this->view->profileDetails=$profileDetails;
        }
    }  
    
 
    
    //action for checking Email field view
    public function checkEmailAccAction()
    {
        
    }

     public function sendBulkMailAcccessAction()
    {
        
    }
     public function exportToXlsAccessAction()
    {
        
    }
      public function addToEventsAccessAction()
    {
        
    }
      public function matchmakersettingsAction()
    {
        
    }
     //action for checking Photo field view
    public function checkPhotoAccAction()
    {
        
    }
    public function checkAddrAccAction()
    {
        
    }
     //action for checking Phone field view
    public function checkPhoneAccAction()
    {
        
    }
     public function checkSatusAccAction()
    {
        
    }
     public function profileAction()
    {	
         $this->view->headScript()
                       ->appendFile("/js/profile.js"); 
        $applicantDetailsTbl = new applicant_Model_applicantDetails();
        $applicantId= $this->_getParam('applicantId');
        $this->view->applicantId= $applicantId;
        
        $registry = Zend_Registry::getInstance();
      
     $acl=  $registry->get('acl'); 
     $role= $registry->get('role');
		//8-9-2012 start
		$auth = Zend_Auth::getInstance();    //get authentication
        if($auth->hasIdentity())
		{ 
			$identity = $auth->getIdentity();
			if(isset($identity->role))
			{
				$groupRole=strtolower($identity->group_role);
				$user_role=strtolower($identity->user_role);
			}
		}
		//8-9-2012 end
     
      $this->view->profileedit=$acl->isAllowed($role, 'admin'.":"."applicant", 'editprofile');
      $this->view->profileDelete=$acl->isAllowed($role, 'admin'.":"."applicant", 'delete');
      $this->view->showEmailId=$acl->isAllowed($role, 'admin'.":"."applicant", 'checkEmailAcc');
      $this->view->showPhoneNo=$acl->isAllowed($role, 'admin'.":"."applicant", 'checkPhoneAcc');
      $this->view->showAddress=$acl->isAllowed($role, 'admin'.":"."applicant", 'checkAddrAcc');
       $this->view->showPhoto=$acl->isAllowed($role, 'admin'.":"."applicant", 'checkPhotoAcc');
      $this->view->showMaritalStatus=$acl->isAllowed($role, 'admin'.":"."applicant", 'checkSatusAcc');
       $data =array();
        //8-9-2012 start
		if($groupRole=='matchmaker')
		{
			$userApplicantAccessList=array();
            $userApplicantAccessTab = new admin_Model_userapplicantaccess();
			if($user_role!='')
			{
				$query = $userApplicantAccessTab->select();
				$query->from("user_applicant_access",array('applicant_lists'));
				$query->where( "staff_id='".$user_role."'");
				$query->setIntegrityCheck(false);
	  
				$userApplicantAccess=$userApplicantAccessTab->fetchRow($query);
				if($userApplicantAccess->applicant_lists !="")
				{
				   $userApplicantAccessList=  unserialize($userApplicantAccess->	applicant_lists);
				}
				if(in_array($applicantId,$userApplicantAccessList))
				{
					$row=$applicantDetailsTbl->fetchRow("applicant_id = '$applicantId'");
				}
			}
			else
			{
				$row='';
			}
		}
		else
		{
			$row=$applicantDetailsTbl->fetchRow("applicant_id = '$applicantId'");
		}
        if($row !="")
		{ 
            $this->view->pofileFound=true;
            $data=$row->toArray();
        
        $profileDetails= new stdClass();
        
        if(count($data>0))
        {
        foreach($data as $field => $value)
        {
           $profileDetails->$field =$value; 
        }
         
        $eventsTbl = new masters_Model_events();
        $eventsQuery=$eventsTbl->select();
        $eventsQuery->from("events as ev",array("event_id","host","location","date"=>"DATE_FORMAT(date,'%D&nbsp;%b&nbsp;%Y')","from_time","to_time"));
        $eventsQuery->join("events_attended as eva","ev.event_id=eva.event_id",array());
        $eventsQuery->where("eva.applicant_id= '$applicantId'");
        $eventsQuery->setIntegrityCheck(false);
        
        $this->view->eventslist=$eventsTbl->fetchAll($eventsQuery);
         $currency=new Zend_Currency();
         $currency->setLocale("en_US");
         $currency->toCurrency();
		 $currency->setValue($profileDetails->app_annual_income);
         $profileDetails->app_annual_income =  $currency->toCurrency();
         
       
        //print_r($this->view); die;
         //Applicant Profile Heading
	
	//$this->view->app_phone1_type =  $data['app_phone1_type'];
       if(!$this->view->showPhoneNo)
      {
          $profileDetails->photo_file1=$profileDetails->photo_file2=
                  $profileDetails->photo_file3=$profileDetails->photo_file4=$profileDetails->photo_file5="";
      }
      if(!$this->view->showAddress)
      {
          $profileDetails->intro_address=$profileDetails->app_address="xxxxxxxxxxxxxxxxx";
      }
      
      if($this->view->showPhoneNo)
      {	
        $profileDetails->app_phone1=Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone1);
       	$profileDetails->app_phone2 = ($profileDetails->app_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone2));
	$profileDetails->app_phone3 = ($profileDetails->app_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone3));
        $profileDetails->intro_phone1 = ($profileDetails->intro_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone1));
	$profileDetails->intro_phone2 = ($profileDetails->intro_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone2));
	$profileDetails->intro_phone3 = ($profileDetails->intro_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone3));
	
	$profileDetails->nf_ref_phone1 = ($profileDetails->nf_ref_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone1));
	
	$profileDetails->nf_ref_phone2 = ($profileDetails->nf_ref_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone2));
	$profileDetails->nf_ref_phone3 = ($profileDetails->nf_ref_phone3  == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone3));
	
        
        
      }
      else
      {
          $profileDetails->app_phone1=$profileDetails->app_phone2=$profileDetails->app_phone3="(XXX)XXX-XXXX";
          $profileDetails->intro_phone1=$profileDetails->intro_phone2=$profileDetails->intro_phone3="(XXX)XXX-XXXX";
          $profileDetails->nf_ref_phone1=$profileDetails->nf_ref_phone2=$profileDetails->nf_ref_phone3="(XXX)XXX-XXXX";

      }
      if(!$this->view->showEmailId)
      {
            $profileDetails->app_email1=$profileDetails->app_email2=$profileDetails->app_email3
                     ="XXXXXXX@XXX.XXX";
            $profileDetails->intro_email1=$profileDetails->intro_email2=$profileDetails->intro_email3
                    ="XXXXXXX@XXX.XXX";
      }
      else
      {
      }
      if($this->view->showMaritalStatus)
      {
          switch($profileDetails->app_marital_status)
          {
              case 'S':
              $profileDetails->app_marital_status='Single';
             break;
         case 'D':
              $profileDetails->app_marital_status='Divorced';
             break;
         case 'W':
             if($profileDetails->app_gender =  'Male')
              $profileDetails->app_marital_status='Widower';
             else
                  $profileDetails->app_marital_status='Widow';
             break;
          }
      }
  else
      {
            $profileDetails->app_marital_status='XXXXXXXX';
      }
	 if(trim($profileDetails->match_religion) == 'Not Specified') $profileDetails->match_religion='';
         if(trim($profileDetails->match_age_low) == 'Not Specified') $profileDetails->match_age_low='';
	 if(trim($profileDetails->match_age_high) == 'Not Specified') $profileDetails->match_age_high='';
	 if(trim($profileDetails->match_nationality) == 'Not Specified') $profileDetails->match_nationality='';
	 if(trim($profileDetails->match_ethnicity) == 'Not Specified') $profileDetails->match_ethnicity='';
	 if(trim($profileDetails->match_state) == 'Not Specified') $profileDetails->match_state='';
	 if(trim($profileDetails->match_us_status) == 'Not Specified') $profileDetails->match_us_status='';
	 if(trim($profileDetails->match_us_living_years) == 'Not Specified') $profileDetails->match_us_living_years='';
	 if(trim($profileDetails->match_language_must) == 'Not Specified') $profileDetails->match_language_must='';
	 if(trim($profileDetails->match_language_desirable) == 'Not Specified') $profileDetails->match_language_desirable='';
	 if(trim($profileDetails->match_height_low) == 'Not Specified') $profileDetails->match_height_low='';
	 if(trim($profileDetails->match_height_high) == 'Not Specified') $profileDetails->match_height_high='';
	 if(trim($profileDetails->match_skin_color) == 'Not Specified') $profileDetails->match_skin_color='';
	 if(trim($profileDetails->match_islamic_practice_level) == 'Not Specified') $profileDetails->match_islamic_practice_level='';
	 if(trim($profileDetails->match_hijab_status) == 'Not Specified') $profileDetails->match_hijab_status='';
	 if(trim($profileDetails->match_kid_status) == 'Not Specified') $profileDetails->match_kid_status='';
	 if(trim($profileDetails->match_other_requirement) == 'Not Specified') $profileDetails->match_other_requirement='';

	//Match Education & Carer
	if(trim($profileDetails->match_education) == 'Not Specified') $profileDetails->match_education='';
	if(trim($profileDetails->match_occupation) == 'Not Specified') $profileDetails->match_occupation='';
         $currency->setValue($profileDetails->match_annual_income_low);
         $currency->toCurrency();
	$profileDetails->match_annual_income_low = ($profileDetails->match_annual_income_low ==""? "": ("From: ". $currency->toCurrency()));
	
        $currency->setValue($profileDetails->match_annual_income_high);
        $profileDetails->match_annual_income_high = ($profileDetails->match_annual_income_high==""? "": ("To: ".$currency->toCurrency()));
	$this->view->profileDetails=$profileDetails;
        
        }
        else
        {
             $this->view->pofileFound=false;
        }
    }
}
     public function exportlistAction()
     {
         ini_set('memory_limit', '-1');
        $this->_helper->layout()->disableLayout();
        Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);
          require_once('PHPExcel/PHPExcel.php');
          require_once('PHPExcel/PHPExcel/Writer/Excel2007.php');
            $auth = Zend_Auth::getInstance();
       if($auth->hasIdentity())
        {
         $storage = $auth->getStorage();
        $storedata=$storage->read();
        
   
        }
        $searchreportfieldsTab = new admin_Model_searchreportfields();
          $fieldsquery = $searchreportfieldsTab->select();
         $fieldsquery->from('search_report_fields as srf',array('field_description_id'));
          $fieldsquery->join('search_fields as sf','srf.field_description_id=sf.field_name',array('field_description'));
         $fieldsquery->where("usertype='$storedata->group_role' and active=1");
         $fieldsquery->setIntegrityCheck(false);
       
          $fieldsToFetch=array();
          
         $result = $searchreportfieldsTab->fetchAll($fieldsquery);
         $fieldsDisplay=array();
         
         $fieldsDisplay["applicant_id"]="Profile ID";
         $fieldsDisplay["name"]="Name";
         $fieldsToFetch["applicant_id"]="applicant_id";
         $fieldsToFetch["name"]="CONCAT(app_first_name,  ' ',app_last_name)";
         foreach($result as $field)
         {
            $fieldsDisplay[$field->field_description_id]=$field->field_description;
             $fieldsToFetch[$field->field_description_id]=$field->field_description_id;
         }
        
         $fieldsDisplay["applicant_id"]="Profile ID";
         
       
         if(isset($fieldsDisplay['age']))
         {
            
             $fieldsToFetch['age']="DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')";
         }
         if(isset($fieldsDisplay['name']))
         {
            
             $fieldsToFetch['name']="CONCAT(app_first_name,  ' ',app_last_name)";
         }
          if(isset($fieldsDisplay['app_marital_status']))                    
         {
            $fieldsToFetch['app_marital_status']= new zend_db_expr("IF( app_marital_status =  'S',  'Single', IF( app_marital_status =  'D',  'Divorced', IF( app_marital_status =  'W'
            AND app_gender =  'Male',  'Widower',  'Widow' ) ) )");
         }
         if(isset($fieldsDisplay['app_email']))                    
         {
            $fieldsToFetch['app_email']= "concat(app_email1,'\\r\\n',app_email2,'\\r\\n',app_email3)";
         }
          if(isset($fieldsDisplay['app_phone']))                    
         {
            $fieldsToFetch['app_phone']= "concat(app_phone1,'\\r\\n',app_phone2,'\\r\\n',app_phone3)";
         }
          if(isset($fieldsDisplay['registration_date']))                    
         {
            $fieldsToFetch['registration_date']= "DATE_FORMAT(registration_date, '%m/%Y')";
         }
          if(isset($fieldsDisplay['app_dob']))                    
         {
            $fieldsToFetch['app_dob']= "DATE_FORMAT(app_dob, '%m/%d/%Y')";
         }
          if(isset($fieldsDisplay['last_activity_date']))                    
         {
            $fieldsToFetch['last_activity_date']= "DATE_FORMAT(last_activity_date, '%m/%d/%Y')";
         }
        
       
          $applicantDetailsTbl = new applicant_Model_applicantDetails();
          $query=$applicantDetailsTbl->select();
          $profileids= $this->_getParam("profileids");
          $query->from("applicant_information",$fieldsToFetch);
         $query->where("applicant_id in ('".implode("','",$profileids)."')");
            
          $query->setIntegrityCheck(false);
           $details =  $applicantDetailsTbl->fetchAll($query);
            
             
         $objPHPExcel = new   PHPExcel();    
                          
            //$objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);

            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(0)->setWidth(5);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(1)->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(2)->setWidth(30);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(3)->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(4)->setWidth(8);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(5)->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(6)->setWidth(25);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(7)->setWidth(15);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(8)->setWidth(12);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn(9)->setWidth(11);

            $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow(0, 1, 9,1)->setCellValueByColumnAndRow("0","1","APPLICANTS LIST");
            $objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(25);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 1)->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 1)->getFont()->setSize(13);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, 1)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 2,  'S.No.');
           // $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 2, 'Profile ID');
            $i=1;
            $row=2;
            foreach( $fieldsDisplay  as $field=>$title)
            {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $i++, 2, $title);    
            }
           
            
            $objPHPExcel->getActiveSheet()->getStyle('A2:Z2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getRowDimension(2)->setRowHeight(18);
            $objPHPExcel->getActiveSheet()->getStyle('A2:Z2')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getStyle('A2:Z2')->getFont()->setSize(10);
           // $objPHPExcel->getActiveSheet()->getStyle('A:C')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0) ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(2) ->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $slno=1;
           
           foreach($details  as $data){
                     $row++;$j=0;
              $objPHPExcel->getActiveSheet()->getRowDimension($row)->setRowHeight(15);
              $objPHPExcel->getActiveSheet()->getStyle('A'.$row.':'.'Z'.$row)->getFont()->setSize(13);
              $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($j++,$row,$slno++);
            
               foreach( $fieldsDisplay  as $field=>$title)
            {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $j++, $row, $data->$field);    
            }
              
              }
            
                
 	              header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                         header('Content-Disposition: attachment;filename="ApplicantsList.xls" ');
                         header('Cache-Control: max-age=0');
			 $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
                         $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                     	 $objWriter->save('php://output');
                     
           
          
                  
     }
      public function eventslistAction(){
        $this->view->headScript()->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.widget.min.js")
                ->appendFile("/js/jquery.ui.datepicker.js")
                ->appendFile("/js/jquery.ui.timepicker.js?v=0.2.9")
                ->appendFile("/js/jquery.jscrollpane.js")
                ->appendFile("/js/eventAddValidation.js");
         $this->view->headLink()->appendStylesheet("/css/jquery-ui-1.8.14.custom.css")
                 ->appendStylesheet("/css/jquery.ui.timepicker.css")
                ->appendStylesheet("/css/jquery.jscrollpanes.css") ;
      
         
         $eventTab = new masters_Model_events();
         $query = $eventTab->select();
         $query->from("events as ev",array('event_id','host','location','date'=>'DATE_FORMAT(date,"%d/%m/%Y")','from_time','to_time'));
         $query->order("date desc");
         $this->view->result=$eventTab->fetchAll($query);
        
      
  }
  
       public function eventsattendedAction(){
        $this->view->headScript()->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.widget.min.js")
                ->appendFile("/js/jquery.ui.datepicker.js")
                ->appendFile("/js/jquery.ui.timepicker.js?v=0.2.9")
                ->appendFile("/js/jquery.jscrollpane.js")
                ->appendFile("/js/eventAddValidation.js");
         $this->view->headLink()->appendStylesheet("/css/jquery-ui-1.8.14.custom.css")
                 ->appendStylesheet("/css/jquery.ui.timepicker.css")
                ->appendStylesheet("/css/jquery.jscrollpanes.css") ;
      
         
         $eventTab = new masters_Model_events();
         $query = $eventTab->select();
         $query->from("events as ev",array('event_id','host','location','date'=>'DATE_FORMAT(date,"%d/%m/%Y")','from_time','to_time'));
         $query->order("date desc");
         $this->view->result=$eventTab->fetchAll($query);
         $form = new  masters_Form_eventattended();
         if($this->getRequest()->isPost())
                         {
                            // print_r($form);
                            //die; 
						//if( $form->isValid($_POST))
                         if( $form->isValid($_POST))
                             {
                            
                             
 $host =     $form->getValue('host');
  $location =     $form->getValue('location');
  
 $date =     $form->getValue('date');
 $date = new Zend_Date($date,"M/d/Y");
 $date = $date->toString("Y-M-d");
  //5-9-2012
  /*$fromTime =     $form->getValue('fromTime');
  $toTime =     $form->getValue('toTime');*/
  $fromTime ='';
   $toTime ='';
  //5-9-2012
 $eventTab->createEvents($host,$location,$date,$fromTime,$toTime);
 $this->view->status="success";
                             
                         }
                         }
         
      $this->view->form=$form;
      
  }
       public function applicanteventsattendedAction(){
      
      $event_id= $this->_getParam('event_id');
      $this->view->event_id=$event_id;
      $event_id= base64_decode($event_id);
        $eventTab = new masters_Model_events();
         $query = $eventTab->select();
         $query->from("events as ev",array('event_id','host','location','date'=>'DATE_FORMAT(date,"%d/%m/%Y")','from_time','to_time'));
    $query->where("ev.event_id='$event_id'");
         $this->view->result=$eventTab->fetchRow($query);
         
         $eventsattendedTab = new masters_Model_eventsattended();
         $eventquery = $eventsattendedTab->select();
         $eventquery->from('events_attended as ea',array(''));
         $eventquery->join("applicant_information as ai","ai.applicant_id=ea.applicant_id",array("applicant_id","app_gender",
             "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
             "approval_status","app_gender","app_marital_status","app_first_name","app_last_name","app_nick_name"));
    $eventquery->where("ea.event_id='$event_id'");
 $eventquery->setIntegrityCheck(false);
         $this->view->eventresult=$eventTab->fetchAll($eventquery);
         
         
      
      
  }
  
  public function editmatchmakeraccessAction(){ 
      
      
           $this->view->headScript()
                ->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.widget.min.js")
                ->appendFile("/js/jquery.ui.position.js")
                ->appendFile("/js/jquery.ui.autocomplete.js")
                 ->appendFile("/js/editmatchmakeraccess.js");
         
         
            $this->view->headLink()->appendStylesheet("/css/jquery.ui.autocomplete.css");
      
      $matchmaker= $this->_getParam('matchmaker');
      $this->view->matchmaker=$matchmaker;
      $matchmaker= base64_decode($matchmaker);
      
      $userApplicantAccessList=array();
      $userApplicantAccessTab = new admin_Model_userapplicantaccess();
      
      $query = $userApplicantAccessTab->select();
      $query->from("staff_login_information as st",array('staff_id','usertype','status','last_login_date',	'name'));
      $query->joinLeft("user_applicant_access as ua", "ua.staff_id=st.staff_id",array('applicant_lists'));
      $query->where("st.staff_id = '$matchmaker'");
       $query->setIntegrityCheck(false);
  
      $userApplicantAccess=$userApplicantAccessTab->fetchRow($query);
      if($userApplicantAccess->	applicant_lists !="")
      {
          $userApplicantAccessList=  unserialize($userApplicantAccess->	applicant_lists);
      }
      
      
         $this->view->result=$userApplicantAccess;
         
         $applicantTab = new applicant_Model_applicantDetails();
         $applicantquery = $applicantTab->select();
       
         $applicantquery->from("applicant_information as ai",array("applicant_id","app_gender",
             "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
             "approval_status","app_gender","app_marital_status","app_first_name","app_last_name","app_nick_name"));
       
          $applicantquery->where("applicant_id in ('".implode("','",$userApplicantAccessList)."')");
         $applicantquery->setIntegrityCheck(false);
         $this->view->applicantList=$applicantTab->fetchAll($applicantquery);
         
      
  }
  public function updatematchmakeraccessAction(){ 
      
      $this->_helper->layout()->disableLayout();
      Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);  
      $matchMakeruserapplicantaccessTab = new admin_Model_userapplicantaccess();
      $mode=$this->_getParam("mode","add");
      $matchMaker=$this->_getParam("matchmaker");
       $mainArray = array();
         $result=array();
      try{
           $applicantIds=$this->_getParam("profileids"); 
         if($applicantIds=="")
        $applicantIds=$this->_getParam("addApplicants");
         
           
            $userApplicantAccess = $matchMakeruserapplicantaccessTab->fetchRow("staff_id='$matchMaker'");
           
           if($userApplicantAccess=="")
              {
              
                  $userApplicantAccess=$matchMakeruserapplicantaccessTab->createRow();
                  $userApplicantAccess->staff_id=$matchMaker;
              }
            if($userApplicantAccess->applicant_lists !="")
                {
              
                 $mainArray = unserialize($userApplicantAccess->applicant_lists);
                }  
           
               
      switch($mode)
      {
         case "add" :
              foreach($applicantIds as $applicant_id)
            {
             $mainArray[$applicant_id]=$applicant_id;
            }
               $result['msg']="applicants Inserted !";
         break;    
          case "delete" :
             
            
              foreach($applicantIds as $applicant_id)
                {
                  if(isset( $mainArray[$applicant_id]))
                      unset( $mainArray[$applicant_id]);
                }
                 $result['msg']="applicants Deleted !";
                break;
        }
         
            
         $applicantList = serialize($mainArray);
          $userApplicantAccess->applicant_lists=$applicantList;
          $userApplicantAccess->save();
          
             $result['result']=true;
        
    
               }  catch (Exception $e)
               {
                      $result['result']=false;
                   $result['msg']=$e;
                 
               }
          
    
        
   
      if($this->getRequest()->isXmlHttpRequest())
      {
       echo Zend_Json::encode($result);
      }
      else
      {
        $this->_redirect("/admin/applicant/editmatchmakeraccess/matchmaker/".base64_encode($matchMaker)."/msg/".base64_encode($result['msg']));
      }
 
  }
  
  public function matchmakerlistAction(){ 
         $this->view->headScript()->appendFile("/js/staffuserlist.js");
      $staffLoginTab = new masters_Model_stafflogininformation();
      $this->view->results = $staffLoginTab->fetchAll("usertype='MatchMaker'");
  }
  public function matchmakeraccesslistAction(){
      
      $matchmaker= $this->_getParam('matchmaker');
      $this->view->matchmaker=$matchmaker;
      $matchmaker= base64_decode($matchmaker);
      $userApplicantAccessList=array("");
      $userApplicantAccessTab = new admin_Model_userapplicantaccess();
      
      $query = $userApplicantAccessTab->select();
      $query->from("staff_login_information as st",array('staff_id','usertype','status','last_login_date',	'name'));
      $query->joinLeft("user_applicant_access as ua", "ua.staff_id=st.staff_id",array('applicant_lists'));
      $query->where("st.staff_id = '$matchmaker'");
       $query->setIntegrityCheck(false);
      $userApplicantAccess=$userApplicantAccessTab->fetchRow($query);
      if($userApplicantAccess->	applicant_lists !="")
      {
          $userApplicantAccessList=  unserialize($userApplicantAccess->	applicant_lists);
      }
      
      
         $this->view->result=$userApplicantAccess;
         
         $applicantTab = new applicant_Model_applicantDetails();
         $applicantquery = $applicantTab->select();
       
         $applicantquery->from("applicant_information as ai",array("applicant_id","app_gender",
             "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
             "approval_status","app_gender","app_marital_status","app_first_name","app_last_name","app_nick_name"));
       // $applicantquery->where("ea.event_id='$event_id'");
          $applicantquery->where("applicant_id in ('".implode("','",$userApplicantAccessList)."')");
         $applicantquery->setIntegrityCheck(false);
         $this->view->applicantList=$applicantTab->fetchAll($applicantquery);
         
         
      
      
  }
  
   public function editeventsattendedAction(){
      
         $this->view->headScript()
                ->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.widget.min.js")
                ->appendFile("/js/jquery.ui.position.js")
                ->appendFile("/js/jquery.ui.autocomplete.js")
                 ->appendFile("/js/addEvents.js");
         
         
            $this->view->headLink()->appendStylesheet("/css/jquery.ui.autocomplete.css");
      
         
         $event_id= $this->_getParam('event_id');
      $this->view->event_id=$event_id;
      $event_id= base64_decode($event_id);
        $eventTab = new masters_Model_events();
         $query = $eventTab->select();
         $query->from("events as ev",array('event_id','host','location','date'=>'DATE_FORMAT(date,"%d/%m/%Y")','from_time','to_time'));
    $query->where("ev.event_id='$event_id'");
         $this->view->result=$eventTab->fetchRow($query);
         
         $eventsattendedTab = new masters_Model_eventsattended();
         $eventquery = $eventsattendedTab->select();
         $eventquery->from('events_attended as ea',array(''));
         $eventquery->join("applicant_information as ai","ai.applicant_id=ea.applicant_id",array("applicant_id","app_gender",
             "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
             "approval_status","app_gender","app_marital_status","app_first_name","app_last_name","app_nick_name"));
    $eventquery->where("ea.event_id='$event_id'");
  
 $eventquery->setIntegrityCheck(false);
         $this->view->eventresult=$eventTab->fetchAll($eventquery);
   }
 public function updateapplicanteventsAction()
 {
     
      $this->_helper->layout()->disableLayout();
      Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);  
       $event=$this->_getParam("event","add");
      $eventid=$this->_getParam("eventid");
       try{
      switch($event)
      {
        case "add" :
         $applicantIds=$this->_getParam("profileids","");
         if($applicantIds=="")
        $applicantIds=$this->_getParam("addApplicants");
            
        
        $eventsattendedTab = new masters_Model_eventsattended();
       
         $checkQuery=$eventsattendedTab->select();
          $checkQuery->from('events_attended as ea',array('applicant_id'));
        $checkQuery->where("event_id='$eventid'  and applicant_id in ('".implode("','",$applicantIds)."')");
        $excludeArray=array();
        foreach($eventsattendedTab->fetchAll($checkQuery) as $evntsapp)
        {
            $excludeArray[]=$evntsapp->applicant_id;
        }
        $db=$eventsattendedTab->getAdapter();
        $db->beginTransaction();
       
        foreach($applicantIds as $applicant_id)
        {
            if(!in_array($applicant_id, $excludeArray))
            {
               $row= $eventsattendedTab->createRow();
               $row->applicant_id=$applicant_id;
               $row->event_id=$eventid;
               $row->save();
            }
        }
        $db->commit();
       $result['msg']="applicants Inserted !";
        break;
        
          case "delete" :
        $applicantIds=$this->_getParam("profileids");
        $eventsattendedTab = new masters_Model_eventsattended();
       
         $checkQuery=$eventsattendedTab->select();
          $checkQuery->from('events_attended as ea',array('applicant_id'));
        $checkQuery->where("event_id='$eventid'  and applicant_id in ('".implode("','",$applicantIds)."')");
        $deleteArray=array();
        foreach($eventsattendedTab->fetchAll($checkQuery) as $evntsapp)
        {
            $deleteArray[]=$evntsapp->applicant_id;
        }
        $db=$eventsattendedTab->getAdapter();
        $db->beginTransaction();
        $eventsattendedTab->delete("applicant_id in ('".implode("','",$deleteArray)."')");
        
        $db->commit();
       $result['msg']="applicants Deleted !";
        break;
        
      }
        $result['result']=true;
       }catch(Exception $e)
       {
           $result['result']=false;
           $result['msg']=$e->getMessage();
           
       }
      if($this->getRequest()->isXmlHttpRequest())
      {
       echo Zend_Json::encode($result);
      }
      else
      {
          $this->_redirect("/admin/applicant/applicanteventsattended/event_id/"
                  .base64_encode($eventid)."/msg/".base64_encode($result['msg']));
      }
 }
  public function editeventAction(){
   
           $id= base64_decode($this->_getParam('event_id'));
         $this->view->headScript()->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.core.min.js")
                ->appendFile("/js/jquery.ui.widget.min.js")
                ->appendFile("/js/jquery.ui.datepicker.js")
                ->appendFile("/js/jquery.ui.timepicker.js?v=0.2.9")
               ->appendFile("/js/jquery.jscrollpane.js")  
                ->appendFile("/js/eventAddValidation.js");
         $this->view->headLink()->appendStylesheet("/css/jquery-ui-1.8.14.custom.css")->appendStylesheet("/css/jquery.ui.timepicker.css");
      
         
         $eventTab = new masters_Model_events();
         $query = $eventTab->select();
         $query->from("events as ev",array('event_id','host','location','date'=>'DATE_FORMAT(date,"%m/%d/%Y")',
             'fromTime'=>'from_time','toTime'=>'to_time'));
           $query->where("ev.event_id=$id");
         $result=$eventTab->fetchRow($query);
         $form = new  masters_Form_eventattended();
         $form->populate($result->toArray());
         if($this->getRequest()->isPost())
                         {
                        //     print_r($_POST);
                              
                         if( $form->isValid($_POST))
                             {
                             
                             
 $host =     $form->getValue('host');
  $location =     $form->getValue('location');
  
 $date =     $form->getValue('date');
 $date = new Zend_Date($date,"M/d/Y");
 $date = $date->toString("Y-M-d");
  $fromTime =     $form->getValue('fromTime');
  
 $toTime =     $form->getValue('toTime');
 $eventTab->updateEvents($id,$host,$location,$date,$fromTime,$toTime);
 $this->_redirect("/admin/applicant/eventsattended");
                             
                         }
                         }
         
      $this->view->form=$form;
    }
    
   public function deleteeventAction(){
   
          $event_id= $this->_getParam('event_id');
      $this->view->event_id=$event_id;
      $event_id= base64_decode($event_id);
        $eventTab = new masters_Model_events();
         $query = $eventTab->select();
         $query->from("events as ev",array('event_id','host','location','date'=>'DATE_FORMAT(date,"%d/%m/%Y")','from_time','to_time'));
    $query->where("ev.event_id='$event_id'");
         $this->view->result=$eventTab->fetchRow($query);
         
         $eventsattendedTab = new masters_Model_eventsattended();
         $eventquery = $eventsattendedTab->select();
         $eventquery->from('events_attended as ea',array(''));
         $eventquery->join("applicant_information as ai","ai.applicant_id=ea.applicant_id",array("applicant_id","app_gender",
             "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
             "approval_status","app_gender","app_marital_status","app_first_name","app_last_name","app_nick_name"));
      $eventquery->where("ea.event_id='$event_id'");
     $eventquery->setIntegrityCheck(false);
         $this->view->eventresult=$eventTab->fetchAll($eventquery);
         if($this->getRequest()->isPost())
         {
               $eventTab->delete("event_id='$event_id'");       
               $eventsattendedTab->delete("event_id='$event_id'");            
              $this->_redirect("/admin/applicant/eventsattended");
                             
      }
         
    }
    
     public function deleteAction(){
         $this->view->pofileDeleted=False;
          $this->view->headScript()
                       ->appendFile("/js/JBlock.js"); 
        $applicantDetailsTbl = new applicant_Model_applicantDetails();
        $applicantId= base64_decode($this->_getParam('applicantId'));
        $this->view->applicantId= $applicantId;
       $data =array();
        
        $row=$applicantDetailsTbl->fetchRow("applicant_id = '$applicantId'");
        if($this->getRequest()->isPost())
         { 
            $act =$this->_getParam('act');
            if("cancel"===$act)
            {
                $this->_redirect("/admin/applicant/profile/applicantId/$applicantId");
            }
            else
            {
            
            $confirm=$this->_getParam('confirm');
            if('Yes' ===$confirm)
                 $row->delete ();  
             $this->view->pofileDeleted=true;
            }
            }
        
          if($this->view->pofileDeleted==False)
           $this->view->headScript()->appendScript(
        "$(document).ready(function(){
        
        $.blockUI({
        css: { 
        padding:        0, 
        margin:         0, 
        width:          '30%', 
        top:            '10%', 
        left:           '35%', 
        textAlign:      'center', 
        color:          '#000', 
        border:         '3px solid #aaa', 
        backgroundColor:'#fff', 
        cursor:         'wait' 
    }, 
message: $('#formDelete') }); 
 
       }); ");   
            
        if($row !=""){ 
            $this->view->pofileFound=true;
            $data=$row->toArray();
        
        $profileDetails= new stdClass();
        
        if(count($data>0))
        {
        foreach($data as $field => $value)
        {
           $profileDetails->$field =$value; 
        }
         
        $eventsTbl = new masters_Model_events();
        $eventsQuery=$eventsTbl->select();
        $eventsQuery->from("events as ev",array("event_id","host","location","date"=>"DATE_FORMAT(date,'%D&nbsp;%b&nbsp;%Y')","from_time","to_time"));
        $eventsQuery->join("events_attended as eva","ev.event_id=eva.event_id",array());
        $eventsQuery->where("eva.applicant_id= '$applicantId'");
        $eventsQuery->setIntegrityCheck(false);
        
        $this->view->eventslist=$eventsTbl->fetchAll($eventsQuery);
         $currency=new Zend_Currency();
         $currency->setLocale("en_US");
         $currency->toCurrency();
	 $currency->setValue($profileDetails->app_annual_income);
         $profileDetails->app_annual_income =  $currency->toCurrency();
         
       
        
         //Applicant Profile Heading
	
	//$this->view->app_phone1_type =  $data['app_phone1_type'];
        $profileDetails->app_phone1=Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone1);
       	$profileDetails->app_phone2 = ($profileDetails->app_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone2));
	$profileDetails->app_phone3 = ($profileDetails->app_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->app_phone3));
	
	 if(trim($profileDetails->match_religion) == 'Not Specified') $profileDetails->match_religion='';
         if(trim($profileDetails->match_age_low) == 'Not Specified') $profileDetails->match_age_low='';
	 if(trim($profileDetails->match_age_high) == 'Not Specified') $profileDetails->match_age_high='';
	 if(trim($profileDetails->match_nationality) == 'Not Specified') $profileDetails->match_nationality='';
	 if(trim($profileDetails->match_ethnicity) == 'Not Specified') $profileDetails->match_ethnicity='';
	 if(trim($profileDetails->match_state) == 'Not Specified') $profileDetails->match_state='';
	 if(trim($profileDetails->match_us_status) == 'Not Specified') $profileDetails->match_us_status='';
	 if(trim($profileDetails->match_us_living_years) == 'Not Specified') $profileDetails->match_us_living_years='';
	 if(trim($profileDetails->match_language_must) == 'Not Specified') $profileDetails->match_language_must='';
	 if(trim($profileDetails->match_language_desirable) == 'Not Specified') $profileDetails->match_language_desirable='';
	 if(trim($profileDetails->match_height_low) == 'Not Specified') $profileDetails->match_height_low='';
	 if(trim($profileDetails->match_height_high) == 'Not Specified') $profileDetails->match_height_high='';
	 if(trim($profileDetails->match_skin_color) == 'Not Specified') $profileDetails->match_skin_color='';
	 if(trim($profileDetails->match_islamic_practice_level) == 'Not Specified') $profileDetails->match_islamic_practice_level='';
	 if(trim($profileDetails->match_hijab_status) == 'Not Specified') $profileDetails->match_hijab_status='';
	 if(trim($profileDetails->match_kid_status) == 'Not Specified') $profileDetails->match_kid_status='';
	 if(trim($profileDetails->match_other_requirement) == 'Not Specified') $profileDetails->match_other_requirement='';

	//Match Education & Carer
	if(trim($profileDetails->match_education) == 'Not Specified') $profileDetails->match_education='';
	if(trim($profileDetails->match_occupation) == 'Not Specified') $profileDetails->match_occupation='';
         $currency->setValue($profileDetails->match_annual_income_low);
         $currency->toCurrency();
	$profileDetails->match_annual_income_low = ($profileDetails->match_annual_income_low ==""? "": ("From: ". $currency->toCurrency()));
	
        $currency->setValue($profileDetails->match_annual_income_high);
        $profileDetails->match_annual_income_high = ($profileDetails->match_annual_income_high==""? "": ("To: ".$currency->toCurrency()));
	
        

	$profileDetails->intro_phone1 = ($profileDetails->intro_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone1));
	$profileDetails->intro_phone2 = ($profileDetails->intro_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone2));
	$profileDetails->intro_phone3 = ($profileDetails->intro_phone3 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->intro_phone3));
	
	$profileDetails->nf_ref_phone1 = ($profileDetails->nf_ref_phone1 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone1));
	
	$profileDetails->nf_ref_phone2 = ($profileDetails->nf_ref_phone2 == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone2));
	$profileDetails->nf_ref_phone3 = ($profileDetails->nf_ref_phone3  == ""?"":Pioneersys_commonFunctions::setUsPhoneFormat($profileDetails->nf_ref_phone3));
	$this->view->profileDetails=$profileDetails;
        
        }
        else
        {
             $this->view->pofileFound=false;
        }
    }
    
     }
    
  public function geteventsAction(){
         $eventTab = new masters_Model_events();
         $query = $eventTab->select();
         $query->from("events as ev",array('event_id','host','location','date'=>'DATE_FORMAT(date,"%d/%m/%Y")',
             'fromTime'=>'from_time','toTime'=>'to_time'));
           
        $searchString=strtolower($this->_getParam('search',""));
       
         
         $query->orWhere("lower(host) like '%".$searchString."%'");
          $query->orWhere("lower(location) like '%".$searchString."%'");
          
        $query->setIntegrityCheck(false);
       
            $this->_helper->layout()->disableLayout();
       
       Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);  
        try {   
              
               $data=$eventTab->fetchAll($query)->toArray();
                 
                    $result['data']=$data;
                    $result['result']=true;
      
        }
 catch (Exception $e){
     
      $result['result']=false;
      $result['error']=$e->getMessage();

 }
 echo Zend_Json::encode($result);
   }
   public function getapplicantsAction(){ 
        $applicantDetailsTbl = new applicant_Model_applicantDetails();
        $query = $applicantDetailsTbl->select();
        $searchString=strtolower($this->_getParam('search',""));
       
         $query->from('applicant_information as ai',array("applicant_id","app_gender",
             "age"=>"DATE_FORMAT((FROM_DAYS(DATEDIFF(NOW(),app_dob))),'%y')",
             "approval_status","app_gender","app_marital_status","app_first_name","app_last_name"));
         $query->orWhere("lower(ai.app_first_name) like '%".$searchString."%'");
          $query->orWhere("lower(ai.app_last_name) like '%".$searchString."%'");
           $query->orWhere("lower(ai.applicant_id) like '%".$searchString."%'");
        $query->setIntegrityCheck(false);
       
            $this->_helper->layout()->disableLayout();
       
       Zend_Controller_Front::getInstance()->setParam('noViewRenderer', true);  
        try {   
              
               $data=$applicantDetailsTbl->fetchAll($query)->toArray();
                 
                    $result['data']=$data;
                    $result['result']=true;
      
        }
 catch (Exception $e){
     
      $result['result']=false;
      $result['error']=$e->getMessage();

 }
 echo Zend_Json::encode($result);
   }
   
   public function mailsAction(){
	   //Read e mail from directory and move to database
	   //$read_email							=	$this->fetchEmailFromDirectory();
	   //get authentication
	  	
	   $auth								=	Zend_Auth::getInstance();    
       if($auth->hasIdentity()){
		   $identity 						=	$auth->getIdentity();
		   if(isset($identity->role)){
			   $group_role					=	strtolower($identity->group_role);
			   $user_role					=	strtolower($identity->user_role);
		   }
	   }
	  	$newMessageFormObj		=	new admin_Form_newEmail();
		$this->view->form		=	$newMessageFormObj;
		$unread_emails			=	0;
	   if($group_role=='matchmaker'){
		   if($user_role!=''){
			   
			   $staff_details_tbl 			=	new admin_Model_staffLoginInformation();
			   $dbAdapterObj				=	$staff_details_tbl->getAdapter();
			   $select_staff				=	$staff_details_tbl->select()->from('staff_login_information',array('email'))
			   									->where('staff_login_information.staff_id = ?', $user_role);
			   $staff_email					=	$dbAdapterObj->fetchOne($select_staff);
			   //echo $staffDetailsArr;	die();
			   if(!is_null($staff_email)>0){
				   $staff_email_id			=	$staff_email;
				   $staffEmailArr			=	explode('@',$staff_email_id);
				  
				   $staffEmailArr[1];
				   if(!empty($staffEmailArr[1])){
					    $domain_part				=	$staffEmailArr[1];
						$config			=	new Zend_Config_Ini(APPLICATION_PATH.'/configs/settings.ini','settings');
						$domain_name	=	$config->sendgrid->domain;
					  	if($staffEmailArr[1]	==	$domain_name){
							$email_prefix_part	=	!empty($staffEmailArr[1])?	$staffEmailArr[0]	:	'';
							$unread_emails		=	$this->fetchEmailFromDirectory($email_prefix_part);
					   }
				   }
			   }
			   
			   $email_details_tbl 			=	new admin_Model_emailLogs();
			   $dbEmailAdapterObj			=	$staff_details_tbl->getAdapter();
			   $select						=	$email_details_tbl->select()->from('email_log_information',array('receiver_mail_id','sender_mail_id','mailing_date'=>'MAX(mailing_date)'));
			   									
			   									//->join('applicant_information','email_log_information.sender_mail_id=applicant_information.intro_email1',array())
			   									$select->joinRight('staff_login_information','email_log_information.receiver_mail_id=staff_login_information.email',array())
												//
			   									->where('staff_login_information.staff_id = ?', $user_role)->group('email_log_information.sender_mail_id')->order('mailing_date DESC');
			   
			   /*$email_details_tbl 		=	new admin_Model_emailLogs();
			   $query						=	$email_details_tbl->select();
			   $query->where( "staff_id='".$user_role."'");*/
			   //echo ($select->__toString());
			   
			   $emailDetails				=	$dbEmailAdapterObj->fetchAll($select);
			   //print_r($emailDetails);
			   $emailDetailsArr				=	$emailDetails;
			   //$applicant_details_tbl			=	new applicant_Model_applicantDetails();
			   //$applicant_details_tbl->selectAplByEmail();
			   $registry = Zend_Registry::getInstance();
			   $registry->set('unRead', $unread_emails);
			   $this->view->email_details	=	$emailDetailsArr;
	   	   }
	   }
   }
 
	  
	  
   public function fetchEmailFromDirectory($email_prefix_part){
	   ini_set('max_execution_time', 0);
	   //Read emails from directory
	   //$mail 								=	new Zend_Mail_Storage_Writable_Maildir(array('dirname' =>'/home/euphonte/mail/euphontest.in/karthika/'));
	   //$mail 								=	new Zend_Mail_Storage_Writable_Maildir(array('dirname' =>'/home4/fouonet5/mail/mvmteam.com/test1'));
	   $mail = 									new Zend_Mail_Storage_Writable_Maildir(array('dirname' =>'/home4/fouonet5/mail/mvmteam.com/'.$email_prefix_part.'/')); 
	   //$mail 								=	new Zend_Mail_Storage_Writable_Maildir(array('dirname' =>'/home4/fouonet5/mail/.test1@mvmteam_com/'));
	   //print_r($mail);
	   $count=0;
	   $read_count=0;
	   foreach ($mail as $id=>$message){
		   //Skip if email status is read
		   
		   if ($message->hasFlag(Zend_Mail_Storage::FLAG_SEEN)){
			   $count++;
			   continue;
		   }
		  
		   //If email status is unread
		   if ($message->hasFlag(Zend_Mail_Storage::FLAG_RECENT)){
			   
			   //To get the current email message id
			   //echo 'Iam in unread';
			   $unique_id 					=	$mail->getUniqueId($id);
		       $current_message_id 			=	$mail->getNumberByUniqueId($unique_id);
			   
			   //To get the sender email address
			   $from						=	$message->from;
			   $from_id 					=	$from;
		       if(strpos($from, '<') !== false){
				   $start_from  			=	strpos($from, '<');
  				   $email_from  			=	substr($from, $start_from, -1);
  				   $from_id 				=	str_replace('<', '', $email_from);
			   }
			    
			   //If the to field have more than one email addresses
			   $to_addr						=	$message->to;
  			   $toArr						=	explode(',',$to_addr);
			   $count++;
  			   foreach($toArr as $to){
				   //To get every single receiver email address
				   $to_id 					=	$to;
		      	   if(strpos($to, '<') !== false){
					   $start_to  			=	strpos($to, '<');
  					   $email_to  			=	substr($to, $start_to, -1);
  					   $to_id 				=	str_replace('<', '', $email_to);
				   }
				   
				   //Get email subject and date
				   $message_subject			=	$message->subject;
				   $message_date			=	$message->date;
				   
				   //To get email content
				   $part 					=	$message;
				  // print_r($part);
				  echo $count;
				   while ($part->isMultipart()){
					  
					   foreach (new RecursiveIteratorIterator($mail->getMessage($count)) as $part){
						//foreach (new RecursiveIteratorIterator($mail->getMessage(1)) as $part){ 
						$found_part 			=	null;
						   try {
		        				if (strtok($part->contentType, ';') == 'text/plain') {
		           					$found_part = $part;
									//echo $part,'test';
		            					break;
		        				}
		    				} catch (Zend_Mail_Exception $e) {
		        				// ignore
		    				}
						   break;
					   }
					   
				   }
				   $message_content			=	$found_part;
				   
				   ;
				   //echo $message_content;
				   //Push email details to an array
				   $emailDetailsArr							=	array();
				   $emailDetailsArr['sender_mail_id']		=	$from_id;
				   $emailDetailsArr['receiver_mail_id']		=	$to_id;
				   $emailDetailsArr['subject']				=	$message_subject;
				   $emailDetailsArr['message']				=	$message_content;
				   //$emailDetailsArr['mailing_date']			=	$message_date;
				   //print_r($emailDetailsArr);die();
				   //Insert email details to table
				   $email_details_tbl 		=	new admin_Model_emailLogs();
				   $flag_insert				=	$email_details_tbl->insert($emailDetailsArr);
				  //echo 'Test',$flag_insert;
				   
				   //Set email status as read
				   if(!empty($current_message_id)){
					   $read_count++;
					   $mail->setFlags($current_message_id, array(Zend_Mail_Storage::FLAG_SEEN));
				   }
				   
				   //$mail->removeMessage($currentmessageid);
			   }
		   }
	   }
	   return $read_count;
   }
   
   public function getEmailListAction(){
	   $auth								=	Zend_Auth::getInstance();    
       if($auth->hasIdentity()){
		   $identity 						=	$auth->getIdentity();
		   if(isset($identity->role)){
			   $group_role					=	strtolower($identity->group_role);
			   $user_role					=	strtolower($identity->user_role);
		   }
	   }
	  
		$insert_flag	=	0;   
		
		$data						=	$this->getRequest()->getPost();
		$receiver_email				=	trim($data['receiver_email']);
		$sender_email				=	trim($data['sender_email']);
		$temp_flag					=	false;
		
		if($data['message']!=''){
			$receiver_email				=	trim($data['sender_email']);
			$sender_email				=	trim($data['receiver_email']);
			
			$message	=	$data['message'];
			$newMessageFormObj		=	new admin_Form_newEmail();
			if ($newMessageFormObj->isValid($data)){
				//echo 'test';
				$emailDetailsArr						=	array();
				$emailDetailsArr['sender_mail_id']		=	$receiver_email;
				$emailDetailsArr['receiver_mail_id']	=	$sender_email;
				$emailDetailsArr['message']				=	$message;
				//$emailDetailsArr['mailing_date']		=	date('Y-m-d');
				
				//Insert email details to table
				$email_details_tbl 		=	new admin_Model_emailLogs();
				$insert_flag	=	$email_details_tbl->insert($emailDetailsArr);
				
				
					$message=$message;
         			$subject='Muslim Matchmaking - Message Notification';
         			$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/settings.ini','settings');
				  	$template=$config->emailtemplate->bulkemail;
				  	$html = new Zend_View();
				  	$html->setScriptPath(APPLICATION_PATH ."/emailTemplates");
				 	$message=$message;
					
				  	$mail= new Zend_Mail();
				  	$mail->setFrom($receiver_email);
				  	$mail->setSubject('Muslim Matchmaking - Message Notification');
					$html->assign('from', 'MVM Team');
				  	$html->assign('name', '' );
				  	$html->assign('message', stripcslashes($message));
				  	$bodyText = $html->render($template);
				  
				
				  	$mail->addTo($sender_email);
				  	$mail->setBodyHtml($bodyText);
				  	$mail->send();
					
					//swap sender and reciever emails
					
					/*$temp_receiver=$sender_email;
					$receiver_email	=	$sender_email;
					$sender_email	=	$temp_receiver;*/
					$temp_flag	=	true;
				
			}
			else{
				 $newMessageFormObj->populate($data);
			}
		}else{
			$insert_flag	=	true;
		}
		$this->_helper->layout()->disableLayout();
	  // echo $sender_email,'--',$receiver_email;
	  $applicant_name	=	'';
	  $staff_name	=	'';
	if($insert_flag==true){
			
			if($sender_email!=''){
				// $sender_email='test1@mvmteam.com';
				$applicant_details_tbl			=	new applicant_Model_applicantDetails();
				$dbAdapterObj					=	$applicant_details_tbl->getAdapter();
				$select							=	$applicant_details_tbl->select()
												   ->from('applicant_information',array('app_nick_name'))
												   ->where('('.$dbAdapterObj->quoteInto('app_email1 = ?', $sender_email).' OR '.$dbAdapterObj->quoteInto('app_email2 = ?', $sender_email).' OR '.$dbAdapterObj->quoteInto('app_email3 = ?', $sender_email).')')
												   ->order('applicant_id DESC')->limit(1);
				$applicantDetailsArr			=	$applicant_details_tbl->fetchAll($select);
				$applicantDetailsArr			=	$applicantDetailsArr->toArray();
			}
			if(count(array_filter($applicantDetailsArr))>0){
				$applicant_name	=	$applicantDetailsArr[0]['app_nick_name'];
			}else{
				$applicant_name	=	$sender_email;
			}
			
			
			if($receiver_email!=''){
				// $sender_email='test1@mvmteam.com';
				$staff_details_tbl			=	new admin_Model_staffLoginInformation();
				$dbsAdapterObj				=	$staff_details_tbl->getAdapter();
				$select						=	$staff_details_tbl->select()
												   ->from('staff_login_information',array('staff_id'))
												   ->where('( '.$dbsAdapterObj->quoteInto('email = ?', $receiver_email).' )')
												   ->order('staff_id DESC')->limit(1);
				$staffDetailsArr			=	$staff_details_tbl->fetchAll($select);
				$staffDetailsArr			=	$staffDetailsArr->toArray();
				if(count(array_filter($staffDetailsArr))>0){
					$staff_name	=	$staffDetailsArr[0]['staff_id'];
				}
				//echo $staff_name,'re',$receiver_email;
			}
			
			
			
			
		//}
	   //print_r($applicantDetailsArr);
	   $email_details_tbl 			=	new admin_Model_emailLogs();
	   $db = $email_details_tbl->getAdapter();
	  /* $select						=	$email_details_tbl->select()->where("sender_mail_id = $sender_email AND receiver_mail_id = $receiver_email")->orWhere("sender_mail_id = $receiver_email AND receiver_mail_id = $sender_email");*/
	   $select						=	$email_details_tbl->select()->where('(' . $db->quoteInto('sender_mail_id = ?', $sender_email) . ' AND ' . $db->quoteInto('receiver_mail_id = ?', $receiver_email) . ') OR (' . $db->quoteInto('sender_mail_id = ?', $receiver_email) . ' AND ' . $db->quoteInto('receiver_mail_id = ?', $sender_email) . ')')->order('mailing_date DESC');
	   $emailDetails				=	$email_details_tbl->fetchAll($select);
	   $emailDetailsArr				=	$emailDetails->toArray();
	 
	   $htmlArr						=	array();
	   $count						=	1;
	   
	  	
		if($temp_flag	=	false){
			$temp_receiver=$applicant_name;
			$applicant_name	=	$staff_name;
			$staff_name	=	$temp_receiver;
		}
					
	   foreach($emailDetailsArr as $key=>$value){
		    //print_r($emailDetailsArr[$key]);
		 /*  $htmlArr[]				=	'<tr height="50" >
          								<td>'.$count.'</td>
          								<td>'.$emailDetailsArr[$key]['subject'].'</td>
          								<td width="10px;"><div id="textfield_'.$count.'" cols="30" style="width: 265px; height: 61px;">'.$emailDetailsArr[$key]['message'].'</div></td>
          								<td>'.$emailDetailsArr[$key]['mailing_date'].'</td>
        								</tr>';
			$count++;	*/
			(($key%2)==1)? $msg_bg_flag	=	"email-odd" :	$msg_bg_flag	=	"email-even";
				
			if(trim($emailDetailsArr[$key]['message'])!='')	{
				$htmlArr[]				=	'<div class="email-bind" id="email-'.$key.'">';
				
				//.$emailDetailsArr[$key]['sender_mail_id'];
				if($sender_email	==	$emailDetailsArr[$key]['sender_mail_id']){
					$htmlArr[]			=	'<span class="email-from" >'.$applicant_name.'</span>';
					
				}
				else{
					$htmlArr[]			=	'<span class="email-to">'.$staff_name.'</span>';
					
				}//'<span >'.$emailDetailsArr[$key]['subject'].'</span> 
				$htmlArr[]				=	'<span ><font class="email-time">'.$this->timeStampToTime($emailDetailsArr[$key]['mailing_date']).'</font>&nbsp;<font class="email-date" >'.$this->getMonthDate($emailDetailsArr[$key]['mailing_date']).'</font></span>
											<p class="email-msg '.$msg_bg_flag.' id="textfield_'.$key.'">'.$emailDetailsArr[$key]['message'].'</p>
											</div>';
				$count++;
			}
	   }
   }
	   
	   $html_string					=	join('',$htmlArr);
	   echo json_encode(array('html'=>$html_string));
   }
   
   protected function timeStampToTime($timestamp	=	''){
	   $timestampArr	=	explode(" ",$timestamp);
	   $dateArr			=	isset($timestampArr[0])	?	$timestampArr[0]	:	'';
	   $timeArr			=	isset($timestampArr[1])	?	$timestampArr[1]	:	'';
	   $newTime			=	NULL;
	   
	   if(!empty($timeArr)){
		   $timevalArr	=	explode(":",$timeArr);
		   $hrs			=	isset($timevalArr[0])	?	$timevalArr[0]	:	0;
		   $mins		=	isset($timevalArr[1])	?	$timevalArr[1]	:	0;
		   
		   if($hrs >12){
			   $hrs		=	$hrs - 12;
			   $session	=	'PM';
		   }else if($hrs == 12){
			   $session	=	'NOON';
		   }else{
			   $session	=	'AM';
		   }
		   
		   $newTime	=	$hrs.':'.$mins.'&nbsp;'.$session;
		   
		   return	$newTime;
	   }
	   else return $newTime;
	}
	
protected function getMonthDate($date=''){
	
	
	$week_day	=	date('l', strtotime($date));
	$month		=	date('M', strtotime($date));
	
	$timeStampArr	=	array();
	$timeStampArr	=	explode(" ",$date);
	$dateArr		=	explode("-",$timeStampArr[0]);
	if(count($dateArr)>=2){
		$year		=	$dateArr[0];
		$date_val	=	$dateArr[2];
		return substr($week_day,0,3)." ".$month. " ".$date_val.' ,'.$year;
	}
 	
}
	
  /* public function getEmailListAction(){
	   $auth								=	Zend_Auth::getInstance();    
       if($auth->hasIdentity()){
		   $identity 						=	$auth->getIdentity();
		   if(isset($identity->role)){
			   $group_role					=	strtolower($identity->group_role);
			   $user_role					=	strtolower($identity->user_role);
		   }
	   }
	  
		$insert_flag	=	0;   
		
		$data						=	$this->getRequest()->getPost();
		$sender_email				=	trim($data['sender_email']);
		$receiver_email				=	trim($data['receiver_email']);
		$temp_flag					=	false;
		
		if($data['message']!=''){
			$receiver_email				=	trim($data['sender_email']);
			$sender_email				=	trim($data['receiver_email']);
			
			$message	=	$data['message'];
			$newMessageFormObj		=	new admin_Form_newEmail();
			if ($newMessageFormObj->isValid($data)){
				//echo 'test';
				$emailDetailsArr						=	array();
				$emailDetailsArr['sender_mail_id']		=	$sender_email;
				$emailDetailsArr['receiver_mail_id']	=	$receiver_email;
				$emailDetailsArr['message']				=	$message;
				$emailDetailsArr['mailing_date']		=	date('Y-m-d');
				
				//Insert email details to table
				$email_details_tbl 		=	new admin_Model_emailLogs();
				$insert_flag	=	$email_details_tbl->insert($emailDetailsArr);
				
				
					$message=$message;
         			$subject='MVM Team - Notification';
         			$config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/settings.ini','settings');
				  	$template=$config->emailtemplate->bulkemail;
				  	$html = new Zend_View();
				  	$html->setScriptPath(APPLICATION_PATH ."/emailTemplates");
				 	$message=$message;
					
				  	$mail= new Zend_Mail();
				  	$mail->setFrom($receiver_email);
				  	$mail->setSubject('MVM Team - Notification');
					$html->assign('from', $sender_email);
				  	$html->assign('name', 'MVM Team' );
				  	$html->assign('message', stripcslashes($message));
				  	$bodyText = $html->render($template);
				  
				
				  	$mail->addTo($sender_email);
				  	$mail->setBodyHtml($bodyText);
				  	$mail->send();
					
					//swap sender and reciever emails
					
					/*$temp_receiver=$sender_email;
					$receiver_email	=	$sender_email;
					$sender_email	=	$temp_receiver;*/
					/*$temp_flag	=	true;
				
			}
			else{
				 $newMessageFormObj->populate($data);
			}
		}else{
			$insert_flag	=	true;
		}
		$this->_helper->layout()->disableLayout();
	  // echo $sender_email,'--',$receiver_email;
	  $applicant_name	=	'';
	  $staff_name	=	'';
	if($insert_flag==true){
			
			if($sender_email!=''){
				// $sender_email='test1@mvmteam.com';
				$applicant_details_tbl			=	new applicant_Model_applicantDetails();
				$dbAdapterObj					=	$applicant_details_tbl->getAdapter();
				$select							=	$applicant_details_tbl->select()
												   ->from('applicant_information',array('app_nick_name'))
												   ->where('('.$dbAdapterObj->quoteInto('app_email1 = ?', $sender_email).' OR '.$dbAdapterObj->quoteInto('app_email2 = ?', $sender_email).' OR '.$dbAdapterObj->quoteInto('app_email3 = ?', $sender_email).')')
												   ->order('applicant_id DESC')->limit(1);
				$applicantDetailsArr			=	$applicant_details_tbl->fetchAll($select);
				$applicantDetailsArr			=	$applicantDetailsArr->toArray();
			}
			if(count(array_filter($applicantDetailsArr))>0){
				$applicant_name	=	$applicantDetailsArr[0]['app_nick_name'];
			}else{
				$applicant_name	=	'['.$sender_email.']';
			}
			
			
			if($receiver_email!=''){
				// $sender_email='test1@mvmteam.com';
				$staff_details_tbl			=	new admin_Model_staffLoginInformation();
				$dbsAdapterObj				=	$staff_details_tbl->getAdapter();
				$select						=	$staff_details_tbl->select()
												   ->from('staff_login_information',array('staff_id'))
												   ->where('( '.$dbsAdapterObj->quoteInto('email = ?', $receiver_email).' )')
												   ->order('staff_id DESC')->limit(1);
				$staffDetailsArr			=	$staff_details_tbl->fetchAll($select);
				$staffDetailsArr			=	$staffDetailsArr->toArray();
				if(count(array_filter($staffDetailsArr))>0){
					$staff_name	=	$staffDetailsArr[0]['staff_id'];
				}
				//echo $staff_name,'re',$receiver_email;
			}
			
			
			
			
		//}
	   //print_r($applicantDetailsArr);
	   $email_details_tbl 			=	new admin_Model_emailLogs();
	   $db = $email_details_tbl->getAdapter();
	  /* $select						=	$email_details_tbl->select()->where("sender_mail_id = $sender_email AND receiver_mail_id = $receiver_email")->orWhere("sender_mail_id = $receiver_email AND receiver_mail_id = $sender_email");*/
	  /* $select						=	$email_details_tbl->select()->where('(' . $db->quoteInto('sender_mail_id = ?', $sender_email) . ' AND ' . $db->quoteInto('receiver_mail_id = ?', $receiver_email) . ') OR (' . $db->quoteInto('sender_mail_id = ?', $receiver_email) . ' AND ' . $db->quoteInto('receiver_mail_id = ?', $sender_email) . ')')->order('mailing_date ASC');
	   $emailDetails				=	$email_details_tbl->fetchAll($select);
	   $emailDetailsArr				=	$emailDetails->toArray();
	 
	   $htmlArr						=	array();
	   $count						=	1;
	   
	  	
		if($temp_flag	=	false){
			$temp_receiver=$applicant_name;
			$applicant_name	=	$staff_name;
			$staff_name	=	$temp_receiver;
		}
					
	   foreach($emailDetailsArr as $key=>$value){
		    //print_r($emailDetailsArr[$key]);
		 /*  $htmlArr[]				=	'<tr height="50" >
          								<td>'.$count.'</td>
          								<td>'.$emailDetailsArr[$key]['subject'].'</td>
          								<td width="10px;"><div id="textfield_'.$count.'" cols="30" style="width: 265px; height: 61px;">'.$emailDetailsArr[$key]['message'].'</div></td>
          								<td>'.$emailDetailsArr[$key]['mailing_date'].'</td>
        								</tr>';
			$count++;	*/	
			/*if(trim($emailDetailsArr[$key]['message'])!='')	{
				$htmlArr[]				=	'<div id="'.$count.'">';
				//.$emailDetailsArr[$key]['sender_mail_id'];
				if($sender_email	==	$emailDetailsArr[$key]['sender_mail_id']){
					$htmlArr[]			=	'<span >'.$applicant_name.'</span>';
					
				}
				else{
					$htmlArr[]			=	'<span >'.$staff_name.'</span>';
					
				}//'<span >'.$emailDetailsArr[$key]['subject'].'</span> 
				$htmlArr[]				=	'<span style="float:right;font-weight:100;">'.$emailDetailsArr[$key]['mailing_date'].'</span><br/>
											<p style="font-style:normal;font-weight:100; margin-left:20px;" id="textfield_'.$count.'">'.$emailDetailsArr[$key]['message'].'</p>
											</div><br/>';
				$count++;
			}
	   }
   }
	   
	   $html_string					=	join('',$htmlArr);
	   echo json_encode(array('html'=>$html_string));
   }*/
    
}