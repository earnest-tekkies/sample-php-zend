<?php

class Plugin_acl extends Zend_Controller_Plugin_Abstract {

    
 public function preDispatch(Zend_Controller_Request_Abstract $request)
{
   try{
      $acl = new Zend_Acl();
    $groupRole="";
    $user_role="";
    $previleges=array();
       $auth = Zend_Auth::getInstance();
    if($auth->hasIdentity())
     {
        
       $identity = $auth->getIdentity();
        $storage=$auth->getStorage();
        if(isset($identity->role))
        {
        $role = strtolower($identity->role);
        $groupRole=strtolower($identity->group_role);
        $user_role=strtolower($identity->user_role);
        }
        else
            $role = 'guest';
     }
     else
         {
        $role = 'guest';
         }
       
       // add  default  resources
      $acl->add(new Zend_Acl_Resource('default:users'));
      $acl->add(new Zend_Acl_Resource('default:joinnow'));
      $acl->add(new Zend_Acl_Resource('default:index'));
       $acl->add(new Zend_Acl_Resource('default:captcha'));
       $acl->add(new Zend_Acl_Resource('default:sendgrid'));
   
          
// add the roles
    $acl->addRole(new Zend_Acl_Role('guest'));
    $acl->addRole(new Zend_Acl_Role('user'), 'guest');
    $acl->addRole(new Zend_Acl_Role('applicant'),'guest');
     $acl->addRole(new Zend_Acl_Role('matchmaker'),'guest'); 
    $acl->addRole(new Zend_Acl_Role('admin'), 'user');
     $acl->addRole(new Zend_Acl_Role('staff'),'admin');
     
      $acl->allow('guest', 'default:users',array('login','logout'));
    $acl->allow('guest', 'default:index');
    $acl->allow('guest', 'default:sendgrid');
    $acl->allow('guest', 'default:captcha','index');
     $acl->allow('guest', 'default:joinnow',array('index','emailregistration'));
     
     if($role !='guest')
     {
        
     if($groupRole !=null)
      $acl->addRole(new Zend_Acl_Role($role),"$groupRole");
     }
   
 
      
      
    $aclResources= new Model_aclResources();
   
    foreach($aclResources->fetchAll() as $resource)
     {
      $resourceList[$resource->module][]=$resource->controller;
    
    }
    
    
    
    foreach($resourceList as $module=>$controllers)
     {
      $moduleResource = new Zend_Acl_Resource($module);
     $acl->add( new Zend_Acl_Resource($moduleResource));
     
     foreach($controllers as $controller)
     {
         try{
          $controllerResource = new Zend_Acl_Resource($module.":".$controller);
     $acl->add($controllerResource,$moduleResource);
         }catch(Exception $e){ echo $e->getMessage();}
     }
    
    }
 
    $aclRoleGroup= new Model_aclRoleGroup();
    $aclUsersRoleTbl =new Model_aclRoleUsers();
    $aclResourcesGroup= new Model_aclResourcesGroup();
    //*********************************************
     $groupPrevileges=array();
    if($groupRole!=""){
     //echo 'renju';
        $groupPrevilegeRow=$aclRoleGroup->find($groupRole)->current();
		
        if($groupPrevilegeRow !="")
        {
              
                   $groupPrevileges=unserialize($groupPrevilegeRow->previleges);
				   //print_r(unserialize($groupPrevilegeRow->previleges));
           //print_r(unserialize('a:1:{s:15:"admin:applicant";a:3:{i:0;s:9:"emaillist";i:1;s:9:"getemails";i:2;s:11:"emaildetail";}}'));
		   
		   //print_r(serialize(array ( 'applicant:index' => array ( 0 => 'messages' ))));
		  // print_r(serialize(array ( 0 => 1,1 => 2, 2 => 3, 3 => 4 ,4 => 5, 5 => 6, 6 => 7, 7 => 8,8=>51,9=>70 )));

        }
		
        }
            
            $userPrevilegeRow=$aclUsersRoleTbl->find($user_role)->current();
             if($userPrevilegeRow !="")
            {
                $userPrevilegeAllow=unserialize($userPrevilegeRow->allow_previleges);
                $userPrevilegeDeny=unserialize($userPrevilegeRow->deny_previleges);
				
				
           
            }
             if(isset($groupPrevileges))
			
        foreach($groupPrevileges as $prev)
        {
           $previleges[$prev]=$prev; 
        }
         if(isset($userPrevilegeAllow))
         foreach($userPrevilegeAllow as $prev)
        {
           $previleges[$prev]=$prev; 
		    
        }
         if(isset($userPrevilegeDeny))
         foreach($userPrevilegeDeny as $prev)
        {
             if(isset($previleges[$prev])) unset($previleges[$prev]);
          
        }
         
    //*******************************************
    
   
    if(count($previleges)>0)
    {
        $prevelegeList=$aclResourcesGroup->fetchAll("acl_resource_id in ('".implode("','", $previleges)."')");
      
        foreach($prevelegeList as $resource)
        {
            $actionsList=unserialize($resource->actions);
               foreach($actionsList as $module=> $actions)
         {
           try{
              $acl->allow($role, $module,$actions);
           }catch(Exception $e){ echo $e->getMessage();}
           
         }
       
        }
    }
 


   
    
  
    $controller = $request->controller;
    $action = $request->action;
      $module = $request->module;
 
      
     $registry = Zend_Registry::getInstance();
     
     
     $registry->set('acl', $acl ); 
     $registry->set('role', $role ); 
     $registry->set('groupRole', $groupRole ); 
      $registry->set('unRead',0);
  
            if ($auth->hasIdentity()) {
                   $storedata=$storage->read();
               
                if($groupRole=='applicant'){
					$config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navApplicant.xml');
					/*$emailLogs= new applicant_Model_emailLogs();
					$query=$emailLogs->select();					
					$applicantId=$storedata->applicant_id;
					$query->from("email_log_information as el",array("unread"=>"count(*)"));
					$query->where("el.applicant_id = '".$applicantId."' and  send_by='S' and applicant_status ='0'"); 
					$query->setIntegrityCheck(false);
					$registry->set('unRead', $emailLogs->fetchRow($query)->unread ); */
					$messageLogs= new applicant_Model_messageLogs();
					$query=$messageLogs->select();					
					$applicantId=$storedata->applicant_id;
					$query->from("message_log as el",array("unRead"=>"count(*)"));
					$query->where("el.receiver_profile_id = '".$applicantId."' and  message_status='1'"); 
					$query->setIntegrityCheck(false);
					$registry->set('unRead', $messageLogs->fetchRow($query)->unRead); 
					
				}
                else
                {
                $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml');
                 $emailLogs= new applicant_Model_emailLogs();
                 $query=$emailLogs->select();
                            
                  
                 $query->from("email_log_information as el",array("unread"=>"count(*)"));
              $query->where(" send_by='A' and admin_status ='0'"); 
               $query->setIntegrityCheck(false);
              
               $registry->set('unRead', $emailLogs->fetchRow($query)->unread ); 
                }
                $container = new Zend_Navigation($config);
                $layout = Zend_Layout::getMvcInstance();
                $view = $layout->getView();
                
                $view->navigation($container)->setAcl($acl)->setRole($role);
            }
   
     if (!$acl->isAllowed($role, $module.":".$controller, $action))
         {
        if ($role == 'guest')
            {
             $request->setModuleName('default');
             $request->setControllerName('index');
              $request->setActionName('index');
              
              Zend_Layout::getMvcInstance()->setLayout('mainlayout');
             
           }
       else {
             $request->setModuleName('error');
             $request->setControllerName('index');
             $request->setActionName('index');
             
             
          }
    }
  
 }
 catch(Exception $e){ echo $e->getMessage();}

}
}
