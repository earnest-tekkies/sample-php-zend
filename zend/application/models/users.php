<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of models
 *
 * @author E Tekkies Team
 */
class Model_users extends Zend_Db_Table_Abstract {
protected $_name = 'users';
public function createUser($login_name, $password, $first_name, $last_name, $previlage)
    {
        // create a new row
        $rowUser = $this->createRow();
        if($rowUser)
            { 
             $rowUser->login_name = $login_name;
             $rowUser->password = md5($password);
             $rowUser->first_name = $first_name;
             $rowUser->last_name = $last_name;
             $rowUser->previlage = $previlage;
             $rowUser->save();
              
             return $rowUser;
        }
       else
           {
              throw new Zend_Exception("Could not create user!");
            }
    }
        public static function getUsers()
        {
        $userModel = new self();
        $select = $userModel->select();
        $select->order(array('first_name','last_name'));
        return $userModel->fetchAll($select);
        }
public function addnewusrer($lastid,$security_role,$login_id,$password,$f_name,$l_name)
    {

        $data=array(
            'users_id'=>$lastid,
            'login_name'=>$login_id,
            'password'=>md5($password),
            'first_name'=>$f_name,
            'last_name'=>$l_name,
            'previlage'=>$security_role,
        );
      $this->insert($data);
    }
    public function updateUser($id,$security_role,$f_name,$l_name)
    {
      $data=array(
            'first_name'=>$f_name,
            'last_name'=>$l_name,
            'previlage'=>$security_role,
        );
    print_r($data);
    $this->update($data, 'users_id = '.(int)$id);
}

     public function updateUsers($id, $login_name, $first_name, $last_name, $previlage)
        {
         // fetch the user's row
         $rowUser = $this->find($id)->current();
         if($rowUser)
           {
               $rowUser->login_name = $login_name;
               $rowUser->first_name = $first_name;
               $rowUser->last_name = $last_name;
               $rowUser->previlage = $previlage;
               $rowUser->save();
               return $rowUser;
           }
           else
           {
             throw new Zend_Exception("User update failed. User not found!");
           }
        }
public function deleteUser($id)
{
    // fetch the user's row
    $rowUser = $this->find($id)->current();
    if($rowUser)
        {
         $rowUser->delete();
        }else
          {
           throw new Zend_Exception("Could not delete user. User not found!");
         }
}
  public function updatePassword($id, $password)
    {
        // fetch the user's row
        $rowUser = $this->find($id)->current();
        if($rowUser)
            {
             //update the password
             $rowUser->password = md5($password);
             $rowUser->save();
            }
            else
            {
                throw new Zend_Exception("Password update failed. User not found!");
            }
    }

public function fetchUsers($filters = array(), $sortField = null, $limit = null, $page = 1)
	{
		$select = $this->select();
		// add any filters which are set
			if(count($filters) > 0)
			{
				foreach ($filters as $field => $filter)
			 	{
				$select->where($field . ' = ?', $filter);
				}
			}
			// add the sort field is it is set
			if(null != $sortField)
			{
			$select->order($sortField);
			}

			// create a new instance of the paginator adapter and return it
		$adapter = new Zend_Paginator_Adapter_DbTableSelect($select);
		return $adapter;
		//return $this->fetchAll($select);
	}
  
}
?>
