<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initView()
    {
        // Initialize view
        $view = new Zend_View();
        $view->doctype('XHTML1_STRICT');
        $view->headTitle('MVMTeam - Muslim Volunteers Matchmaking Team');
        $view->headMeta()->setName('keywords', 'php application');                     
        $view->headMeta()->appendHttpEquiv('pragma', 'no-cache');
        $view->headMeta()->appendHttpEquiv('Cache-control', 'no-cache');
        $view->headMeta()->setName('keywords', 'xavi'); 
        $view->headMeta()->setHttpEquiv('Content-Type', 'text/html;charset=utf-8');
        // Add it to the ViewRenderer
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper(
            'ViewRenderer'
        );
        $viewRenderer->setView($view);

        // Return it, so that it can be stored by the bootstrap
        return $view;
    }
    
 

  protected function _initAutoload()
{
      

// Add autoloader empty namespace
$autoLoader = Zend_Loader_Autoloader::getInstance();
$autoLoader
            ->registerNamespace('Pioneersys_')
            ->registerNamespace('PHPExcel_')
            ;
$resourceLoader = new Zend_Loader_Autoloader_Resource(array(
'basePath' => APPLICATION_PATH,
'namespace' => '',
'resourceTypes' => array(
'form' => array(
'path' => 'forms/',
'namespace' => 'Form_',
),
'model' => array(
'path' => 'models/',
'namespace' => 'Model_'
),
   'plugin' => array(
   'path' => 'plugin/',
   'namespace' => 'Plugin_'
)
)
));

// Return it so that it can be stored by the bootstrap
return $autoLoader;
}
protected function _initNavigation()
{ 
    $name= new Zend_Session_Namespace("l");
    $this->bootstrap('layout');
    $layout = $this->getResource('layout');
    $view = $layout->getView();
    $layout->setLayout('mainlayout');

   $config = new Zend_Config_Xml(APPLICATION_PATH.'/configs/navigation.xml');

    $navigation = new Zend_Navigation($config);
    
    $view->navigation($navigation);
 
 
}

}