<?php
class UsersController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $this->_helper->layout()->disableLayout();
        $authAdapter = Zend_Auth::getInstance();
        $authAdapter->clearIdentity();
    }
    public function addAction()
    {
        $form = new Form_usersAddForm();
          $userModel = new Model_users();
           $form->removeElement('confirmpword');
         $form->removeElement('pword');
         $dele= $form->getElement('Submit');
         $dele->setLabel('User Add');

         $user= $userModel->fetchAll();
       
   

        $this->view->form = $form;
        if ($this->_request->isPost())
           {
            if ($form->isValid($_POST)) {
             $login_name=$form->getValue('login_name');
              foreach($user as $dat)
            {
            if($dat->login_name == $login_name)
            {
             
             $this->view ->error= "Cannot Create User , Username Exist";
            }
            else {
                try{
                 $userModel->createUser(
            $form->getValue('login_name'),
            $form->getValue('password'),
            $form->getValue('first_name'),
            $form->getValue('last_name'),
            $form->getValue('previlage')
            );
                     $this->_redirect('/users/list');
            }
            catch (Exception $e)
            {
                echo "<script language='javascript'>alert(".$e->getMessage().")</script>";
            }
         //  return $this->_forward('/list');



            }
            }

             }
                          }
                     
    }

    public function listAction()
    {
        $currentUsers =new Model_users();
        $query=$currentUsers->select();
        $query->from(array('o'=>'users'),array('*'));
         $query->setIntegrityCheck(FALSE);
         
              $adapter =  new Zend_Paginator_Adapter_DbTableSelect($query);
                        $paginator = new Zend_Paginator($adapter);
                        $paginator->setItemCountPerPage(10);
                        //if none is set then default to page 1.
                        $page = $this->_request->getParam('page', 1);
                        $paginator->setCurrentPageNumber($page);
                        // pass the paginator to the view to render
                        $this->view->paginator = $paginator;
    }
    public function updateAction()
    {
           $id = $this->_request->getParam('id');
       
           $userModel = new Model_users();
           $currentUser = $userModel->find($id)->current();
           $form = new Form_usersAddForm();
            $form->removeElement('confirmpword');
           $form->removeElement('password');
           $form->removeElement('pword');
           $login_name= $form->getElement('login_name');
           $login_name->setAttrib('readonly','readonly');
           $dele= $form->getElement('Submit');
           $dele->setLabel('Update');
          $form->populate($currentUser->toArray());
         
      if ($this->_request->isPost())
         {

        if ($form->isValid($_POST))
               {
            
                $userModel->updateUsers(
                       $id,
                $form->getValue('login_name'),
                $form->getValue('first_name'),
                $form->getValue('last_name'),
                $form->getValue('previlage')
                );
                return $this->_forward('list');
                }
               // print_r($form->getErrors());
            }
         $this->view->form=$form;

    }

    public function passwordAction()
    {
        
        $id = $this->_request->getParam('id');
        $form = new Form_usersAddForm();
        $userModel = new Model_users();
        $currentUser = $userModel->find($id)->current();
         $password = $userModel->find($id)->current()->password;
  
        $form->populate($currentUser->toArray());

        
                               $form->removeElement('first_name');
                               $form->removeElement('last_name');
                               $form->removeElement('login_name');
                               $form->removeElement('previlage');
                               $form->removeElement('type');
                                 $dele= $form->getElement('Submit');
                            $dele->setLabel('Change Password');
                           
                                if ($this->_request->isPost())
                                   {
                                      
                                     if ($form->isValid($_POST))
                                       {
                                         $pword=$form->getValue('pword');
                                         if($password==md5($pword))
                                         {
                                         if($form->getValue('password')==$form->getValue('confirmpword'))
                                         {
                                        $userModel->updatePassword(
                                        $id,
                                        $form->getValue('password')
                                        );
                                        return $this->_forward('list');
                                         }else
                                         {
                                             $this->view ->error= "New Password doesn't match to Confirm Password";
                                         }
                                         }
                                         else {
                                           $this->view ->error= "Cannot Change ,Incorrect Password ";
                                         }
                                       }
                                       else {
                                           $form->populate($_POST);
                                       }
                                   }
                             
                             $this->view->form = $form;
    }

    public function deleteAction()
    {
         $id = $this->_request->getParam('id');
           $userModel = new Model_users();
        $currentUser = $userModel->find($id)->current();
           $form = new Form_usersAddForm();
            $form->removeElement('confirmpword');
             $form->populate($currentUser->toArray());
  $this->view->headScript() ->appendScript( "
                        function confirmDelete()
                        {
                        var s=confirm('Do You Really Want To Delete This User' );
                        if(s==0)
                        {
                        return false;
                        }
                        else
                        {
                        return true;
                        }
                        }
                        "
                        );
       
       
           
            $form->setAttrib('onSubmit','return confirmDelete(); ');
                            $login_name= $form->getElement('login_name');
                            $login_name->setAttrib('readonly','readonly');
                            $first_name= $form->getElement('first_name');
                            $first_name->setAttrib('readonly','readonly');
                            $last_name= $form->getElement('last_name');
                            $last_name->setAttrib('readonly','readonly');
                            $previlage= $form->getElement('previlage');
                            $previlage->setAttrib('readonly','readonly');
                            $dele= $form->getElement('Submit');
                            $dele->setLabel('Delete');
              if ($this->_request->isPost())
                {
                     $userModel->deleteUser($id);
                     $this->_redirect('/users/list/');
   
                    }
                   $this->view->form=$form;
       
    }

   
    
    public function loginAction()
    {
         $authAdapter = Zend_Auth::getInstance();
         $authAdapter->clearIdentity();
       
       
         $this->_helper->layout()->disableLayout();
         $userModel = new Model_staffLogin();
         $applicantDetailsTbl = new applicant_Model_applicantDetails();
         $loginLogsTbl = new Model_loginLogs();
         
         if($this->getRequest()->isPost())
         {
            $login_id = trim($this->_getParam('login_id'));
            $password = $this->_getParam('password');

            $db = Zend_Db_Table::getDefaultAdapter();
            if(is_numeric($login_id))
                {
                 $db_table ="applicant_login";  
                 $Identity=    'applicant_id';
                 $credential=  'app_password';
                 }
             else{
                  $db_table ="staff_login_information";  
                  $Identity     =    'staff_id';
                  $Identity     =   strtolower($Identity);
                  $login_id     =   strtolower($login_id);
                  $credential   =  'password';
            }
             
            $authAdapter = new Zend_Auth_Adapter_DbTable ($db,$db_table,$Identity,$credential);
            $error = $Identity.$login_id.$password;
            $authAdapter->setIdentity($login_id);
            $authAdapter->setCredential(md5($password));
          
            $result = $authAdapter->authenticate();
             if ($result->isValid() ) 
                 {

                    try{
                        $loginRow=$authAdapter->getResultRowObject();
                        $auth = Zend_Auth::getInstance();
                        $storage=$auth->getStorage();
                        $storage->write($loginRow);
                        $row= $loginLogsTbl->createRow();
                        
                       
                       if(is_numeric($login_id)){
                         
                           $applicant_id=$loginRow->applicant_id;
                           $applicantDetails= $applicantDetailsTbl->find($applicant_id)->current();
                           if($applicantDetails=="")
                           {
                            throw new Zend_Exception("Invalid User Name or Password");
                           }
                           if($applicantDetails->approval_status=="Closed")
                           {
                           throw new Zend_Exception("User Unsubcribed!");
                           }
                               
                     

                    $userDetails=new stdClass();
                    $userDetails->applicant_id=$applicantDetails->applicant_id;
                    $userDetails->user_role=$applicantDetails->applicant_id;
                    $userDetails->group_role="applicant";
                    $userDetails->staff_id=$applicantDetails->app_first_name." ".$applicantDetails->app_last_name;
                    $userDetails->role="applicant-".$applicantDetails->applicant_id;
                    $row->userid=$userDetails->applicant_id;
                    $row->usertype=$userDetails->group_role;	
                    $row->login_time=date('Y-m-d H:i:s');
                    $row->logout_time=date('Y-m-d H:i:s');
                    $row->pages_visited=0;
                    $row->save();
                    $userDetails->loginRowId=$row->id;
                    $storage->write($userDetails);
                    $this->_redirect('/applicant');
                     
                   }
                  else   /// staff login
                  {
                     
                    $staff_id=$loginRow->staff_id;
                    $query =$userModel->select();
                    $query->from('staff_login_information as sli',array('staff_id','user_role'=>'staff_id',
                        'group_role'=>'usertype','role'=>'CONCAT(usertype,  \'-\',  staff_id )','status','last_login_date','name'));
                    $query->where("sli.staff_id='$staff_id'");
                    $query->setIntegrityCheck(false);
                   
                    $staffDetails=$userModel->fetchRow($query);
                    
                      if($staffDetails=="")
                           {
                            throw new Zend_Exception("Invalid User Name or Password");
                           }
                           if($staffDetails->status!="Enabled")
                           {
                           throw new Zend_Exception("User Disabled!");
                           }
                   
                        $userDetails=new stdClass();
                    $userDetails->user_role=$staffDetails->user_role;
                    $userDetails->group_role=$staffDetails->group_role;
                    $userDetails->staff_id=$staffDetails->staff_id;
                    $userDetails->role=$staffDetails->role;
                    $row->userid=$userDetails->staff_id;
                    $row->usertype=$userDetails->group_role;	
                    $row->login_time=date('Y-m-d H:i:s');
                    $row->logout_time=date('Y-m-d H:i:s');
                    $row->pages_visited=0;
                    $row->save();
                    $userDetails->loginRowId=$row->id;
                    $storage->write($userDetails);
                     
                        $storage->write($userDetails);
                        $this->_redirect('admin/applicant/profilesearch');
                    
                      
                  }
                                
               } catch(Exception $e)
                      {
                       
                         $reqUrl= $this->_getParam('reqUrl');
                         $req =$this->getRequest();
                         $this->_redirect($reqUrl."/e/".base64_encode($e->getMessage()));
                      }
         
                            
              }
            else{
                $reqUrl= $this->_getParam('reqUrl');
                 $req =$this->getRequest();
                  if(is_numeric($login_id)){
                        $applicantDetails= $applicantDetailsTbl->find($login_id)->current();
                      if(count($applicantDetails)==0)
                      {
                       $error ="Invalid Applicant Id";
                      }
                       else
                       {
                       $error ="Password Doesnt Match";
                       }
                  }else{
                      $login_id = strtolower($login_id);
                       $userDetails = $userModel->fetchRow("LOWER(staff_id)='$login_id'");
                      if(count($userDetails)==0)
                      {
                        $error ="Invalid User Name";
                      }
                       else
                       {
                        $error ="Password Doesnt Match";
                       }
                  }
                 
               $this->_redirect($reqUrl."/e/".base64_encode($error));
              

            }
            }
       
         
   
    }
    
    
    public function logoutAction()
    {
        $authAdapter = Zend_Auth::getInstance();
        $result=$authAdapter->getIdentity();
       $authAdapter->clearIdentity();
        $this->_redirect('/');
    }


}

