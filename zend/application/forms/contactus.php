<?php

class Form_contactus extends Zend_Form
{
    public function init()
    {
       
        // create new element
       
        // element options
        $this->setDecorators(array('ViewHelper'));
        // add the element to the form
      $this->setAttrib("id","contactus");
        $this->setName("contactus");
        //create the form elements

         
       $app_gender = $this->createElement('radio','gender');
       $app_gender->addMultiOptions(array('Male'=>'Male','Female'=>'Female'))
               ->setRegisterInArrayValidator(false);
       $app_gender->setAttrib('class','pageRequired gender')->setAttrib('title','Please select your sex');
       $this->addElement($app_gender);

         $username = $this->createElement('text','name');
        $username->setRequired('true');
        $this->addElement($username);
        
        
        $email = $this->createElement('text', 'email');
        $email->setRequired('true');
        $this->addElement($email);
        
      
        $phone = $this->createElement('text','phone')->setRequired('True');
        $this->addElement($phone);
        
        
        $notes = $this->createElement('textarea','notes')
                ->setAttrib('rows', 4)->setAttrib('cols',18)->setRequired('True');
        $this->addElement($notes);
        
       
        $submit = $this->createElement('submit', 'Submit');
        $this->addElement($submit);
        $this->setElementDecorators(array ('viewHelper'));
        $this->setMethod('post');

    }
  }
?>
