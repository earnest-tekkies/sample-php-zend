<div id="zone-bar">
<ul>
    <li><a href="#"><span>Human Resource&nbsp;
                <em class="opener-technology"><img src="/images/zonebar-downarrow.png" alt="dropdown" /></em>
	</span></a>
        <ul class="technologysublist">
                <li><a href="/humanresource/employeeinformation">Employee Information</a></li>
                <li><a href="/humanresource/employeeinformation/addemployeehumanresource">Employee Add</a></li>
                <li><a href="/commonsettings/commonconfig/workshiftview">Work Shift</a></li>
                <li><a href="/humanresource/candidaterecrutement/getjobopen">Candidate Recruitment</a></li>
                 <li><a href="/humanresource/organisationtree/">Organisation Tree</a></li>
        </ul>
    </li>
    
    <li><a href="#"><span>Sales<em class="opener-world"><img src="/images/zonebar-downarrow.png" alt="dropdown" /></em></span></a>
      <ul class="worldsublist">
                <li><a href="/sales/salesenquiry/viewsalesenquiry/"> Enquiry</a></li>
                <li><a href="/sales/customers/viewsalescustomerdetails/">Customers</a></li>

                <li><a href="/sales/salesenquiryfollowup/enquiryfollowup/">Enquiry Follow Up</a></li>
                 <li><a href="/sales/bookings/followup/">Booking Follow Up</a></li>
                 <li><a href="/sales/bookings/add/">Booking Add</a></li>
                <li><a href="/sales/bookings/">Bookings List</a></li>
                <li><a href="/sales/allotments/">Allotment List</a></li>
                <li><a href="/sales/bookings/">Bookings List</a></li>
                <li><a href="/sales/pdiinspection/pdiinspection/">PDI Inspection</a></li>


        </ul>
    </li>
    
    <li><a href="#"><span>Service<em class="opener-science"><img src="/images/zonebar-downarrow.png" alt="dropdown" /></em></span></a>
       <ul class="sciencesublist">
               <li><a href="/service/jobcard/addjobcard">Jobcard</a></li>
                <li><a href="/schedules/servicereminder/scheduleservicereminder">Service Reminder</a></li>
               <li><a href="/service/serviceconfirm/viewserviceconfirmlist">Service confirm List</a></li>
               <li><a href="/service/onlinecustomer/viewregisteredonlinecustomer">Online customer</a></li>
               <li><a href="/service/parts/addserviceparts"> Parts And Lubricants</a></li>
               <li><a href="/service/parts/addjobdescription">Labour</a></li>
               <li><a href="/service/parts/jobcardview">Jobcard Filter</a></li>
                        
              <li><a href="/service/parts/outsidework">Outside work</a></li>
               

        </ul>
    </li>
   <li><a href="#"><span>Spares&nbsp;<em class="opener-gaming"><img src="/images/zonebar-downarrow.png" alt="dropdown" /></em></span></a>
        <ul class="gamingsublist">

                <li><a href="/spares/sparesmaster/sprpartmasterview">Parts Master</a></li>
              <li><a href="/spares/sparespurchase/sprpurchaseorderview"> Purchase Order</a></li>
                <li><a href="/spares/sparespurchase/sprpurchaseview"> Purchase  </a></li>
                 <li><a href="/spares/sparessales/sprquatationview"> Quatation</a></li>
                 <li><a href="/spares/sparessales/sprsalesview/"> Sales</a></li>
                 <li><a href="/spares/sparesstock/stocklist" >Stock Tranfer</a></li>
                    <li><a href="/spares/sparessales/backorder">Back Order</a></li>
                    <li><a href="/spares/sparespurchase/purchasebackorder">Purchase Back Order</a></li>
                   <li><a href="/spares/sparesstock/deadstockreminder">Dead Stock</a></li>
                      <li><a href="/spares/sparesmaster/itemstatus" >Item Status</a></li>
                <li><a href="/spares/sparesmaster/viewsparecatmaster">Category Master</a></li>
                <li><a href="/spares/sparesmaster/viewsparegroupmaster">Group Master</a></li>
                <li><a href="/spares/sparesmaster/viewsubgroup">Sub Group Master</a></li>
        </ul>
</li>
 
<li><a href="#"><span>Settings&nbsp;<em class="opener-lifestyle"><img src="/images/zonebar-downarrow.png" alt="dropdown" /></em></span></a>
    <ul class="lifestylesublist">
        <li><a href="/commonsettings/commonconfig/commonappearence">Common&amp; Setings</a></li>
        <li><a href="/commonsettings/commonconfig/departmentsetting">Common Department</a></li>
           <li><a href="/spares/settings/taxsettingsview">Tax Settings</a></li>
        <li><a href="/commonsettings/commonconfig/viewsettinggrade">Grade</a></li>
        <li><a href="/commonsettings/commonconfig/viewempmaster">Employee Master</a></li>
        <li><a href="/commonsettings/commonconfig/viewsdesigsettings">Designation</a></li>
        <li><a href="/commonsettings/commonconfig/viewsettinglocality">Locality</a></li>
        <li><a href="/commonsettings/commonconfig/viewsettingtaluk">Taluk</a></li>
        <li><a href="/commonsettings/commonconfig/viewsettingenquirytype">Enquiry Type</a></li>
        <li><a href="/commonsettings/commonconfig/viewsettingcustomertypemaster">Customer Type</a></li>
        <li><a href="/masters/servicetypes/viewservicetypes">Service Type</a></li>
       
        <li><a href="/commonsettings/commonconfig/viewsettingprofession">Profession</a></li>
        <li><a href="/commonsettings/commonconfig/viewsettingpurchasetype">Purchase</a></li>
        <li><a href="/users/list">Users</a></li>
         <li><a href="/masters/customerfeedback/addcustomerfeedback">Customer feedback</a></li>
        <li><a href="/commonsettings/commonconfig/viewservicereminderday">service Reminder</a></li>
          <!--li><a href="/masters/customerfeedback/viewcustomerfeedback">Customer feedback</a></li-->
    </ul>
</li>
<li><a href="#"><span>Report&nbsp;<em class="opener-entertainment"><img src="/images/zonebar-downarrow.png" alt="dropdown" /></em></span></a>
    <ul class="entertainmentsublist">

    </ul>
</li>
<li>
        <a href="#"><span>Finance&nbsp;<em class="opener-sports"><img src="/images/zonebar-downarrow.png" alt="dropdown" /></em>
        </span></a>
        <ul class="sportssublist">


        </ul>
</li>

</ul>
</div>
